# MyAnimeCloud #

## Introduction

I have created this application to extend MAL functionality for voice actors (seiyuu).

## Contact

I would love to work together with other developers to improve this application.

Don't hesitate to send a message to Cloud8x4@gmail.com.

### Use cases

* When watching an anime you think "I know this seiyuu" and want to quickly get info of it.
* What other roles does this voice actor have?
* Have I watched a lot of animes with this seiyuu?
* Are there still main / side roles this seiyuu voiced? I would like to quickly find them add them to my MAL Plan To Watch list. 
* I want to give a score to the seiyuus.
* I want to show seiyuus in a list with next to them info about them, links to the MAL site en especially other important roles sorted by ranking or most recent role or other ...

# Installation ##

## Development in Homestead

* For development I use Windows 10 and a [Laravel Homestead](https://laravel.com/docs/master/homestead) virtual machine.
* Choose an application name. In the following example I use myanimecloud.test.
* Map the new applications in Homestead.yaml.
```
sites:
    - map: myanimecloud.test
      to: /home/vagrant/Code/myanimecloud/public
      
databases:
    - myanimecloud
```
* Add `127.0.0.1 myanimecloud.test` to your hosts file `C:\Windows\System32\drivers\etc\hosts`.
* Run the instructions in `deploy_homestead.sh`.
* Run `sudo nano /etc/cron.d/myanimecloud` to create a cron file for your application and automate an import of a seiyuu or anime every 5 minutes.
```
* * * * * root php /home/vagrant/Code/myanimecloud/artisan schedule:run >> /dev/null 2>&1
```

## Production in Ubuntu 20.04 #

Execute the instructions in `deploy.sh`.

Some notes.
* I executed the instructions after adding a new Virtual Machine and installing ubuntu-20.04.1-live-server-amd64.iso.
* I had trouble after executing mysql_secure_installation with logging in. Method can be improved.
* Auto updates and auto cleanup is not clear and can be improved.
* Probably too many PHP modules are being installed.
* Not sure what to select when configuring locales.

## Development and production

The following is the same for development and production to finish installing the MyanimeCloud application.

* In the myanimecloud application directory, run `composer install`.
* Run `cp .env.example .env`.
* In `.env` set `APP_ENV` to production, `APP_DEBUG` to false and `APP_URL` to https://myanimecloud.com.
* In `.env` set the database details `DB_DATABASE`, `DB_USERNAME` and `DB_PASSWORD`.
* Run `php artisan key:generate` to generate your application key. The result is automatically written in .env after APP_KEY=.
* Run `php artisan config:cache && php artisan config:clear`.
* Run `php artisan migrate` to create the application database structure.
* Run `php artisan storage:link` to create a link to your public disc.
    * If you get an error using Homestead, I found 2 possible solutions.
        * Stop vagrant, open bash as administrator and run vagrant, then execute previous command.
        * In bash outside of Vagrant, run `ln -sr storage/app/public public/storage`. 
* Run `cd storage/app/public && mkdir seiyuu && mkdir anime && mkdir character && cd ../../..` to create directories for images.
* Run `sudo chmod 777 -R storage` to fix permissions.
* Run `yarn` to install package dependencies.
  * Cross-env needs to be installed for our build process to work. You might need to run `yarn global add cross-env` if you get errors saying cross_env could not be found.
* Run `yarn run dev` to build your front-end for development and `yarn run production` for production.
  * If you get an error about links, run `yarn install --no-bin-links`.
* In your web browser, go to https://myanimecloud.com.
* Register an account, login and enjoy a view of a website without data!

# Importing data
* Test some manual imports using myanimecloud.com/import/
   * animelist/<yourusername>
   * anime/<MAL_anime_id>
   * import/anime/cast/(MAL_anime_id)
   * import/person/<MAL_person_id>
* In your web browser (Chrome advised) go to myanimecloud.com.
* Login and enjoy the beautiful view as your available data steadily grows larger.

# Maintain code quality

https://eslint.org

## In Homestead

`composer fixlint`

## In Windows

`yarn fixlint`

# Back end testing

https://phpunit.de

This runs automated unit and acceptance tests.

## Usage

`phpunit`

### Options

https://phpunit.readthedocs.io/en/8.5/textui.html#command-line-options

`--no-coverage`: Ignore code coverage configuration.
When xdebug is enabled the tests run quite slow. Run without code coverage for faster results.

`--filter=<pattern>`: Filter which tests to run.

`--stop-on-defect`: Stop execution upon first not-passed test.

### Preparation

Create an `.env.testing` file which points to separate test databases to avoid cluttering your development database.

To ensure jobs are tested properly set `QUEUE_CONNECTION=sync` so jobs are executed during testing.

# Front end testing

* https://www.cypress.io
* https://docs.cypress.io/guides/getting-started/installing-cypress.html
* https://github.com/laracasts/cypress

## Usage

`yarn run cypress open`
