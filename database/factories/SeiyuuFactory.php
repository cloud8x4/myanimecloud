<?php

namespace Database\Factories;

use App\Models\Seiyuu;
use Illuminate\Database\Eloquent\Factories\Factory;

class SeiyuuFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Seiyuu::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'MAL_id' => $this->faker->numberBetween(1, 100000),
            'image' => uniqid() . '.' . $this->faker->fileExtension,
            'name' => $this->faker->name,
            'given_name' => $this->faker->optional()->name,
            'family_name' => $this->faker->optional()->name,
            'birthday' => $this->faker->optional()->date($format = 'Y-m-d', $max = 'now'),
            'website_url' => null,
            'more_details' => null,
            'favorited_count' => $this->faker->optional()->numberBetween(1, 100000),
            'gender' => $this->faker->optional()->randomElement(['male', 'female']),
        ];
    }
}
