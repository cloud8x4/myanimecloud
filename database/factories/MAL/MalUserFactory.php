<?php

namespace Database\Factories\MAL;

use App\Models\MAL\MalUser;
use Illuminate\Database\Eloquent\Factories\Factory;

class MalUserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = MalUser::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->userName,
            'anime_days' => $this->faker->numberBetween(1, 1000),
        ];
    }
}
