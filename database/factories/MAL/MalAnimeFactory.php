<?php

namespace Database\Factories\MAL;

use App\Models\Anime;
use App\Models\MAL\MalAnime;
use App\Models\MAL\MalUser;
use Illuminate\Database\Eloquent\Factories\Factory;

class MalAnimeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = MalAnime::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'mal_user_id' => function () {
                return MalUser::factory()->create()->id;
            },
            'anime_id' => function () {
                return Anime::factory()->create()->id;
            },
            'score' => $this->faker->numberBetween(0, 10),
            'watched_status' => $this->faker->randomElement([
                'completed',
                'dropped',
                'plan to watch',
                'on-hold',
                'watching',
            ]),
            'watched_episodes' => function (array $post) {
                $episodes = Anime::find($post['anime_id'])->episodes;
                if ($episodes) {
                    return $this->faker->numberBetween(0, $episodes);
                }

                return 0;
            },
        ];
    }
}
