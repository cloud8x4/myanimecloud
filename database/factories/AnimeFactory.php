<?php

namespace Database\Factories;

use App\Models\Anime;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class AnimeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Anime::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'MAL_id' => $this->faker->numberBetween(1, 100000),
            'title' => $this->faker->name,
            'type' => $this->faker->optional()->randomElement(['TV', 'Movie', 'OVA', 'Special', 'Music', 'ONA']),
            'episodes' => $this->faker->optional()->numberBetween(1, 1000),
            'duration' => $this->faker->optional()->numberBetween(1, 10000),
            'status' => $this->faker->optional()->randomElement([
                'finished airing',
                'currently airing',
                'not yet aired',
            ]),
            'start_date' => Carbon::now(),
            'end_date' => Carbon::now(),
            'classification' => $this->faker->optional()->randomElement([
                'PG-13 - Teens 13 or older',
                'R+ - Mild Nudity',
                'R - 17+ (violence & profanity)',
                'None',
                'G - All Ages',
                'PG - Children',
                'Rx - Hentai',
            ]),
            'members_score' => $this->faker->optional()->randomFloat(2, 0, 10),
            'members_count' => $this->faker->optional()->numberBetween(1, 100000),
            'rank' => $this->faker->optional()->numberBetween(1, 10000),
            'popularity_rank' => $this->faker->optional()->numberBetween(1, 10000),
            'favorited_count' => $this->faker->optional()->numberBetween(1, 10000),
            'image' => uniqid() . '.' . $this->faker->fileExtension,
        ];
    }
}
