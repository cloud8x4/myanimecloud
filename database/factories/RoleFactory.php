<?php

namespace Database\Factories;

use App\Models\Anime;
use App\Models\Character;
use App\Models\Role;
use App\Models\Seiyuu;
use Illuminate\Database\Eloquent\Factories\Factory;

class RoleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Role::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'seiyuu_id' => function () {
                return Seiyuu::factory()->create()->id;
            },
            'character_id' => function () {
                return Character::factory()->create()->id;
            },
            'anime_id' => function () {
                return Anime::factory()->create()->id;
            },
            'role' => $this->faker->randomElement(['Main', 'Supporting']),
        ];
    }
}
