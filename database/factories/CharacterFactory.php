<?php

namespace Database\Factories;

use App\Models\Character;
use Illuminate\Database\Eloquent\Factories\Factory;

class CharacterFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Character::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'MAL_id' => $this->faker->numberBetween(1, 100000),
            'name' => $this->faker->name,
            'image' => uniqid() . '.' . $this->faker->fileExtension,
            'favorited_count' => $this->faker->optional()->numberBetween(1, 100000),
            'gender' => $this->faker->optional()->randomElement(['male', 'female']),
        ];
    }
}
