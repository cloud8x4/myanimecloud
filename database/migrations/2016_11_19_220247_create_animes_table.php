<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('animes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('MAL_id')->unique();
            $table->string('title');
            $table->string('type')->nullable();
            $table->unsignedInteger('episodes')->nullable();
            $table->unsignedSmallInteger('duration')->nullable();
            $table->string('status')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->string('classification')->nullable();
            $table->decimal('members_score')->nullable();
            $table->unsignedInteger('members_count')->nullable();
            $table->unsignedInteger('rank')->nullable();
            $table->unsignedInteger('popularity_rank')->nullable();
            $table->unsignedInteger('favorited_count')->nullable();
            $table->string('image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animes');
    }
}
