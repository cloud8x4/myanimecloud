<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnimeOpeningThemeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anime_opening_theme', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('anime_id');
            $table->unsignedInteger('opening_theme_id');
            $table->timestamps();

            $table->foreign('anime_id')->references('id')->on('animes');
            $table->foreign('opening_theme_id')->references('id')->on('opening_themes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('anime_opening_theme');
    }
}
