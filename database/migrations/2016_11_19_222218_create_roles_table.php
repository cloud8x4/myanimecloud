<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('seiyuu_id');
            $table->unsignedInteger('character_id');
            $table->unsignedInteger('anime_id');
            $table->string('role');
            $table->timestamps();

            $table->foreign('seiyuu_id')->references('id')->on('seiyuus');
            $table->foreign('character_id')->references('id')->on('characters');
            $table->foreign('anime_id')->references('id')->on('animes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
