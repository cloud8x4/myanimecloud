<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSoftDeletes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        collect([
            'animes',
            'characters',
            'ending_themes',
            'genres',
            'mal_animes',
            'mal_users',
            'opening_themes',
            'posts',
            'producers',
            'roles',
            'seiyuus',
            'user_settings',
        ])
            ->each(function ($table) {
                Schema::table($table, function (Blueprint $table) {
                    $table->softDeletes();
                });
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        collect([
            'animes',
            'characters',
            'ending_themes',
            'genres',
            'mal_animes',
            'mal_users',
            'opening_themes',
            'posts',
            'producers',
            'roles',
            'seiyuus',
            'user_settings',
        ])
            ->each(function ($table) {
                Schema::table($table, function (Blueprint $table) {
                    $table->dropSoftDeletes();
                });
            });
    }
}
