<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddScoreByCharacterRanksToUserSeiyuuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_seiyuu', function (Blueprint $table) {
            $table->integer('score_by_character_ranks')->after('rank')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_seiyuu', function (Blueprint $table) {
            $table->dropColumn('score_by_character_ranks');
        });
    }
}
