<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMalAnimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mal_animes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('mal_user_id');
            $table->unsignedInteger('anime_id');
            $table->string('watched_status');
            $table->unsignedSmallInteger('watched_episodes');
            $table->decimal('score');
            $table->timestamps();

            $table->foreign('mal_user_id')->references('id')->on('mal_users');
            $table->foreign('anime_id')->references('id')->on('animes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mal_animes');
    }
}
