<?php

use App\Models\Seiyuu;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeiyuusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'seiyuus',
            function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('MAL_id')->nullable()->unique();
                $table->string('image')->nullable();
                $table->string('name');
                $table->string('given_name')->nullable();
                $table->string('family_name')->nullable();
                $table->date('birthday')->nullable();
                $table->string('website_url')->nullable();
                $table->text('more_details')->nullable();
                $table->unsignedInteger('favorited_count')->nullable();
                $table->string('gender')->nullable();
                $table->timestamps();
            }
        );

        $unknownSeiyuu = new Seiyuu();
        $unknownSeiyuu->name = 'unknown';
        $unknownSeiyuu->image = 'unknown.gif';
        $unknownSeiyuu->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seiyuus');
    }
}
