<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnimeEndingThemeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anime_ending_theme', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('anime_id');
            $table->unsignedInteger('ending_theme_id');
            $table->timestamps();

            $table->foreign('anime_id')->references('id')->on('animes');
            $table->foreign('ending_theme_id')->references('id')->on('ending_themes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('anime_ending_theme');
    }
}
