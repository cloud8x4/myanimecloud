module.exports = {
    env: {
        browser: true,
        es2021: true,
        node: true,
        jquery: true,
    },
    extends: [
        'eslint:recommended',
        'plugin:vue/essential',
        'plugin:vue/recommended',
    ],
    globals: {
        '$': true,
        axios: true,
        Vue: true,
    },
    parserOptions: {
        ecmaVersion: 12,
        sourceType: 'module',
    },
    plugins: [
        'vue',
    ],
    root: true,
    rules: {
        'array-bracket-spacing': [ 2, 'always' ],
        'arrow-spacing': 2,
        'block-spacing': 2,
        'comma-dangle': [ 2, 'always-multiline' ],
        'eol-last': [ 2, 'always' ],
        'indent': [ 'error', 4, { 'SwitchCase': 1 } ],
        'no-irregular-whitespace': 2,
        'no-trailing-spaces': 2,
        'no-unsafe-negation': 2,
        'no-useless-concat': 'error',
        'no-whitespace-before-property': 2,
        'object-curly-newline': [
            'error', {
                'consistent': true,
                'minProperties': 2,
                'multiline': true
            }
        ],
        'object-property-newline': 'error',
        'prefer-template': 'error',
        'quotes': [ 2, 'single' ],
        'semi': [ 2, 'never' ],
        'sort-imports': [
            'error', {
                'ignoreCase': true,
                'ignoreMemberSort': false,
                'memberSyntaxSortOrder': [ 'none', 'all', 'multiple', 'single' ],
            },
        ],
        'vue/html-closing-bracket-newline': [
            'error', {
                'singleline': 'never',
                'multiline': 'always',
            },
        ],
        'vue/html-closing-bracket-spacing': [
            2, {
                "startTag": "never",
                "endTag": "never",
                "selfClosingTag": "never"
            }
        ],
        'vue/html-indent': [
            'error', 4, {
                'attribute': 1,
                'alignAttributesVertically': true,
            },
        ],
        'vue/max-attributes-per-line': [
            2, {
                'singleline': 1,
                'multiline': {
                    'max': 1,
                    'allowFirstLine': true,
                },
            },
        ],
        'vue/no-unused-vars': 'error',
        'vue/no-v-html': 0,
        'vue/require-default-prop': 2,
        'vue/require-prop-types': 2,
        'wrap-iife': 2,
        'yoda': 2,
    }
}
