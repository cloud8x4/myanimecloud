<?php

return [
    'Seiyuu' => 'Seiyuu',
    'id' => env('APP_NAME') . 'ID',
    'MAL_id' => 'MAL ID',
    'favorited_count' => 'Favorites',
    'name' => 'Name',
    'current_user_seiyuu_rank' => 'My rank',
    'seiyuu_rank' => 'User rank',
    'Gender' => 'Gender',
    'Age' => 'Age',
    'Birthday' => 'Birthday',
    'seiyuu_score_by_character_ranks' => 'My score',
    'My rank' => 'My rank',
    'Sort by' => 'Sort by',
    'User rank' => 'User rank',
];
