<?php

return [
    'Anime' => 'Anime',
    'Character' => 'Character',
    'erabe_game' => 'Erabe Game',
    'Login' => 'Login',
    'Logout' => 'Logout',
    'Roles' => 'Roles',
    'Profile' => 'Profile',
    'Top' => 'Top',
    'Register' => 'Register',
    'Shared' => 'Shared',
    'Search' => 'Search',
    'Seiyuu' => 'Seiyuu',
    'Users' => 'Users',
];
