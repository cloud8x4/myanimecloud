<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Datetime Language Lines
    |--------------------------------------------------------------------------
    |
    */
    'day' => '%A',
    'time' => '%R',
    'monthdate' => '%d %h',
    'daydatemonth' => '%A %d %B',
    'daydatemonthyear' => '%A %d %B %Y',
    'monthdatecommayear' => '%B %d, %Y',
    'dateformatform' => '%d/%m/%Y %H:%M',
    'daycommayear' => '%d, %Y',
    'mysql' => '%Y-%m-%d %T',
    'form' => [
        'date_format' => 'd/m/Y H:i',
        'allday_date_format' => 'd/m/Y',
        'mask' => '00/00/0000 00:00',
        'allday_mask' => '00/00/0000',
        'placeholder' => '__/__/____ __:__',
        'allday_placeholder' => '__/__/____',
        'datepicker_locale' => 'en-gb',
        'datepicker_allday_format' => 'DD/MM/YYYY',
    ],
];
