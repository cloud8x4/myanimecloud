<?php

return [
    'Character' => 'Character',
    'id' => env('APP_NAME') . ' ID',
    'MAL_id' => 'MAL ID',
    'favorited_count' => 'Favorites',
    'name' => 'Name',
    'current_user_character_rank' => 'My rank',
    'character_rank' => 'User rank',
    'Role' => 'Role',
    'Gender' => 'Gender',
    'My rank' => 'My rank',
    'Sort by' => 'Sort by',
    'User rank' => 'User rank',
];
