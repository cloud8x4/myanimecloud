<?php

return [
    'User' => 'User',
    'User list' => 'User list',
    'update' => [
        'title' => 'Update ' . env('APP_NAME') . ' profile',
        'help' => 'Your username will be linked to a MAL username if it has been imported once in ' . env('APP_NAME') . '.',
        'help2' => 'You will see anime scores of this username imported from MAL.',
        'help3' => 'Your character and seiyuu rankings are linked to your e-mail address so you will not see different rankings.',
        'name' => 'Username',
        'email' => 'E-mail',
        'password' => 'Password',
        'password help' => 'Enter new password if you want to change',
        'confirm password' => 'Confirm password',
        'confirm password help' => 'Repeat new password if you want to change',
        'button' => 'Update',
        'success' => 'Successfully updated your profile',
    ],
];
