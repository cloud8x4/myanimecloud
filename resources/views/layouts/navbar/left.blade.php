<ul class="nav navbar-nav">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
           aria-expanded="false">
            {{ trans('seiyuu.Seiyuu') }}<span class="caret"></span>
        </a>
        <ul class="dropdown-menu">
            <li>
                <a href="{{ route('seiyuu_roles') }}" id="seiyuu_roles">
                    {{ trans('navbar.Roles') }}
                </a>
            </li>
            <li>
                @if (Auth::user())
                    <a href="{{ route('seiyuu_top') }}" id="seiyuu_top">
                        {{ trans('navbar.Top') }}
                    </a>
                @else
                    <span title="Login to use this feature!" class="registration-required p-l-15">
                       {{ trans('navbar.Top') }}
                   </span>
                @endif
            </li>
        <!--        <li role="separator" class="divider"></li>
        <li><a href="{{ url('/seiyuu/add') }}">Add</a></li>-->
        </ul>
    </li>
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
           aria-expanded="false">
            {{ trans('character.Character') }}<span class="caret"></span>
        </a>
        <ul class="dropdown-menu">
            <li>
                <a href="{{ route('character_roles') }}" id="character_roles">
                    {{ trans('navbar.Roles') }}
                </a>
            </li>
            <li>
                @if (Auth::user())
                    <a href="{{ route('character_top') }}" id="character_top">
                        {{ trans('navbar.Top') }}
                    </a>
                @else
                    <span title="Login to use this feature!" class="registration-required p-l-15">
                       {{ trans('navbar.Top') }}
                   </span>
                @endif
            </li>
            <li>
                @if (Auth::user())
                    <a href="/character/erabe" id="character_erabe">
                        {{ trans('navbar.erabe_game') }}
                    </a>
                @else
                    <span title="Login to use this feature!" class="registration-required p-l-15">
                       {{ trans('navbar.erabe_game') }}
                   </span>
                @endif
            </li>
        <!--        <li role="separator" class="divider"></li>
        <li><a href="{{ url('/character/add') }}">Add</a></li>-->
        </ul>
    </li>
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
           aria-expanded="false">
            {{ trans('anime.Anime') }}<span class="caret"></span>
        </a>
        <ul class="dropdown-menu">
            <li>
                <a href="{{ route('anime_roles') }}" id="anime_roles">
                    {{ trans('navbar.Roles') }}
                </a>
            </li>
            <li>
                @if (Auth::user())
                    <a href="{{ route('anime_shared') }}" id="anime_shared">
                        {{ trans('navbar.Shared') }}
                    </a>
                @else
                    <span title="Login to use this feature!" class="registration-required p-l-15">
                       {{ trans('navbar.Shared') }}
                   </span>
                @endif
            </li>
        <!--        <li role="separator" class="divider"></li>
        <li><a href="{{ url('/anime/add') }}">Add</a></li>-->
        </ul>
    </li>
    <li>
        <a href="{{ route('users') }}" id="users">
            {{ trans('navbar.Users') }}
        </a>
    </li>
<!--<li><a href="{{ url('/game') }}">Game</a></li>-->
</ul>
