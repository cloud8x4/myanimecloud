<ul class="nav navbar-nav navbar-right">
    <!-- Authentication Links -->
    @if (Auth::guest())
        <li><a href="{{ url('/login') }}">{{ trans('navbar.Login') }}</a></li>
        <li><a href="{{ url('/register') }}">{{ trans('navbar.Register') }}</a></li>
    @else
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                {{ Auth::user()->name }} <span class="caret"></span>
            </a>

            <ul class="dropdown-menu" role="menu">
                <li>
                    <a id="user_profile" href="{{ action('UserController@profile', Auth::user()) }}">
                        {{ trans('navbar.Profile') }}
                    </a>
                </li>
                <li>
                    <a id="user_import" href="/user/{{ Auth::user()->id }}/import">
                        Import
                    </a>
                </li>
                <li role="separator" class="divider"></li>
                <li>
                    <a href="{{ url('/logout') }}"
                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        {{ trans('navbar.Logout') }}
                    </a>

                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
        </li>
    @endif
</ul>
