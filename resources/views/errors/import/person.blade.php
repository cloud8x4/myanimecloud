<div>
    <b>Error code {{ $error['code'] }}</b><br/>
    Details: {{ $error['details'] }}<br/>
</div>
<br/>
<div>
    Import of anime <a href="{{ $seiyuu->present()->linkMAL }}" target="_blank"> {{ $seiyuu->title }}</a> failed.
</div>
