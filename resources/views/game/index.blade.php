@extends('layouts.app')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h1>{{ env('APP_NAME') }}: The Game</h1>
        </div>
        <div class="panel-body">
            <p> Start of Game Page </p>
        </div>
    </div>
@endsection
