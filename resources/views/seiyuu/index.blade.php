@extends('layouts.app')

@section('content')
    <div class="panel panel-default">
        @include('seiyuu.index.heading')
        @include('seiyuu.index.configuration')
        @include('layouts.paginator', ['paginator' => $seiyuu->roles])
        @include('seiyuu.index.body', ['roles' => $seiyuu->roles])
        @include('layouts.paginator', ['paginator' => $seiyuu->roles])
    </div>
@endsection
