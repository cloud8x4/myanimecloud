@extends('layouts.app')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading title">
            Add seiyuu
        </div>
        <div class="panel-body">
            <form method="post">
                {{ csrf_field() }}
                @include('form.inputtext', ['label' => 'MAL id', 'field' => 'MAL_id', 'placeholder' => 'number'])
                @include('form.inputtext', ['label' => 'Name', 'field' => 'name', 'placeholder' => 'Full name'])
                @include('form.inputtext', ['label' => 'Birthday', 'field' => 'birthday', 'placeholder' => 'yyyy-mm-dd'])
                @include('form.inputtext', ['label' => 'Image link', 'field' => 'image', 'placeholder' => 'https://link_to_image.png'])
                <button id="add" type="submit" class="btn btn-primary">Add</button>
            </form>
        </div>
    </div>
@endsection
