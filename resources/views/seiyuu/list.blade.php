@extends('layouts.app')

@section('content')
    <div class="panel panel-default">
        @include('seiyuu.list.heading')
        @include('seiyuu.list.configuration')
        @include('layouts.paginator', ['paginator' => $seiyuus])
        @include('seiyuu.list.body')
        @include('layouts.paginator', ['paginator' => $seiyuus])
    </div>
@endsection
