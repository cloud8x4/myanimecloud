<div name="seiyuuListCharacterName" class="row @if (!$settings['seiyuuListVisibilityCharacterName']) displayNone @endif">
    @foreach ($roles as $role)
        <div class="col-xs-3 col-sm-2 col-md-2 col-lg-1 p-x-4
        @if($loop->iteration > 6) hidden-sm hidden-md @endif">
            <div class="{{ strtolower($role->role) }}">{{ $role->character->name }}</div>
        </div>
    @endforeach
</div>
<div class="row">
    @foreach ($roles as $role)
        <div class="col-xs-3 col-sm-2 col-md-2 col-lg-1 p-x-4
        @if($loop->iteration > 4) hidden-xs @endif
        @if($loop->iteration > 6) hidden-sm hidden-md @endif">
            @include('character.image', ['character' => $role->character])
        </div>
    @endforeach
</div>
<div class="row">
    @foreach ($roles as $role)
        <div class="col-xs-3 col-sm-2 col-md-2 col-lg-1 p-x-4
        @if($loop->iteration > 4) hidden-xs @endif
        @if($loop->iteration > 6) hidden-sm hidden-md @endif">
            @include('anime.image', ['anime' => $role->anime])
        </div>
    @endforeach
</div>
<div name="seiyuuListAnimeTitle" class="row @if (!$settings['seiyuuListVisibilityAnimeTitle']) displayNone @endif">
    @foreach ($roles as $role)
        <div class="col-xs-3 col-sm-2 col-md-2 col-lg-1 p-x-4
        @if($loop->iteration > 6) hidden-sm hidden-md @endif">
            {{ $role->anime->title }}
        </div>
    @endforeach
</div>
