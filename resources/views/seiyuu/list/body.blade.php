<div class="panel-body">
    <div class="list-group">
        @foreach ($seiyuus as $seiyuu)
            <div class="list-group-item">
                <div class="row">
                    <div class="col-xs-4 col-sm-2">
                        @include('seiyuu.list.seiyuu')
                    </div>
                    <div class="col-xs-8 col-sm-10">
                        @include('seiyuu.list.roles', ['roles' => $seiyuu->roles])
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
