@include('seiyuu.image')
<br/>
<br/>
<div name="seiyuuListSeiyuuName"
     @if (!$settings->get('seiyuuListVisibilitySeiyuuName')) class="displayNone" @endif
>
    {{ $seiyuu->name }}
</div>
<br/>
<div name="seiyuuListSeiyuuRank"
     @if (!$settings->get('seiyuuListVisibilitySeiyuuRank')) class="displayNone" @endif
>
    @include('form.seiyuu.currentuserrank')
</div>
<div name="seiyuuListSeiyuuScoreByCharacterRanks"
     @if (!$settings->get('seiyuuListVisibilitySeiyuuScoreByCharacterRanks')) class="displayNone" @endif
>
    @include('seiyuu.score_by_character_ranks')
</div>
<br/>
<div name="seiyuuListSeiyuuFavoritedCount"
     @if (!$settings->get('seiyuuListVisibilitySeiyuuFavoritedCount')) class="displayNone" @endif
>
    {{ $seiyuu->present()->presentFavoritedCount }}
</div>
<br/>
<div name="seiyuuListSeiyuuAge" @if (!$settings->get('seiyuuListVisibilitySeiyuuAge')) class="displayNone" @endif
>
    {{ $seiyuu->present()->age }}
</div>
<div name="seiyuuListSeiyuuBirthday"
     @if (!$settings->get('seiyuuListVisibilitySeiyuuBirthday')) class="displayNone" @endif
>
    {{ $seiyuu->present()->birthdayDateLocalized }}
</div>
