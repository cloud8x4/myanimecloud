<div id="filterSeiyuu" class="panel-body collapse">
    <form id="formSeiyuu" name="formSeiyuu" action="{{ action('SeiyuuController@roles') }}">

        <div class="list-group">
            <div class="list-group-item">
                <div class="row">
                    <div class="col-sm-2">
                        <h2>{{ trans('general.Filter') }}</h2>
                    </div>
                    <div class="col-sm-5 col-md-3">
                        <h3>{{ trans('seiyuu.Seiyuu') }}</h3>
                        @include('form.seiyuu.gender', ['gender' => $filter['seiyuuGender']])
                    </div>
                    <div class="col-sm-5 col-md-7">
                        <div class="row">
                            <div class="col-md-6">
                                <h3>{{ trans('character.Character') }}</h3>
                                @include('form.character.gender', ['gender' => $filter['characterGender']])
                            </div>
                            <div class="col-md-6">
                                <h3>{{ trans('anime.Anime') }}</h3>
                                @include('form.anime.genre', ['genres' => $filter['genres']])
                                <br/>
                                @include('form.anime.season', [
                                                                'seasons' => $filter['seasons'],
                                                                'years' => $filter['years']
                                                              ]
                                )
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="list-group">
            <div class="list-group-item">
                <div class="row">
                    <div class="col-md-2">
                        <h2>{{ trans('general.Sort') }}</h2>
                    </div>
                    <div class="col-md-10">
                        @include('form.seiyuu.sort', ['user' => Auth::user()])
                    </div>
                </div>
            </div>
        </div>

        <div class="list-group">
            <div class="list-group-item">
                <div class="row">
                    <div class="col-sm-2">
                        <h2>{{ trans('general.Visibility') }}</h2>
                    </div>
                    <div class="col-sm-5 col-md-3">
                        <h3>{{ trans('seiyuu.Seiyuu') }}</h3>
                        @include('form.visibility', ['setting' => 'seiyuuListVisibilitySeiyuuName', 'label' => trans('seiyuu.name')])
                        @include('form.visibility', ['setting' => 'seiyuuListVisibilitySeiyuuRank', 'label' => trans('seiyuu.My rank')])
                        @include('form.visibility', ['setting' => 'seiyuuListVisibilitySeiyuuScoreByCharacterRanks', 'label' => trans('seiyuu.seiyuu_score_by_character_ranks')])
                        @include('form.visibility', ['setting' => 'seiyuuListVisibilitySeiyuuFavoritedCount', 'label' => trans('seiyuu.favorited_count')])
                        @include('form.visibility', ['setting' => 'seiyuuListVisibilitySeiyuuAge', 'label' => trans('seiyuu.Age')])
                        @include('form.visibility', ['setting' => 'seiyuuListVisibilitySeiyuuBirthday', 'label' => trans('seiyuu.Birthday')])
                    </div>
                    <div class="col-sm-3 col-md-7">
                        <div class="row">
                            <div class="col-md-6">
                                <h3>{{ trans('character.Character') }}</h3>
                                @include('form.visibility', ['setting' => 'seiyuuListVisibilityCharacterName', 'label' => trans('character.name')])
                            </div>
                            <div class="col-md-6">
                                <h3>{{ trans('anime.Anime') }}</h3>
                                @include('form.visibility', ['setting' => 'seiyuuListVisibilityAnimeTitle', 'label' => trans('anime.title')])
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </form>
</div>
