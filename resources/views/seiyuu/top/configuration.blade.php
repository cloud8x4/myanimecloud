<div id="filterSeiyuu" class="panel-body collapse">
    <form id="formSeiyuu" name="formSeiyuu"
          action="{{ action('SeiyuuController@top', [($user!== Auth::user()) ? $user->id : null]) }}">
        <div class="list-group">
            <div class="list-group-item">
                <div class="row">
                    <div class="col-md-2">
                        <h2>Filter</h2>
                    </div>
                    <div class="col-md-4">
                        <h3>Seiyuu</h3>
                        @include('form.seiyuu.gender', ['gender' => $filter['seiyuuGender']])
                    </div>
                </div>
            </div>
        </div>
        <div class="list-group">
            <div class="list-group-item">
                <div class="row">
                    <div class="col-md-2">
                        <h2>Sort</h2>
                    </div>
                    <div class="col-md-10">
                        @include('form.seiyuu.sort')
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
