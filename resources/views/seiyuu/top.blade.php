@extends('layouts.app')

@section('content')
    <div class="panel panel-default">
        @include('seiyuu.top.heading')
        @include('seiyuu.top.configuration')
        @include('layouts.paginator', ['paginator' => $seiyuus])
        @include('seiyuu.top.body')
        @include('layouts.paginator', ['paginator' => $seiyuus])
    </div>
@endsection
