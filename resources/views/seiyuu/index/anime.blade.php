<div class="row">
    <div class="col-sm-4">
        @include('anime.image')
    </div>
    <div class="col-sm-8">
        <div>{{ $anime->title }}</div>
        <br/>
        <div>{{ $anime->present()->myMALScore }}</div>
        <div>{{ $anime->present()->presentMembersScore }}</div>
        <br/>
        <div>{{ $anime->type }} {{ $anime->present()->presentEpisodes }}</div>
        <div>{{ $anime->present()->aired }}</div>
    </div>
</div>
