<div class="row">
    <div class="col-sm-4">
        @include('character.image', ['character' => $role->character])
    </div>
    <div class="col-sm-8">
        <div>{{ $role->character->name }}</div>
        <br/>
        @include('form.character.currentuserrank', ['character' => $role->character])
        <br/>
        <div>{{ $role->role }}</div>
        @include('form.role.weight', ['role' => $role])
    </div>
</div>
