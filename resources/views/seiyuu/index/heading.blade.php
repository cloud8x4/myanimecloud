<div class="panel-heading">
    <h1>{{ $seiyuu->name }}</h1>
</div>

<div class="panel-body">
    <div class="list-group">
        <div class="list-group-item">
            <div class="row">
                <div class="col-xs-6 col-sm-6">
                    @include('seiyuu.image')
                </div>
                <div class="col-xs-6 col-sm-6">
                    <div>
                        <a href="{{ $seiyuu->present()->linkMAL }}" target="_blank">
                            <img src="/storage/MAL.png" alt="{{ $seiyuu->name }}">
                        </a>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-sm-8 col-md-7 col-lg-6">
                            {{ trans('general.Last update') }}:
                            {{ $seiyuu->updated_at->formatLocalized(trans('carbon.dateformatform')) }}
                        </div>
                        <div class="col-sm-4 col-md-5 col-lg-6">
                            <a href="{{ action('API\Jikan\JikanPersonController@person', [$seiyuu->MAL_id]) }}"
                               target="_blank" class="btn btn-primary" role="button">
                                {{ trans('general.Import') }}
                            </a>
                        </div>
                    </div>
                    <br/>
                    @include('form.seiyuu.currentuserrank')
                    @include('seiyuu.score_by_character_ranks')
                    <div>{{ $seiyuu->present()->presentFavoritedCount }}</div>
                    <br/>
                    <div>{{ $seiyuu->present()->age }}</div>
                    <div>{{ $seiyuu->present()->birthdayDateLocalized }}</div>
                </div>
            </div>
        </div>
    </div>
</div>
