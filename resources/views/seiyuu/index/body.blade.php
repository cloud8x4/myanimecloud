<div class="panel-body">
    <div class="list-group">
        @foreach ($roles as $role)
            <div class="list-group-item">
                <div class="row">
                    <div class="col-xs-6 col-sm-6">
                        @include('seiyuu.index.character', ['role' => $role])
                    </div>
                    <div class="col-xs-6 col-sm-6">
                        @include('seiyuu.index.anime', ['anime' => $role->anime])
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
