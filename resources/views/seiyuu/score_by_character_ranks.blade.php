@if (Auth::user())
    <div>
        {{ trans('seiyuu.seiyuu_score_by_character_ranks') }} : {{ $seiyuu->present()->scoreByCharacterRanks() }}
    </div>
@else
    <div title="Login to use this feature!" class="registration-required">
        {{ trans('seiyuu.seiyuu_score_by_character_ranks') }} : N/A
    </div>
@endif
