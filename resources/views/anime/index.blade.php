@extends('layouts.app')

@section('content')
    <div class="panel panel-default">
        @include('anime.index.heading')
        @include('anime.index.configuration')
        @include('layouts.paginator', ['paginator' => $anime->roles])
        @include('anime.index.body')
        @include('layouts.paginator', ['paginator' => $anime->roles])
    </div>
@endsection
