@extends('layouts.app')

@section('content')
    <div class="panel panel-default">
        @include('anime.list.heading')
        @include('anime.list.configuration')
        @include('layouts.paginator', ['paginator' => $animes])
        @include('anime.list.body')
        @include('layouts.paginator', ['paginator' => $animes])
    </div>
@endsection
