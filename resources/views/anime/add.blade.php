@extends('layouts.app')

@section('content')
    <div class="panel-heading title">
        Add anime
    </div>

    <div class="panel-body">
        <form method="post">
            {{ csrf_field() }}
            @include('form.inputtext', ['label' => 'MAL id',     'field' => 'MAL_id',       'placeholder' => 'number'])
            @include('form.inputtext', ['label' => 'Title',      'field' => 'title',        'placeholder' => 'Title'])
            @include('form.inputtext', ['label' => 'Episodes',   'field' => 'episodes',     'placeholder' => 'number'])
            @include('form.inputtext', ['label' => 'Score',      'field' => 'members_score','placeholder' => 'number'])
            @include('form.inputtext', ['label' => 'Type',       'field' => 'type',         'placeholder' => 'TV / Special / Movie / OVA'])
            @include('form.inputtext', ['label' => 'Status',     'field' => 'status',       'placeholder' => 'Finished Airing / Currently Airing / Not Yet Aired'])
            @include('form.inputtext', ['label' => 'Start date', 'field' => 'start_date',   'placeholder' => 'yyyy-mm-dd'])
            @include('form.inputtext', ['label' => 'End date',   'field' => 'end_date',     'placeholder' => 'yyyy-mm-dd'])
            @include('form.inputtext', ['label' => 'Image link', 'field' => 'image',        'placeholder' => 'https://link_to_image.png'])
            <button id="add" type="submit" class="btn btn-primary">Add</button>
        </form>
    </div>
@endsection
