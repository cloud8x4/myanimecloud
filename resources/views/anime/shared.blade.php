@extends('layouts.app')

@section('content')
    <div class="panel panel-default">
        @include('anime.shared.heading')
        @include('anime.shared.configuration')
        @include('layouts.paginator', ['paginator' => $animes])
        @include('anime.shared.body')
        @include('layouts.paginator', ['paginator' => $animes])
    </div>
@endsection
