<div class="panel-heading">
    <h1>{{ $anime->title }}</h1>
</div>

<div class="panel-body">
    <div class="list-group">
        <div class="list-group-item">
            <div class="row">
                <div class="col-xs-6 col-sm-6">
                    @include('anime.image')
                </div>
                <div class="col-xs-6 col-sm-6">
                    <div>
                        <a href="{{ $anime->present()->linkMAL }}" target="_blank">
                            <img src="/storage/MAL.png" alt="{{ $anime->title }}">
                        </a>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-sm-8 col-md-7 col-lg-6">
                            {{ trans('general.Last update') }}:
                            {{ $anime->updated_at->formatLocalized(trans('carbon.dateformatform')) }}
                        </div>
                        <div class="col-sm-4 col-md-5 col-lg-6">
                            <a href="{{ action('API\Jikan\AnimeController@anime', [$anime->MAL_id]) }}"
                               target="_blank" class="btn btn-primary" role="button">
                                {{ trans('general.Import') }}
                            </a>
                        </div>
                    </div>
                    <br/>
                    <div>
                        <span>{{ $anime->type }}</span>
                        <span>{{ $anime->present()->presentEpisodes }}</span>
                    </div>
                    <div>{{ $anime->present()->seasonStarted }}</div>
                    <br/>
                    <div>{{ $anime->present()->myMALScore }}</div>
                    <div>{{ $anime->present()->presentMembersScore }}</div>
                </div>
            </div>
        </div>
    </div>
</div>
