<div class="row">
    @foreach($role->seiyuu->roles as $otherNoteableRole)
        <div class="col-xs-4 col-sm-2 col-md-2 col-lg-1 p-x-4
        @if($loop->iteration > 3) hidden-xs @endif
        @if($loop->iteration > 6) hidden-sm hidden-md @endif">
            {{ $otherNoteableRole->character->name }}
        </div>
    @endforeach
</div>
<div class="row">
    @foreach($role->seiyuu->roles as $otherNoteableRole)
        <div class="col-xs-4 col-sm-2 col-md-2 col-lg-1 p-x-4
        @if($loop->iteration > 3) hidden-xs @endif
        @if($loop->iteration > 6) hidden-sm hidden-md @endif">
            @include('character.image', ['character' => $otherNoteableRole->character])
        </div>
    @endforeach
</div>
<div class="row">
    @foreach($role->seiyuu->roles as $otherNoteableRole)
        <div class="col-xs-4 col-sm-2 col-md-2 col-lg-1 p-x-4
        @if($loop->iteration > 3) hidden-xs @endif
        @if($loop->iteration > 6) hidden-sm hidden-md @endif">
            @include('anime.image', ['anime' => $otherNoteableRole->anime])
        </div>
    @endforeach
</div>
<div class="row">
    @foreach($role->seiyuu->roles as $otherNoteableRole)
        <div class="col-xs-4 col-sm-2 col-md-2 col-lg-1 p-x-4
        @if($loop->iteration > 3) hidden-xs @endif
        @if($loop->iteration > 6) hidden-sm hidden-md @endif">
            {{ $otherNoteableRole->anime->title }}
        </div>
    @endforeach
</div>
