<div class="panel-body">
    <div class="list-group">
        @foreach ($anime->roles as $role)
            <div class="list-group-item">
                <div class="row">
                    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-3">
                        <div class="row">
                            <div id="character-{{ $role->character->id }}" class="col-xs-6 col-sm-6 p-r-4">
                                @include('anime.index.character', compact('role'))
                            </div>
                            <div id="seiyuu-{{ $role->seiyuu->id }}" class="col-xs-6 col-sm-6 p-l-4">
                                @include('anime.index.seiyuu', ['seiyuu' => $role->seiyuu])
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-8 col-md-8 col-lg-9">
                        @include('anime.index.roles')
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
