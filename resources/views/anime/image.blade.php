<a href="{{ $anime->present()->link }}">
    <img class="img-responsive img-wrapper" src="{{ $anime->present()->publicImage }}" alt="{{ $anime->title }}">
</a>