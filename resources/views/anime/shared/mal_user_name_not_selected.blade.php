@extends('layouts.app')

@section('content')
    <div class="panel-heading">
        <div class="row">
            <div class="col-xs-10 col-sm-10">
                <h1>Shared anime</h1>
            </div>
            <div class="col-xs-2 col-sm-2">
                <br/>
                <button data-toggle="collapse" data-target="#demo" class="pull-right btn btn-success">
                    <span class="glyphicon glyphicon-filter" aria-hidden="true"></span>
                </button>
            </div>
        </div>
    </div>

    <div id="demo" class="panel-body collapse">
        <div class="list-group">
            <div class="list-group-item">
                @include('form.mal_user_name')
            </div>
        </div>
    </div>

    <div class="panel-body">
        <div class="alert alert-warning">
            MAL username not found
        </div>
    </div>
@endsection
