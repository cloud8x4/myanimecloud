<div class="panel-body">
    <div class="list-group">
        <div class="list-group-item">
            <div class="row">
                <div class="col-xs-9 col-xs-offset-3 col-sm-10 col-sm-offset-2">
                    <div class="row">
                        <div class="col-sm-6">
                            <h3>{{ $malUser1->name }}</h3>
                        </div>
                        <div class="col-sm-6">
                            <h3>{{ $malUser2->name }}</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-9 col-xs-offset-3 col-sm-10 col-sm-offset-2">
                    <div class="row">
                        <div class="col-xs-6 col-sm-3">Watched status</div>
                        <div class="col-xs-6 col-sm-3">Watched episodes</div>
                        <div class="hidden-xs col-sm-3">Watched status</div>
                        <div class="hidden-xs col-sm-3">Watched episodes</div>
                    </div>
                </div>
            </div>
        </div>
        @foreach ($animes as $anime)
        <div class="list-group-item">
            <div class="row">
                <div class="col-xs-3 col-sm-2 p-r-0">
                    @include('anime.image')
                </div>
                <div class="col-xs-9 col-sm-10">
                    <div class="row">
                        <div class="col-xs-6 col-sm-3">{{ $anime->user1_watched_status }}</div>
                        <div class="col-xs-6 col-sm-3">{{ $anime->user1_watched_episodes }}</div>
                        <div class="col-xs-6 col-sm-3">{{ $anime->user2_watched_status }}</div>
                        <div class="col-xs-6 col-sm-3">{{ $anime->user2_watched_episodes }}</div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
