@include('anime.image')
<br/>
<br/>
<div>
    <span>
        <a href="{{ $anime->present()->linkMAL }}" target="_blank">
            <img src="/storage/MAL.png" alt="{{ $anime->title }}">
        </a>
    </span>
    <span name="animeListAnimeTitle"
          @if (!$settings->get('animeListVisibilityAnimeTitle')) class="displayNone" @endif
    >
        {{ $anime->title }}
    </span>
</div>
<br/>
<div name="animeListAnimeMyMalScore"
     @if (!$settings->get('animeListVisibilityAnimeMyMalScore')) class="displayNone" @endif
>
    {{ $anime->present()->myMALScore }}
</div>
<div name="animeListAnimeMembersScore"
     @if (!$settings->get('animeListVisibilityAnimeMembersScore')) class="displayNone" @endif
>
    {{ $anime->present()->presentMembersScore }}
</div>
<br/>
<div>
    <span name="animeListAnimeType"
          @if (!$settings->get('animeListVisibilityAnimeType')) class="displayNone" @endif
    >
            {{ $anime->type }}
        </span>
    <span name="animeListAnimeEpisodes"
          @if (!$settings->get('animeListVisibilityAnimeEpisodes')) class="displayNone" @endif
    >
            {{ $anime->present()->presentEpisodes }}
        </span>
</div>
<div name="animeListAnimeSeasonStarted"
     @if (!$settings->get('animeListVisibilityAnimeSeasonStarted')) class="displayNone" @endif
>
    {{ $anime->present()->seasonStarted }}
</div>
