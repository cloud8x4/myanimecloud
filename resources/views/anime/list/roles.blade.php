<div name="animeListCharacterName" class="row @if (!$settings['animeListVisibilityCharacterName']) displayNone @endif">
    @foreach ($roles as $role)
        <div class="col-xs-3 col-sm-2 col-md-2 col-lg-1 p-x-4
        @if($loop->iteration > 6) hidden-sm hidden-md @endif">
            <div class="{{ strtolower($role->role) }}">{{ $role->character->name }}</div>
        </div>
    @endforeach
</div>
<div name="animeListCharacterGender"
     class="row @if (!$settings['animeListVisibilityCharacterGender']) displayNone @endif">
    @foreach ($roles as $role)
        <div class="col-xs-3 col-sm-2 col-md-2 col-lg-1 p-x-4
        @if($loop->iteration > 4) hidden-xs @endif
        @if($loop->iteration > 6) hidden-sm hidden-md @endif">
            @include('form.character.female_or_male', ['character' => $role->character])
        </div>
    @endforeach
</div>
<div class="row">
    @foreach ($roles as $role)
        <div class="col-xs-3 col-sm-2 col-md-2 col-lg-1 p-x-4
        @if($loop->iteration > 4) hidden-xs @endif
        @if($loop->iteration > 6) hidden-sm hidden-md @endif">
            @include('character.image', ['character' => $role->character])
        </div>
    @endforeach
</div>
<div class="row">
    @foreach ($roles as $role)
        <div class="col-xs-3 col-sm-2 col-md-2 col-lg-1 p-x-4
        @if($loop->iteration > 4) hidden-xs @endif
        @if($loop->iteration > 6) hidden-sm hidden-md @endif">
            @include('seiyuu.image', ['seiyuu' => $role->seiyuu])
        </div>
    @endforeach
</div>
<div name="animeListSeiyuuGender" class="row @if (!$settings['animeListVisibilitySeiyuuGender']) displayNone @endif">
    @foreach ($roles as $role)
        <div class="col-xs-3 col-sm-2 col-md-2 col-lg-1 p-x-4
        @if($loop->iteration > 4) hidden-xs @endif
        @if($loop->iteration > 6) hidden-sm hidden-md @endif">
            @include('form.seiyuu.female_or_male', ['seiyuu' => $role->seiyuu])
        </div>
    @endforeach
</div>
<div name="animeListSeiyuuName" class="row @if (!$settings['animeListVisibilitySeiyuuName']) displayNone @endif">
    @foreach ($roles as $role)
        <div class="col-xs-3 col-sm-2 col-md-2 col-lg-1 p-x-4
        @if($loop->iteration > 6) hidden-sm hidden-md @endif">
            {{ $role->seiyuu->name }}
        </div>
    @endforeach
</div>
