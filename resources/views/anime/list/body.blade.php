<div class="panel-body">
    <div class="list-group">
        @foreach ($animes as $anime)
            <div class="list-group-item">
                <div class="row">
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-2">
                        @include('anime.list.anime')
                    </div>
                    <div class="col-xs-8 col-sm-9 col-md-9 col-lg-10">
                        @include('anime.list.roles', ['roles' => $anime->roles])
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
