<div id="filterAnimes" class="panel-body collapse">
    <form id="formAnimes" name="formAnimes" action="anime">
        <div class="list-group">

            <div class="list-group-item">
                <div class="row">
                    <div class="col-sm-2">
                        <h2>{{ trans('general.Filter') }}</h2>
                    </div>
                    <div class="col-sm-5 col-md-3">
                        <h3>{{ trans('anime.Anime') }}</h3>
                        @include('form.anime.genre', ['genres' => $filter['genres']])
                        <br/>
                        @include('form.anime.season', [
                                                        'seasons' => $filter['seasons'],
                                                        'years' => $filter['years']
                                                      ]
                        )
                        <br/>
                        @include('form.anime.status', ['statuses' => $filter['statuses']])
                        <br/>
                        @include('form.anime.classification', ['classifications' => $filter['classifications']])
                        <br/>
                        @include('form.anime.type')
                    </div>
                    <div class="col-sm-3 col-md-7">
                        <div class="row">
                            <div class="col-md-6">
                                <h3>MyAnimeList</h3>
                                @include('form.anime.watched_status')
                            </div>
                            <div class="col-md-6">
                                <h3>
                                    {{ trans('character.Character') }}
                                </h3>
                                @include('form.character.gender', ['gender' => $filter['characterGender']])
                                <h3>
                                    {{ trans('seiyuu.Seiyuu') }}
                                </h3>
                                @include('form.seiyuu.gender', ['gender' => $filter['seiyuuGender']])
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="list-group">
            <div class="list-group-item">
                <div class="row">
                    <div class="col-sm-2">
                        <h2>Sort</h2>
                    </div>
                    <div class="col-sm-10">
                        @include('form.anime.sort')
                    </div>
                </div>
            </div>
        </div>

        <div class="list-group">
            <div class="list-group-item">
                <div class="row">
                    <div class="col-sm-2">
                        <h2>Visibility</h2>
                    </div>
                    <div class="col-sm-5 col-md-3">
                        <h3>{{ trans('Anime') }}</h3>
                        @include('form.visibility', ['setting' => 'animeListVisibilityAnimeTitle', 'label' => trans('anime.title')])
                        @include('form.visibility', ['setting' => 'animeListVisibilityAnimeType', 'label' => trans('anime.Type')])
                        @include('form.visibility', ['setting' => 'animeListVisibilityAnimeEpisodes', 'label' => trans('anime.Episodes')])
                        @include('form.visibility', ['setting' => 'animeListVisibilityAnimeSeasonStarted', 'label' => trans('anime.Season Started')])
                        @include('form.visibility', ['setting' => 'animeListVisibilityAnimeMyMalScore', 'label' => trans('anime.mal_animes.score')])
                        @include('form.visibility', ['setting' => 'animeListVisibilityAnimeMembersScore', 'label' => trans('anime.members_score')])
                    </div>
                    <div class="col-sm-3 col-md-7">
                        <div class="row">
                            <div class="col-md-6">
                                <h3>{{ trans('character.Character') }}</h3>
                                @include('form.visibility', ['setting' => 'animeListVisibilityCharacterName', 'label' => trans('character.name')])
                                @include('form.visibility', ['setting' => 'animeListVisibilityCharacterGender', 'label' => trans('character.Gender')])
                            </div>
                            <div class="col-md-6">
                                <h3>{{ trans('seiyuu.Seiyuu') }}</h3>
                                @include('form.visibility', ['setting' => 'animeListVisibilitySeiyuuName', 'label' => trans('seiyuu.name')])
                                @include('form.visibility', ['setting' => 'animeListVisibilitySeiyuuGender', 'label' => trans('seiyuu.Gender')])
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </form>
</div>
