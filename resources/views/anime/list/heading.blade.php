<div class="panel-heading">
    <div class="row">
        <div class="col-xs-10 col-sm-10">
            <h1>Anime</h1>
        </div>
        <div class="col-xs-2 col-sm-2">
            <br/>
            <button id="filterAnimesButton" data-toggle="collapse" data-target="#filterAnimes"
                    class="pull-right btn btn-success">
                <span class="glyphicon glyphicon-filter" aria-hidden="true"></span>
            </button>
        </div>
    </div>
</div>
