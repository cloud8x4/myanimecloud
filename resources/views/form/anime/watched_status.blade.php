<input id="watchedStatus" name="animeWatchedStatus" type="hidden">
<div class="input-group">
    <span id="basic-addon1"
          @if (Auth::user()) class="input-group-addon"
          @else class="input-group-addon registration-required" title="Login to use this feature!" @endif
    >
        Watched Status
    </span>
    <div class="dropdown">
        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            {{ $filter->get('animeWatchedStatus') }}
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
            <li class="watchedStatusSelect" data-value="Do not apply"><a href="#">Do not apply</a></li>
            <li class="watchedStatusSelect" data-value="In my list"><a href="#">In my list</a></li>
            <li class="watchedStatusSelect" data-value="Not in my list"><a href="#">Not in my list</a></li>
            <li role="separator" class="divider"></li>
            <li class="watchedStatusSelect" data-value="Watching"><a href="#">Watching</a></li>
            <li class="watchedStatusSelect" data-value="Plan to Watch"><a href="#">Plan to Watch</a></li>
            <li role="separator" class="divider"></li>
            <li class="watchedStatusSelect" data-value="On-Hold"><a href="#">On-Hold</a></li>
            <li class="watchedStatusSelect" data-value="Completed"><a href="#">Completed</a></li>
            <li role="separator" class="divider"></li>
            <li class="watchedStatusSelect" data-value="Dropped"><a href="#">Dropped</a></li>
            <li class="watchedStatusSelect" data-value="Not gonna watch"><a href="#">Not gonna watch</a></li>
        </ul>
    </div>
</div>
