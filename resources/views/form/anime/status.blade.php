<input id="status" name="animeStatus" type="hidden">
<div class="input-group">
    <span class="input-group-addon" id="basic-addon1">Status</span>
    <div class="dropdown">
        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            {{ $filter->get('animeStatus') }}
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
            @foreach($statuses as $status)
                <li class="statusSelect" data-value="{{ $status }}">
                    <a href="#">{{ trans('anime.'.$status) }}</a>
                </li>
            @endforeach
        </ul>
    </div>
</div>
