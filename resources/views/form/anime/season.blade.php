<input id="season" name="animeSeason" type="hidden">
<input id="year" name="animeYear" type="hidden">
<div class="input-group">
    <span class="input-group-addon" id="basic-addon1">{{ trans('anime.start_date') }}</span>
    <div class="dropdown">
        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuSortBy"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            {{ trans('general.'.$filter['animeSeason']) }}
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenuSortBy">
            @foreach($seasons as $season)
                <li class="seasonSelect" data-value="{{ $season }}"><a href="#">{{ trans('general.'.$season) }}</a></li>
            @endforeach
        </ul>
    </div>
    <div class="dropdown">
        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            {{ $filter['animeYear'] }}
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu multi-column columns-3" aria-labelledby="dropdownMenu1">
            <div class="row">
                @foreach($years as $year)
                    <div class="col-xs-12 col-sm-4">
                        <ul class="multi-column-dropdown">
                            <li class="yearSelect" data-value="{{ $year }}">
                                <a href="#">
                                    @if (is_int($year))
                                        {{ $year }}
                                    @else
                                        {{ trans('general.'.$year) }}
                                    @endif
                                </a>
                            </li>
                        </ul>
                    </div>
                @endforeach
            </div>
        </ul>
    </div>
</div>
