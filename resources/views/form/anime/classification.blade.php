<input id="classification" name="animeClassification" type="hidden">
<div class="input-group">
    <span class="input-group-addon" id="basic-addon1">Classification</span>
    <div class="dropdown">
        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            {{ $filter->get('animeClassification') }}
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
            @foreach($classifications as $classification)
                <li class="classificationSelect" data-value="{{ $classification }}">
                    <a href="#">{{ trans('anime.'.$classification) }}</a>
                </li>
            @endforeach
        </ul>
    </div>
</div>
