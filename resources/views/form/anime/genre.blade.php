<input id="genre" name="animeGenre" type="hidden">
<div class="input-group">
    <span class="input-group-addon" id="basic-addon1">Genre</span>
    <div class="dropdown">
        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            {{ $filter->get('animeGenre') }}
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu multi-column columns-3" aria-labelledby="dropdownMenu1">
            <div class="row">
                @foreach($genres as $genre)
                    <div class="col-xs-12 col-sm-4">
                        <ul class="multi-column-dropdown">
                            <li class="genreSelect" data-value="{{ $genre }}"><a href="#">{{ $genre }}</a></li>
                        </ul>
                    </div>
                @endforeach
            </div>
        </ul>
    </div>
</div>
