<input id="sortBy" name="sortBy" type="hidden">
<input id="sortOrder" name="sortOrder" type="hidden">
<div class="input-group">
    <span class="input-group-addon" id="basic-addon1">Sort by</span>
    <div class="dropdown">
        <button class="btn btn-default dropdown-toggle"
                type="button"
                id="dropdownMenuSortBy"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="true"
        >
            {{ trans('anime.'.$sort->get('field')) }}
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenuSortBy">
            <li>
                <a href="#"
                   @if (Auth::user()) class="sortBySelect" data-value="mal_animes.score"
                   @else title="Login to use this feature!" class="registration-required" @endif
                >
                    {{ trans('anime.mal_animes.score') }}
                </a>
            </li>
            <li>
                <a href="#" class="sortBySelect" data-value="members_score">
                    {{ trans('anime.members_score') }}
                </a>
            </li>
            <li>
                <a href="#" class="sortBySelect" data-value="start_date">
                    {{ trans('anime.start_date') }}
                </a>
            </li>
            <li>
                <a href="#" class="sortBySelect" data-value="animes.updated_at">
                    {{ trans('anime.animes.updated_at') }}
                </a>
            </li>
            <li>
                <a href="#" class="sortBySelect" data-value="MAL_id">
                    {{ trans('anime.MAL_id') }}
                </a>
            </li>
            <li>
                <a href="#" class="sortBySelect" data-value="id">
                    {{ trans('anime.id') }}
                </a>
            </li>
        </ul>
    </div>
    <div class="dropdown">
        <button
                class="btn btn-default dropdown-toggle"
                type="button"
                id="dropdownMenuSortOrder"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="true"
        >
            {{ trans('general.'.$sort->get('order')) }}
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenuSortOrder">
            <li>
                <a href="#" class="sortOrderSelect" data-value="asc">
                    {{ trans('general.asc') }}
                </a>
            </li>
            <li>
                <a href="#" class="sortOrderSelect" data-value="desc">
                    {{ trans('general.desc') }}
                </a>
            </li>
        </ul>
    </div>
</div>
