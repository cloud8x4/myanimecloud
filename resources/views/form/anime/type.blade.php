<input id="type" name="animeType" type="hidden">
<div class="input-group">
    <span class="input-group-addon" id="basic-addon1">Type</span>
    <div class="dropdown">
        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            {{ $filter->get('animeType') }}
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
            <li class="typeSelect" data-value="All types"><a href="#">All types</a></li>
            <li class="typeSelect" data-value="TV"><a href="#">TV</a></li>
            <li class="typeSelect" data-value="Movie"><a href="#">Movie</a></li>
            <li class="typeSelect" data-value="OVA"><a href="#">OVA</a></li>
            <li class="typeSelect" data-value="Special"><a href="#">Special</a></li>
            <li class="typeSelect" data-value="ONA"><a href="#">ONA</a></li>
            <li class="typeSelect" data-value="Music"><a href="#">Music</a></li>
            <li class="typeSelect" data-value="Empty"><a href="#">Empty</a></li>
        </ul>
    </div>
</div>
