<form class="navbar-form navbar-left" action="/search">
    <div class="form-group">
        <input name="search" type="text" class="form-control" placeholder="Search">
    </div>
    <button type="submit" class="btn btn-default">
        <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
    </button>
</form>
