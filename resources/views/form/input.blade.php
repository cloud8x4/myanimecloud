<div class="form-group">
    <div class="row">
        <label class="col-sm-3 col-md-3 col-lg-2 form-control-label">{{ $label }}</label>
        <div class="col-sm-7 col-md-5 col-lg-3">
            <p class="form-control-static">
                <input type="{{ $type }}" class="form-control @if ($errors->has($field))error @endif" id="{{ $field }}"
                       name="{{ $field }}" value="{{ old($field) ?? $initialValue ?? null }}"
                       aria-describedby="{{ $field }}Help" placeholder="{{ $placeholder }}">
            </p>
            @if ($errors->has($field))
                <div class="divError">{{ $errors->first($field) }}</div>
            @endif
            <small id="{{ $field }}Help" class="form-text text-muted">
                {{ $help ?? null }}
            </small>
        </div>
    </div>
</div>
