<input id="characterGender" name="characterGender" type="hidden">
<div class="input-group">
    <span class="input-group-addon" id="basic-addon1">{{ trans('character.Gender') }}</span>
    <div class="dropdown">
        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            {{ $gender }}
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
            <li class="characterGenderSelect" data-value="All genders">
                <a href="#">{{ trans('general.All genders') }}</a>
            </li>
            <li class="characterGenderSelect" data-value="Female">
                <a href="#">{{ trans('general.Female') }}</a>
            </li>
            <li class="characterGenderSelect" data-value="Male">
                <a href="#">{{ trans('general.Male') }}</a>
            </li>
            <li class="characterGenderSelect" data-value="No gender">
                <a href="#">{{ trans('general.No gender') }}</a>
            </li>
            <li class="characterGenderSelect" data-value="Unknown">
                <a href="#">{{ trans('general.Unknown') }}</a>
            </li>
        </ul>
    </div>
</div>
