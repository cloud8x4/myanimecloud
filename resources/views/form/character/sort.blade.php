<input id="characterSortBy" name="characterSortBy" type="hidden">
<input id="characterSortOrder" name="characterSortOrder" type="hidden">
<div class="input-group">
    <span class="input-group-addon" id="basic-addon1">{{ trans('character.Sort by') }}</span>
    <div class="dropdown">
        <button class="btn btn-default dropdown-toggle"
                type="button"
                id="dropdownMenuSortBy"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="true"
        >
            {{ trans('character.'.$sort['field']) }}
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenuSortBy">
            <li>
                <a href="#"
                   @if (Auth::user()) class="characterSortBySelect" data-value="current_user_character_rank"
                   @else title="Login to use this feature!" class="registration-required" @endif
                >
                    {{ trans('character.My rank') }}
                </a>
            </li>
            @if ($user !== Auth::user())
                <li>
                    <a href="#" class="characterSortBySelect" data-value="character_rank">
                        {{ trans('character.User rank') }}
                    </a>
                </li>
            @endif
            <li>
                <a href="#" class="characterSortBySelect" data-value="MAL_id">
                    {{ trans('character.MAL_id') }}
                </a>
            </li>
            <li>
                <a href="#" class="characterSortBySelect" data-value="id">
                    {{ trans('character.id') }}
                </a>
            </li>
        </ul>
    </div>
    <div class="dropdown">
        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuSortOrder"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            {{ trans('general.'.$sort['order']) }}
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenuSortOrder">
            <li>
                <a href="#" class="characterSortOrderSelect" data-value="asc">
                    {{ trans('general.asc') }}
                </a>
            </li>
            <li>
                <a href="#" class="characterSortOrderSelect" data-value="desc">
                    {{ trans('general.desc') }}
                </a>
            </li>
        </ul>
    </div>
</div>
