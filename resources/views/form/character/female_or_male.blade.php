<div class="btn-group" role="group">
    <button type="button" class="btn btn-default characterFemaleOrMaleSelect
            @if ($character->gender == 'male') active @endif"
            data-id="{{ $character->id }}" data-gender="male">♂
    </button>
    <button type="button" class="btn btn-default characterFemaleOrMaleSelect
            @if ($character->gender == 'female') active @endif"
            data-id="{{ $character->id }}" data-gender="female">♀
    </button>
</div>
