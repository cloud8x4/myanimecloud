@if (Auth::user())
    <div name="characterCurrentRank" class="pointer">
        {{ trans('My rank') }}: {{ $character->present()->myRank() }}
    </div>
    <input type="number"
           min="1"
           name="characterNewRank"
           data-id="{{ $character->id }}"
           class="form-control displayNone"
    >
@else
    <div title="Login to use this feature!" class="registration-required">
        {{ trans('My rank') }}: N/A
    </div>
@endif
