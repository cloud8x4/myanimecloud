<div class="checkbox">
    <label>
        <input id="{{ $setting }}" name="{{ $setting }}" type="checkbox" @if ($settings[$setting]) checked @endif>
        {{ $label }}
    </label>
</div>
