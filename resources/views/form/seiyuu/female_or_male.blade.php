<div class="btn-group" role="group">
    <button type="button" class="btn btn-default seiyuufemaleOrMaleSelect
            @if ($seiyuu->gender == 'male') active @endif"
            data-id="{{ $seiyuu->id }}" data-gender="male">♂
    </button>
    <button type="button" class="btn btn-default seiyuufemaleOrMaleSelect
            @if ($seiyuu->gender == 'female') active @endif"
            data-id="{{ $seiyuu->id }}" data-gender="female">♀
    </button>
</div>
