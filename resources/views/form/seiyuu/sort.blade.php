<input id="seiyuuSortBy" name="seiyuuSortBy" type="hidden">
<input id="seiyuuSortOrder" name="seiyuuSortOrder" type="hidden">
<div class="input-group">
    <span class="input-group-addon" id="basic-addon1">{{ trans('seiyuu.Sort by') }}</span>
    <div class="dropdown">
        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuSeiyuuSortBy"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            {{ trans('seiyuu.'.$sort->get('field')) }} <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenuSeiyuuSortBy">
            <li>
                <a href="#"
                   @if (Auth::user()) data-value="current_user_seiyuu_rank" class="seiyuuSortBySelect"
                   @else title="Login to use this feature!" class="registration-required" @endif
                >
                    {{ trans('seiyuu.My rank') }}
                </a>
            </li>
            @if ($user !== Auth::user())
                <li>
                    <a href="#" class="seiyuuSortBySelect" data-value="seiyuu_rank">
                        {{ trans('seiyuu.User rank') }}
                    </a>
                </li>
            @endif
            <li>
                <a href="#"
                   @if (Auth::user()) class="seiyuuSortBySelect" data-value="seiyuu_score_by_character_ranks"
                   @else title="Login to use this feature!" class="registration-required" @endif
                >
                    {{ trans('seiyuu.seiyuu_score_by_character_ranks') }}
                </a>
            </li>
            <li>
                <a href="#" class="seiyuuSortBySelect" data-value="favorited_count">
                    {{ trans('seiyuu.favorited_count') }}
                </a>
            </li>
            <li>
                <a href="#" class="seiyuuSortBySelect" data-value="MAL_id">
                    {{ trans('seiyuu.MAL_id') }}
                </a>
            </li>
            <li>
                <a href="#" class="seiyuuSortBySelect" data-value="id">
                    {{ trans('seiyuu.id') }}
                </a>
            </li>
        </ul>
    </div>
    <div class="dropdown">
        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuSeiyuuSortOrder"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            {{ trans('general.'.$sort->get('order')) }}
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenuSeiyuuSortOrder">
            <li>
                <a href="#" class="seiyuuSortOrderSelect" data-value="asc">
                    {{ trans('general.asc') }}
                </a>
            </li>
            <li>
                <a href="#" class="seiyuuSortOrderSelect" data-value="desc">
                    {{ trans('general.desc') }}
                </a>
            </li>
        </ul>
    </div>
</div>
