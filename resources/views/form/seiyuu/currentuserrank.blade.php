@if (Auth::user())
    <div name="currentRank" class="pointer">
        My rank: {{ $seiyuu->present()->myRank }}
    </div>
    <input type="number" min="1" name="newRank" data-id="{{ $seiyuu->id }}" class="form-control displayNone">
@else
    <div title="Login to use this feature!" class="registration-required">
        My rank: N/A
    </div>
@endif
