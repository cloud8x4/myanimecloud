@if (Auth::user())
    <div name="roleWeight" class="pointer">
        Weight: {{ $role->weight }} %
    </div>
    <input type="number"
           min="1"
           max="100"
           name="roleNewWeight"
           data-id="{{ $role->id }}"
           class="form-control displayNone"
    >
@else
    <div title="Login to use this feature!" class="registration-required">
        Weight: {{ $role->weight }} %
    </div>
@endif
