<form class="input-group" action="/anime/shared">
    {{ method_field('POST') }}
    <div class="input-group">
        <span class="input-group-addon" id="basic-addon1">MAL user name</span>
        <input name="malUserName" value="@if (!empty($malUser2)) {{ $malUser2->name }} @endif" type="text" class="form-control"
               placeholder="MAL username">
    </div>
</form>
