@extends('layouts.app')

@section('content')
    <input id="app-name" name="app-name" type="hidden" value="{{ env('APP_NAME') }}">
    <div id="app">
        Vue has not been loaded
    </div>
@endsection
