@extends('layouts.app')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading title">
            {{ trans('user.update.title') }}
        </div>

        <div class="panel-body">
            <div>
                {{ trans('user.update.help') }}<br/>
                {{ trans('user.update.help2') }}<br/>
                {{ trans('user.update.help3') }}
            </div>
            <br/>
            @if (!empty($updateResult))
                <div class="alert alert-{{ $updateResult }}">
                    {{ trans('user.update.'.$updateResult) }}
                </div>
                <br/>
            @endif
            <form method="post" action="{{ action('UserController@update') }}">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                @include('form.input', [
                                'type' => 'text',
                                'label' => trans('user.update.name'),
                                'field' => 'name',
                                'placeholder' => trans('user.update.name'),
                                'initialValue' => $name
                             ]
                 )
                @include('form.input', [
                                'type' => 'email',
                                'label' => trans('user.update.email'),
                                'field' => 'email',
                                'placeholder' => trans('user.update.email'),
                                'initialValue' => $email
                             ]
                 )
                @include('form.input', [
                                'type' => 'password',
                                'label' => trans('user.update.password'),
                                'field' => 'password',
                                'placeholder' => trans('user.update.password'),
                                'help' => trans('user.update.password help')
                             ]
                 )
                @include('form.input', [
                                'type' => 'password',
                                'label' => trans('user.update.confirm password'),
                                'field' => 'password_confirmation',
                                'placeholder' => trans('user.update.confirm password'),
                                'help' => trans('user.update.confirm password help')
                             ]
                 )
                <button id="update" type="submit" class="btn btn-primary">{{ trans('user.update.button') }}</button>
            </form>
        </div>
    </div>
@endsection
