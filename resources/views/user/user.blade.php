@extends('layouts.app')

@section('content')
    <div class="panel panel-default">
        @include('user.user.heading')
        @include('user.user.body')
    </div>
@endsection
