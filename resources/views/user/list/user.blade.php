<div class="card">
    <a href="{{ action('UserController@user', [$user->id]) }}" style="text-decoration: none; color: inherit;">
        <div class="card-body">
            <h5 class="card-title">{{ $user->name }}</h5>
            <h6 class="card-subtitle mb-2 text-muted">
                <div>{{ env('APP_NAME') }} ID: {{ $user->id }}</div>
            </h6>
            <p class="card-text"></p>
        </div>
    </a>
</div>
