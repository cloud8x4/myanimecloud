<div class="panel-body">
    <div class="list-group">
        <div class="row">
            @foreach ($users as $user)
                <div class="col-sm-6 col-md-4 col-lg-3">
                    @include('user.list.user')
                    <br/>
                </div>
            @endforeach
        </div>
    </div>
</div>
