@extends('layouts.app')

@section('content')
    <div class="panel panel-default">
        @include('user.list.heading')
        @include('layouts.paginator', ['paginator' => $users])
        @include('user.list.body')
        @include('layouts.paginator', ['paginator' => $users])
    </div>
@endsection
