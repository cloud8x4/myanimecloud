<div class="panel-body">
    <div class="list-group">
        <div>
            MyAnimeList profile:
            <a href="{{ $user->present()->linkMAL }}" target="_blank">
                <img src="/storage/MAL.png">
            </a>
        </div>
        <br/>
        <div>Joined: {{ $user->present()->joined }}</div>
        <br/>
        <div>
            <a href="{{ $user->present()->linkCharacterTop }}" class="btn btn-primary">Character top</a>
            <a href="{{ $user->present()->linkSeiyuuTop }}" class="btn btn-primary">Seiyuu top</a>
        </div>
    </div>
</div>
