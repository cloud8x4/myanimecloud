<div class="panel-body">
    <div class="list-group">
        @foreach ($animes as $anime)
            <div class="list-group-item">
                @include('search.list.anime')
            </div>
        @endforeach
    </div>
</div>
