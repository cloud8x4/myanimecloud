<div class="row">
    <div class="col-xs-6 col-sm-3 col-md-2">
        @include('anime.image')
    </div>
    <div class="col-xs-6 col-sm-9 col-md-10">
        <div>{{ $anime->title }}</div>
        <br/>
        <div>{{ $anime->type }} {{ $anime->present()->presentEpisodes }}</div>
        <div>{{ $anime->present()->seasonStarted }}</div>
        <br/>
        <div>{{ $anime->present()->myMALScore }}</div>
        <div>{{ $anime->present()->presentMembersScore }}</div>
    </div>
</div>

