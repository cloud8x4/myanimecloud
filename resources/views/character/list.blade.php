@extends('layouts.app')

@section('content')
    <div class="panel panel-default">
        @include('character.list.heading')
        @include('character.list.configuration')
        @include('layouts.paginator', ['paginator' => $characters])
        @include('character.list.body')
        @include('layouts.paginator', ['paginator' => $characters])
    </div>
@endsection
