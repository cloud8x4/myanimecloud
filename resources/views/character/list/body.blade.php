<div class="panel-body">
    <div class="list-group">
        @foreach ($characters as $character)
            <div class="list-group-item">
                <div class="row">
                    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-2">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 p-r-4">
                                @include('character.list.character')
                            </div>
                            <div class="col-xs-6 col-sm-6 p-l-4">
                                @include('character.list.seiyuu', ['seiyuus' => $character->seiyuus])
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-9 col-md-9 col-lg-10">
                        @include('character.list.animes')
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
