@foreach ($seiyuus as $seiyuu)
    @include('seiyuu.image', ['seiyuu' => $seiyuu])
    <br/>
    <br/>
    <div name="characterRolesSeiyuuName"
         @if (!$settings['characterRolesVisibilitySeiyuuName']) class="displayNone" @endif
    >
        {{ $seiyuu->name }}
    </div>
    <br/>
    <div name="characterRolesSeiyuuRank"
         @if (!$settings['characterRolesVisibilitySeiyuuRank']) class="displayNone" @endif
    >
        @include('form.seiyuu.currentuserrank', ['seiyuu' => $seiyuu])
    </div>
    <div name="characterRolesSeiyuuScoreByCharacterRanks"
         @if (!$settings['characterRolesVisibilitySeiyuuScoreByCharacterRanks']) class="displayNone" @endif
    >
        @include('seiyuu.score_by_character_ranks', ['seiyuu' => $seiyuu])
    </div>
    <br/>
@endforeach
