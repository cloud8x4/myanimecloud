<div id="filterCharacter" class="panel-body collapse">
    <form id="formCharacter" name="formCharacter" action="{{ action('CharacterController@roles') }}">

        <div class="list-group">
            <div class="list-group-item">
                <div class="row">
                    <div class="col-md-2">
                        <h2>{{ trans('general.Filter') }}</h2>
                    </div>
                    <div class="col-md-4">
                        <h3>{{ trans('character.Character') }}</h3>
                        @include('form.character.gender', ['gender' => $filter['characterGender']])
                    </div>
                </div>
            </div>
        </div>

        <div class="list-group">
            <div class="list-group-item">
                <div class="row">
                    <div class="col-md-2">
                        <h2>{{ trans('general.Sort') }}</h2>
                    </div>
                    <div class="col-md-10">
                        @include('form.character.sort', ['user' => Auth::user()])
                    </div>
                </div>
            </div>
        </div>

        <div class="list-group">
            <div class="list-group-item">
                <div class="row">
                    <div class="col-sm-2">
                        <h2>{{ trans('general.Visibility') }}</h2>
                    </div>
                    <div class="col-sm-5 col-md-3">
                        <h3>{{ trans('character.Character') }}</h3>
                        @include('form.visibility', ['setting' => 'characterRolesVisibilityCharacterName', 'label' => trans('character.name')])
                        @include('form.visibility', ['setting' => 'characterRolesVisibilityCharacterRole', 'label' => trans('character.Role')])
                        @include('form.visibility', ['setting' => 'characterRolesVisibilityCharacterRank', 'label' => trans('character.My rank')])
                    </div>
                    <div class="col-sm-3 col-md-7">
                        <div class="row">
                            <div class="col-md-6">
                                <h3>{{ trans('seiyuu.Seiyuu') }}</h3>
                                @include('form.visibility', ['setting' => 'characterRolesVisibilitySeiyuuName', 'label' => trans('seiyuu.name')])
                                @include('form.visibility', ['setting' => 'characterRolesVisibilitySeiyuuRank', 'label' => trans('seiyuu.My rank')])
                                @include('form.visibility', ['setting' => 'characterRolesVisibilitySeiyuuScoreByCharacterRanks', 'label' => trans('seiyuu.seiyuu_score_by_character_ranks')])
                            </div>
                            <div class="col-md-6">
                                <h3>{{ trans('anime.Anime') }}</h3>
                                @include('form.visibility', ['setting' => 'characterRolesVisibilityAnimeTitle', 'label' => trans('anime.title')])
                                @include('form.visibility', ['setting' => 'characterRolesVisibilityAnimeMyMalScore', 'label' => trans('anime.mal_animes.score')])
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </form>
</div>
