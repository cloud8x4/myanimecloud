@include('character.image')
<br/>
<br/>
<div name="characterRolesCharacterName"
     @if (!$settings['characterRolesVisibilityCharacterName']) class="displayNone" @endif
>
    {{ $character->name }}
</div>
<div name="characterRolesCharacterRole"
     @if (!$settings['characterRolesVisibilityCharacterRole']) class="displayNone" @endif
>
    {{ $character->roles()->first()->role }}
</div>
<div name="characterRolesCharacterRank"
     @if (!$settings['characterRolesVisibilityCharacterRank'])class="displayNone" @endif
>
    <br/>
    @include('form.character.currentuserrank')
</div>
