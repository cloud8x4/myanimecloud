<div class="row">
    @foreach ($character->animes as $anime)
        <div class="col-xs-4 col-sm-2 col-md-2 col-lg-1 p-x-4
        @if($loop->iteration > 3) hidden-xs @endif
        @if($loop->iteration > 6) hidden-sm hidden-md @endif">
            @include('anime.image')
        </div>
    @endforeach
</div>
<div name="characterRolesAnimeTitle"
     class="row @if (!$settings['characterRolesVisibilityAnimeTitle']) displayNone @endif">
    @foreach ($character->animes as $anime)
        <div class="col-xs-4 col-sm-2 col-md-2 col-lg-1 p-x-4
        @if($loop->iteration > 3) hidden-xs @endif
        @if($loop->iteration > 6) hidden-sm hidden-md @endif">
            <div>{{ $anime->title }}</div>
        </div>
    @endforeach
</div>
<div name="characterRolesAnimeMyMalScore"
     class="row @if (!$settings['characterRolesVisibilityAnimeMyMalScore']) displayNone @endif">
    <br/>
    @foreach ($character->animes as $anime)
        <div class="col-xs-4 col-sm-2 col-md-2 col-lg-1 p-x-4
        @if($loop->iteration > 3) hidden-xs @endif
        @if($loop->iteration > 6) hidden-sm hidden-md @endif">
            <div>{{ $anime->present()->myMALScore }}</div>
        </div>
    @endforeach
</div>
