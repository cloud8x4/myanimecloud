@extends('layouts.app')

@section('content')
    <div class="panel panel-default">
        @include('character.top.heading')
        @include('character.top.configuration')
        @include('layouts.paginator', ['paginator' => $characters])
        @include('character.top.body')
        @include('layouts.paginator', ['paginator' => $characters])
    </div>
@endsection
