<div class="row">
    @foreach ($animes as $anime)
        <div class="col-xs-6 col-sm-6 col-md-4 col-lg-3">
            @include('anime.image')
            <br/>
            <br/>
            <div>{{ $anime->title }}</div>
            <br/>
            <div>{{ $anime->present()->myMALScore }}</div>
            <div>{{ $anime->present()->presentMembersScore }}</div>
            <br/>
            <div>{{ $anime->type }} {{ $anime->present()->presentEpisodes }}</div>
            <div>{{ $anime->present()->aired }}</div>
        </div>
    @endforeach
</div>
