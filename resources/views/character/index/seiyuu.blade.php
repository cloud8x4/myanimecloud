<div class="row">
    <div class="col-xs-6 col-sm-12">
        @include('seiyuu.image', ['seiyuu' => $role->seiyuu])
    </div>
    <div class="col-xs-6 col-sm-12">
        <div>{{ $role->seiyuu->name }}</div>
        <br/>
        @include('form.seiyuu.currentuserrank', ['seiyuu' => $role->seiyuu])
        @include('seiyuu.score_by_character_ranks', ['seiyuu' => $role->seiyuu])
        <br/>
        <div>{{ $role->role }}</div>
        @include('form.role.weight', ['role' => $role])
        <br/>
        <div>{{ $role->seiyuu->present()->presentFavoritedCount }}</div>
        <br/>
        <div>{{ $role->seiyuu->present()->age }}</div>
        <div>{{ $role->seiyuu->present()->birthdayDateLocalized }}</div>
    </div>
</div>
