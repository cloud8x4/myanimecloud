<div class="panel-heading">
    <h1>{{ $character->name }}</h1>
</div>
<div class="panel-body">
    <div class="list-group">
        <div class="list-group-item">
            <div class="row">
                <div class="col-xs-7 col-sm-6">
                    @include('character.image')
                </div>
                <div class="col-xs-5 col-sm-6">
                    <div>
                        <a href="{{ $character->present()->linkMAL }}" target="_blank">
                            <img src="/storage/MAL.png" alt="{{ $character->name }}">
                        </a>
                    </div>
                    <br/>
                    @include('form.character.currentuserrank')
                </div>
            </div>
        </div>
    </div>
</div>
