<div class="panel-body">
    <div class="list-group">
        @foreach ($roles as $role)
            <div class="list-group-item">
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-lg-3">
                        @include('character.index.seiyuu', [compact('role')])
                    </div>
                    <div class="col-xs-12 col-sm-8 col-lg-9">
                        @include('character.index.animes', ['animes' => $role->animes])
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
