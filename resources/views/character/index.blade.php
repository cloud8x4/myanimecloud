@extends('layouts.app')

@section('content')
    <div class="panel panel-default">
        @include('character.index.heading', compact('character'))
        @include('character.index.configuration')
        @include('character.index.body', compact('roles') )
    </div>
@endsection
