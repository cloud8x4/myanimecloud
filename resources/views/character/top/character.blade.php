@include('character.image')
<br/>
<br/>
<div>{{ $character->name }}</div>
<br/>
@if (Auth::user() !== $user)
    @include('character.userrank')
@endif
@include('form.character.currentuserrank')
<br/>
