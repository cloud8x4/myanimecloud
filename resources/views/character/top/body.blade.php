<div class="panel-body">
    <div class="list-group">
        <div class="list-group-item">
            @foreach ($characters as $character)
                @if ($loop->index %12 == 0)
                    <div class="row">
                        @endif
                        <div class="col-xs-6 col-sm-2 col-md-1 p-x-4">
                            @include('character.top.character')
                        </div>
                        @if ($loop->index %12 == 11)
                    </div>
                @endif
            @endforeach
        </div>
    </div>
</div>
