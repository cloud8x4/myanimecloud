<div id="filterCharacter" class="panel-body collapse">
    <form id="formCharacter" name="formCharacter"
          action="{{ action('CharacterController@top', [($user!== Auth::user()) ? $user->id : null]) }}">
        <div class="list-group">
            <div class="list-group-item">
                <div class="row">
                    <div class="col-md-2">
                        <h2>Filter</h2>
                    </div>
                    <div class="col-md-4">
                        <h3>Character</h3>
                        @include('form.character.gender', ['gender' => $filter['characterGender']])
                    </div>
                </div>
            </div>
        </div>
        <div class="list-group">
            <div class="list-group-item">
                <div class="row">
                    <div class="col-md-2">
                        <h2>Sort</h2>
                    </div>
                    <div class="col-md-10">
                        @include('form.character.sort')
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
