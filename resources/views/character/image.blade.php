<a href="{{ $character->present()->link }}">
    <img class="img-responsive img-wrapper" src="{{ $character->present()->publicImage }}" alt="{{ $character->name }}">
</a>
