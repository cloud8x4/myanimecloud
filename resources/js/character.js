/**
 * Filter
 */

$('.characterGenderSelect').click(function () {
    $('#characterGender').val($(this).data('value'))
    $(`#${$(this).closest('form').attr('id')}`).submit()
})

$('div[name=characterCurrentRank]').click(function () {
    $(this).siblings('input[name=characterNewRank]')
        .show()
        .focus()
})

$('input[name=characterNewRank]').keypress(function (e) {
    if (e.which === 13) {
        const input = this
        $.ajax({
            type: 'POST',
            url: '/api/character/rank',
            data: {
                _token: $('meta[name=csrf-token]').attr('content'),
                id: $(this).data('id'),
                rank: $(this).val(),
            },
            dataType: 'json',
            success: function (data) {
                if (data.newRank) {
                    $(input).siblings('div[name=characterCurrentRank]').html(`My rank: ${data.newRank}`)
                }
                $(input).hide()
                $(input).val('')
            },
        })
    }
})

/**
 * Sort
 */

$('.characterSortBySelect').click(function () {
    $('#characterSortBy').val($(this).data('value'))
    $(`#${$(this).closest('form').attr('id')}`).submit()
})

$('.characterSortOrderSelect').click(function () {
    $('#characterSortOrder').val($(this).data('value'))
    $(`#${$(this).closest('form').attr('id')}`).submit()
})

/* global setUserSetting */

/**
 * Visibility
 */

$('input[name=characterRolesVisibilityCharacterName]').click(function () {
    setUserSetting('characterRolesVisibilityCharacterName', $(this).prop('checked'))
    if ($(this).prop('checked')) {
        $('div[name=characterRolesCharacterName]').show()
    } else {
        $('div[name=characterRolesCharacterName]').hide()
    }
})

$('input[name=characterRolesVisibilityCharacterRole]').click(function () {
    setUserSetting('characterRolesVisibilityCharacterRole', $(this).prop('checked'))
    if ($(this).prop('checked')) {
        $('div[name=characterRolesCharacterRole]').show()
    } else {
        $('div[name=characterRolesCharacterRole]').hide()
    }
})

$('input[name=characterRolesVisibilitySeiyuuName]').click(function () {
    setUserSetting('characterRolesVisibilitySeiyuuName', $(this).prop('checked'))
    if ($(this).prop('checked')) {
        $('div[name=characterRolesSeiyuuName]').show()
    } else {
        $('div[name=characterRolesSeiyuuName]').hide()
    }
})

$('input[name=characterRolesVisibilitySeiyuuRank]').click(function () {
    setUserSetting('characterRolesVisibilitySeiyuuRank', $(this).prop('checked'))
    if ($(this).prop('checked')) {
        $('div[name=characterRolesSeiyuuRank]').show()
    } else {
        $('div[name=characterRolesSeiyuuRank]').hide()
    }
})

$('input[name=characterRolesVisibilitySeiyuuScoreByCharacterRanks]').click(function () {
    setUserSetting('characterRolesVisibilitySeiyuuScoreByCharacterRanks', $(this).prop('checked'))
    if ($(this).prop('checked')) {
        $('div[name=characterRolesSeiyuuScoreByCharacterRanks]').show()
    } else {
        $('div[name=characterRolesSeiyuuScoreByCharacterRanks]').hide()
    }
})

$('input[name=characterRolesVisibilityCharacterRank]').click(function () {
    setUserSetting('characterRolesVisibilityCharacterRank', $(this).prop('checked'))
    if ($(this).prop('checked')) {
        $('div[name=characterRolesCharacterRank]').show()
    } else {
        $('div[name=characterRolesCharacterRank]').hide()
    }
})

$('input[name=characterRolesVisibilityAnimeTitle]').click(function () {
    setUserSetting('characterRolesVisibilityAnimeTitle', $(this).prop('checked'))
    if ($(this).prop('checked')) {
        $('div[name=characterRolesAnimeTitle]').show()
    } else {
        $('div[name=characterRolesAnimeTitle]').hide()
    }
})

$('input[name=characterRolesVisibilityAnimeMyMalScore]').click(function () {
    setUserSetting('characterRolesVisibilityAnimeMyMalScore', $(this).prop('checked'))
    if ($(this).prop('checked')) {
        $('div[name=characterRolesAnimeMyMalScore]').show()
    } else {
        $('div[name=characterRolesAnimeMyMalScore]').hide()
    }
})

/**
 * Other
 */

$('.characterFemaleOrMaleSelect').click(function () {
    $.ajax({
        type: 'POST',
        url: '/api/character/gender',
        data: {
            _token: $('meta[name=csrf-token]').attr('content'),
            id: $(this).data('id'),
            gender: $(this).data('gender'),
        },
        dataType: 'json',
    })

    $(this).addClass('active')
    $(this).siblings().removeClass('active')
})
