import moment from 'moment'
import router from './router'
import Vuetify from 'vuetify'

require('./bootstrap')

window.Vue = require('vue')

require('vue-resource')

Vue.use(moment)
Vue.use(Vuetify)

new Vue({
    el: '#app',
    router,
    components: { Root: () => import('./Root.vue') },
    template: '<Root/>',
    vuetify: new Vuetify(),
})
