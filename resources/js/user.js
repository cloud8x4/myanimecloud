
function setUserSetting (name, value) {
    $.ajax({
        type: 'POST',
        url: '/api/user/setting',
        data: {
            _token: $('meta[name=csrf-token]').attr('content'),
            name: name,
            value: value,
        },
        dataType: 'json',
    })
}
