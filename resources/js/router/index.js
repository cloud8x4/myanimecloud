import Router from 'vue-router'
import Vue from 'vue'

Vue.use(Router)

const generateRoutes = () => {
    return [
        {
            path: '/dashboard',
            name: 'Dashboard',
            component: () => import('../pages/Dashboard'),
        },
        {
            path: '/user/:user',
            name: 'User',
            component: () => import('../pages/User'),
            children: [
                {
                    path: 'import',
                    name: 'User import',
                    component: () => import('../pages/User/Import'),
                },
            ],
        },
        {
            path: '/character/erabe',
            name: 'Character Erabe Game',
            component: () => import('../pages/Character/Erabe'),
        },
        {
            path: '/search',
            name: 'Search',
            component: () => import('../pages/Search'),
        },
        {
            path: '/',
            name: 'Welcome',
            component: () => import('../pages/Welcome'),
        },
        {
            path: '*',
            component: () => import('./NotFound'),
        },
    ]
}

export default new Router({
    mode: 'history',
    base: '/',
    routes: generateRoutes(),
})
