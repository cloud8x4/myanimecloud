/**
 * Filter
 */

$('.genreSelect').click(function () {
    $('#genre').val($(this).data('value'))
    $(`#${$(this).closest('form').attr('id')}`).submit()
})

$('.seasonSelect').click(function () {
    $('#season').val($(this).data('value'))
    $(`#${$(this).closest('form').attr('id')}`).submit()
})

$('.yearSelect').click(function () {
    $('#year').val($(this).data('value'))
    $(`#${$(this).closest('form').attr('id')}`).submit()
})

$('.statusSelect').click(function () {
    $('#status').val($(this).data('value'))
    $(`#${$(this).closest('form').attr('id')}`).submit()
})

$('.classificationSelect').click(function () {
    $('#classification').val($(this).data('value'))
    $(`#${$(this).closest('form').attr('id')}`).submit()
})

$('.typeSelect').click(function () {
    $('#type').val($(this).data('value'))
    $(`#${$(this).closest('form').attr('id')}`).submit()
})

$('.watchedStatusSelect').click(function () {
    $('#watchedStatus').val($(this).data('value'))
    $(`#${$(this).closest('form').attr('id')}`).submit()
})

/**
 * Sort
 */

$('.sortBySelect').click(function () {
    $('#sortBy').val($(this).data('value'))
    $(`#${$(this).closest('form').attr('id')}`).submit()
})

$('.sortOrderSelect').click(function () {
    $('#sortOrder').val($(this).data('value'))
    $(`#${$(this).closest('form').attr('id')}`).submit()
})

/* global setUserSetting */

/**
 * Visibility
 */

$('input[name=animeListVisibilityAnimeTitle]').click(function () {
    setUserSetting('animeListVisibilityAnimeTitle', $(this).prop('checked'))
    if ($(this).prop('checked')) {
        $('span[name=animeListAnimeTitle]').show()
    } else {
        $('span[name=animeListAnimeTitle]').hide()
    }
})

$('input[name=animeListVisibilityAnimeType]').click(function () {
    setUserSetting('animeListVisibilityAnimeType', $(this).prop('checked'))
    if ($(this).prop('checked')) {
        $('span[name=animeListAnimeType]').show()
    } else {
        $('span[name=animeListAnimeType]').hide()
    }
})

$('input[name=animeListVisibilityAnimeEpisodes]').click(function () {
    setUserSetting('animeListVisibilityAnimeEpisodes', $(this).prop('checked'))
    if ($(this).prop('checked')) {
        $('span[name=animeListAnimeEpisodes]').show()
    } else {
        $('span[name=animeListAnimeEpisodes]').hide()
    }
})

$('input[name=animeListVisibilityAnimeSeasonStarted]').click(function () {
    setUserSetting('animeListVisibilityAnimeSeasonStarted', $(this).prop('checked'))
    if ($(this).prop('checked')) {
        $('div[name=animeListAnimeSeasonStarted]').show()
    } else {
        $('div[name=animeListAnimeSeasonStarted]').hide()
    }
})

$('input[name=animeListVisibilityAnimeMyMalScore]').click(function () {
    setUserSetting('animeListVisibilityAnimeMyMalScore', $(this).prop('checked'))
    if ($(this).prop('checked')) {
        $('div[name=animeListAnimeMyMalScore]').show()
    } else {
        $('div[name=animeListAnimeMyMalScore]').hide()
    }
})

$('input[name=animeListVisibilityAnimeMembersScore]').click(function () {
    setUserSetting('animeListVisibilityAnimeMembersScore', $(this).prop('checked'))
    if ($(this).prop('checked')) {
        $('div[name=animeListAnimeMembersScore]').show()
    } else {
        $('div[name=animeListAnimeMembersScore]').hide()
    }
})

$('input[name=animeListVisibilityCharacterName]').click(function () {
    setUserSetting('animeListVisibilityCharacterName', $(this).prop('checked'))
    if ($(this).prop('checked')) {
        $('div[name=animeListCharacterName]').show()
    } else {
        $('div[name=animeListCharacterName]').hide()
    }
})

$('input[name=animeListVisibilityCharacterGender]').click(function () {
    setUserSetting('animeListVisibilityCharacterGender', $(this).prop('checked'))
    if ($(this).prop('checked')) {
        $('div[name=animeListCharacterGender]').show()
    } else {
        $('div[name=animeListCharacterGender]').hide()
    }
})

$('input[name=animeListVisibilitySeiyuuName]').click(function () {
    setUserSetting('animeListVisibilitySeiyuuName', $(this).prop('checked'))
    if ($(this).prop('checked')) {
        $('div[name=animeListSeiyuuName]').show()
    } else {
        $('div[name=animeListSeiyuuName]').hide()
    }
})

$('input[name=animeListVisibilitySeiyuuGender]').click(function () {
    setUserSetting('animeListVisibilitySeiyuuGender', $(this).prop('checked'))
    if ($(this).prop('checked')) {
        $('div[name=animeListSeiyuuGender]').show()
    } else {
        $('div[name=animeListSeiyuuGender]').hide()
    }
})
