$('div[name=roleWeight]').click(function () {
    $(this).siblings('input[name=roleNewWeight]')
        .show()
        .focus()
})

$('input[name=roleNewWeight]').keypress(function (e) {
    if (e.which === 13) {
        const input = this
        $.ajax({
            type: 'POST',
            url: '/api/role/weight',
            data: {
                _token: $('meta[name=csrf-token]').attr('content'),
                id: $(this).data('id'),
                weight: $(this).val(),
            },
            dataType: 'json',
            success: function (data) {
                if (data.newWeight) {
                    $(input).siblings('div[name=roleWeight]').html(`Weight: ${data.newWeight} %`)
                }
                $(input).hide()
                $(input).val('')
            },
        })
    }
})
