/**
 * Filter
 */

$('.seiyuuGenderSelect').click(function () {
    $('#seiyuuGender').val($(this).data('value'))
    $(`#${$(this).closest('form').attr('id')}`).submit()
})

$('div[name=currentRank]').click(function () {
    $(this).siblings('input[name=newRank]')
        .show()
        .focus()
})

$('input[name=newRank]').keypress(function (e) {
    if (e.which === 13) {
        const input = this
        $.ajax({
            type: 'POST',
            url: '/api/seiyuu/rank/',
            data: {
                _token: $('meta[name=csrf-token]').attr('content'),
                id: $(this).data('id'),
                rank: $(this).val(),
            },
            dataType: 'json',
            success: function (data) {
                if (data.newRank) {
                    $(input).siblings('div[name=currentRank]').html(`My rank: ${data.newRank}`)
                }
                $(input).hide()
                $(input).val('')
            },
        })
    }
})

/**
 * Sort
 */

$('.seiyuuSortBySelect').click(function () {
    $('#seiyuuSortBy').val($(this).data('value'))
    $(`#${$(this).closest('form').attr('id')}`).submit()
})

$('.seiyuuSortOrderSelect').click(function () {
    $('#seiyuuSortOrder').val($(this).data('value'))
    $(`#${$(this).closest('form').attr('id')}`).submit()
})

/* global setUserSetting */

/**
 * Visibility
 */

$('input[name=seiyuuListVisibilitySeiyuuName]').click(function () {
    setUserSetting('seiyuuListVisibilitySeiyuuName', $(this).prop('checked'))
    if ($(this).prop('checked')) {
        $('div[name=seiyuuListSeiyuuName]').show()
    } else {
        $('div[name=seiyuuListSeiyuuName]').hide()
    }
})

$('input[name=seiyuuListVisibilitySeiyuuRank]').click(function () {
    setUserSetting('seiyuuListVisibilitySeiyuuRank', $(this).prop('checked'))
    if ($(this).prop('checked')) {
        $('div[name=seiyuuListSeiyuuRank]').show()
    } else {
        $('div[name=seiyuuListSeiyuuRank]').hide()
    }
})

$('input[name=seiyuuListVisibilitySeiyuuScoreByCharacterRanks]').click(function () {
    setUserSetting('seiyuuListVisibilitySeiyuuScoreByCharacterRanks', $(this).prop('checked'))
    if ($(this).prop('checked')) {
        $('div[name=seiyuuListSeiyuuScoreByCharacterRanks]').show()
    } else {
        $('div[name=seiyuuListSeiyuuScoreByCharacterRanks]').hide()
    }
})

$('input[name=seiyuuListVisibilitySeiyuuFavoritedCount]').click(function () {
    setUserSetting('seiyuuListVisibilitySeiyuuFavoritedCount', $(this).prop('checked'))
    if ($(this).prop('checked')) {
        $('div[name=seiyuuListSeiyuuFavoritedCount]').show()
    } else {
        $('div[name=seiyuuListSeiyuuFavoritedCount]').hide()
    }
})

$('input[name=seiyuuListVisibilitySeiyuuAge]').click(function () {
    setUserSetting('seiyuuListVisibilitySeiyuuAge', $(this).prop('checked'))
    if ($(this).prop('checked')) {
        $('div[name=seiyuuListSeiyuuAge]').show()
    } else {
        $('div[name=seiyuuListSeiyuuAge]').hide()
    }
})

$('input[name=seiyuuListVisibilitySeiyuuBirthday]').click(function () {
    setUserSetting('seiyuuListVisibilitySeiyuuBirthday', $(this).prop('checked'))
    if ($(this).prop('checked')) {
        $('div[name=seiyuuListSeiyuuBirthday]').show()
    } else {
        $('div[name=seiyuuListSeiyuuBirthday]').hide()
    }
})

$('input[name=seiyuuListVisibilityCharacterName]').click(function () {
    setUserSetting('seiyuuListVisibilityCharacterName', $(this).prop('checked'))
    if ($(this).prop('checked')) {
        $('div[name=seiyuuListCharacterName]').show()
    } else {
        $('div[name=seiyuuListCharacterName]').hide()
    }
})

$('input[name=seiyuuListVisibilityAnimeTitle]').click(function () {
    setUserSetting('seiyuuListVisibilityAnimeTitle', $(this).prop('checked'))
    if ($(this).prop('checked')) {
        $('div[name=seiyuuListAnimeTitle]').show()
    } else {
        $('div[name=seiyuuListAnimeTitle]').hide()
    }
})

/**
 * Other
 */

$('.seiyuufemaleOrMaleSelect').click(function () {
    $.ajax({
        type: 'POST',
        url: '/api/seiyuu/gender',
        data: {
            _token: $('meta[name=csrf-token]').attr('content'),
            id: $(this).data('id'),
            gender: $(this).data('gender'),
        },
        dataType: 'json',
    })

    $(this).addClass('active')
    $(this).siblings().removeClass('active')
})
