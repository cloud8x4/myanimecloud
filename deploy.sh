
# Install tool to use command ifconfig
sudo apt install net-tools

# Get the IP and 
ifconfig

# Login remotely before continuing

# Change password
passwd

# Enable sudo without password
sudo sh -c "echo 'service ALL=(ALL) NOPASSWD:ALL' > /etc/sudoers.d/service"

# Update apt sources
sudo apt update

# Update entire system
sudo apt upgrade

# Enable auto updates (not sure if this had effect)
sudo sh -c 'echo "unattended-upgrades       unattended-upgrades/enable_auto_updates boolean true" | sudo debconf-set-selections; dpkg-reconfigure -f noninteractive unattended-upgrades'

# TODO: add code to autoremove and autoclean
# Install NTP
sudo apt install ntp

# Install mariadb
sudo apt install mariadb-server

# This script changes some of the less secure default options for things like remote root logins and sample users.
# This time I set a root password which may not have worked. Next time Do not set root password since we will remove the root user later anyway.
sudo mysql_secure_installation

# After this it seems root login does not work with the provided password in previous command. Set a password using the following.
# Maybe not happen if you do not set root password in previous.
sudo mariadb
SELECT user,authentication_string,plugin,host FROM mysql.user;
UPDATE mysql.user SET plugin = 'mysql_native_password' WHERE user = 'root' AND plugin = 'unix_socket';
ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '<your_password>';
FLUSH PRIVILEGES;
SELECT user,authentication_string,plugin,host FROM mysql.user;
# See the difference?

# Set service MariaDB password
read -s SERVICEMYSQLPASSWORD
# Create MariaDB service user
mysql -u root -p"$SERVICEMYSQLPASSWORD" -e "GRANT ALL ON *.* TO 'service'@'127.0.0.1' IDENTIFIED BY '$SERVICEMYSQLPASSWORD' WITH GRANT OPTION; FLUSH PRIVILEGES;"
mysql -u root -p"$SERVICEMYSQLPASSWORD" -e "GRANT ALL ON *.* TO 'service'@'localhost' IDENTIFIED BY '$SERVICEMYSQLPASSWORD' WITH GRANT OPTION; FLUSH PRIVILEGES;"
# Remove MariaDB root user
mysql -u service -p"$SERVICEMYSQLPASSWORD" -e "use mysql; delete from user where user='root'; flush privileges;"
# Create MariaDB MyAnimeCloud database
mysql -u service -p"$SERVICEMYSQLPASSWORD" -e "CREATE DATABASE myanimecloud DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;"
# Set MyAnimeCloud MariaDB password
read -s MYANIMECLOUDMYSQLPASSWORD
# Set service MariaDB password
read -s $MYANIMECLOUDMYSQLPASSWORD
# Create MariaDB MyAnimeCloud user
mysql -u service -p"$SERVICEMYSQLPASSWORD" -e "GRANT ALL PRIVILEGES ON myanimecloud.* TO myanimecloud@'127.0.0.1' IDENTIFIED BY '$MYANIMECLOUDMYSQLPASSWORD';"

# Installing supervisor
sudo apt install supervisor

# Install redis
sudo apt install redis-server

# Install PHP 7.3.
sudo apt install \
php7.3          php7.3-bcmath    php7.3-cli  php7.3-common   php7.3-curl      php7.3-fpm        php7.3-gd \
php7.3-imap     php7.3-intl      php7.3-ldap php7.3-json     php7.3-mbstring  php7.3-memcached  php7.3-mysql \
php7.3-opcache  php7.3-readline  php7.3-soap php7.3-sqlite3  php7.3-xml       php7.3-zip        php-pear

# Install nginx
sudo apt install nginx
# Clear default indexhtml
sudo sh -c 'echo -n > /usr/share/nginx/html/index.html'

# Only if your provider does not offer a firewall service outside the OS, activate UFW
sudo ufw allow 22/tcp
sudo ufw allow 80/tcp
sudo ufw allow 443/tcp
sudo ufw --force enable

# Install locales. This enables you to use PHP's setlocale() function.
sudo locale-gen nl_NL.UTF-8
sudo dpkg-reconfigure locales # Not sure what to select here

# Prepare website location
sudo mkdir -p /var/www/myanimecloud.com/shared
sudo mkdir -p /var/www/myanimecloud.com/shared/storage/logs
sudo chown -R service /var/www/myanimecloud.com

# Configure supervisor
sudo groupadd supervisor
# Replace permission 0700 to 0770 and add chown=root:supervisor/g in a new line
sudo sed -i 's/chmod=0700                       ; sockef file mode (default 0700)/chmod=0770                       ; sockef file mode (default 0700)\nchown=root:supervisor/g' /etc/supervisor/supervisord.conf
sudo service supervisor restart
touch /var/www/myanimecloud.com/shared/storage/logs/supervisor.log
cat << 'EOF' > /tmp/queue.conf
[program:queue]
user=www-data
command=php artisan queue:listen --tries=1
directory=/var/www/myanimecloud.com/
stdout_logfile=/var/www/myanimecloud.com/shared/storage/logs/supervisor.log
redirect_stderr=true
autostart=true
autorestart=true
EOF

sudo mv /tmp/queue.conf /etc/supervisor/conf.d/queue.conf

# Configure cron
echo "* * * * * www-data /usr/bin/php /var/www/myanimecloud.com/artisan schedule:run >> /dev/null 2>&1" > /tmp/myanimecloud
sudo mv /tmp/myanimecloud /etc/cron.d/myanimecloud
sudo chown root:root /etc/cron.d/myanimecloud
sudo chmod 644 /etc/cron.d/myanimecloud

# Enable HTTP site config
cat << 'EOF' > /tmp/default
server {
    listen 80 default_server;
    listen [::]:80 default_server;
    server_name _;
    return 301 https://$host$request_uri;
}
EOF
sudo mv /tmp/default /etc/nginx/sites-available
sudo rm -f /etc/nginx/sites-enabled/default
sudo ln -s /etc/nginx/sites-available/default /etc/nginx/sites-enabled

# Enable ssl site config
sudo mkdir -p /etc/nginx/ssl
sudo openssl dhparam -out /etc/nginx/ssl/dhparams.pem 2048
sudo openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -subj "/C=/ST=/L=/O=/CN=myanimecloud.com" -keyout /etc/nginx/ssl/myanimecloud.com.key -out /etc/nginx/ssl/myanimecloud.com.cer
cat << 'EOF' > /tmp/myanimecloud.com
server {
    listen      443 ssl;
    server_name myanimecloud.com;
    root   /var/www/myanimecloud.com/public;

    access_log off;
    error_log  /var/log/nginx/myanimecloud.com-error.log error;

    ssl_certificate /etc/nginx/ssl/myanimecloud.com.cer;
    ssl_certificate_key /etc/nginx/ssl/myanimecloud.com.key;

    ssl_session_timeout  5m;

    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
    ssl_ciphers 'ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:AES:CAMELLIA:DES-CBC3-SHA:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!aECDH:!EDH-DSS-DES-CBC3-SHA:!EDH-RSA-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA:!3DES';
    ssl_prefer_server_ciphers on;
    ssl_dhparam /etc/nginx/ssl/dhparams.pem;

    charset utf-8;

    index index.php index.html index.htm;
    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }

    location = /favicon.ico { access_log off; log_not_found off; }
    location = /robots.txt  { access_log off; log_not_found off; }

    sendfile off;

    client_max_body_size 100m;

    location ~ \.php$ {
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass unix:/run/php/php7.3-fpm.sock;
        fastcgi_index index.php;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;

        fastcgi_intercept_errors off;
        fastcgi_buffer_size 16k;
        fastcgi_buffers 4 16k;
        fastcgi_connect_timeout 300;
        fastcgi_send_timeout 300;
        fastcgi_read_timeout 300;
    }

    location ~ /\.ht {
        deny all;
    }
}
EOF
sudo mv /tmp/myanimecloud.com /etc/nginx/sites-available
sudo ln -s /etc/nginx/sites-available/myanimecloud.com /etc/nginx/sites-enabled
sudo systemctl restart nginx

# ******************************************
# Install MyAnimeCloud application
# ******************************************

# Add SSH key to connect with Bitbucket
mkdir .ssh && cd .ssh
# Copy ssh key pair to this directory or generate a new one
sudo chmod 600 id_rsa*

ln -f -s /var/www/myanimecloud.com
cd myanimecloud.com
git clone git@bitbucket.org:cloud8x4/myanimecloud.git
mv myanimecloud/* .
mv myanimecloud/.* .
rmdir myanimecloud
git checkout production

sudo apt install composer
composer install

# Install yarn
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt update
sudo apt install yarn

# Install a certificate - source: https://www.digitalocean.com/community/tutorials/how-to-secure-nginx-with-let-s-encrypt-on-ubuntu-20-04
sudo apt install certbot python3-certbot-nginx
sudo certbot --nginx -d myanimecloud.com -d www.myanimecloud.com

# All done so reboot
sudo shutdown -r now
