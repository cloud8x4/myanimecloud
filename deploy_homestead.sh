# Clone this repository on your Homestead image
# In your code directory (e.g D:\Homestead\code)
git clone https://<username>@bitbucket.org/cloud8x4/myanimecloud.git

# Start you VM
vagrant up

# Only after you modified your vagrant instance in your homestead.yaml file
vagrant reload --provision

# Login to your VM
vagrant ssh

# Go to your myanimecloud application directory (e.g D:\homestead_code\myanimecloud)
cd Code/myanimecloud
