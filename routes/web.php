<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

use App\Http\Controllers\AnimeController;
use App\Http\Controllers\API\Jikan\AnimeController as JikanAnimeController;
use App\Http\Controllers\API\Jikan\JikanPersonController;
use App\Http\Controllers\API\MyAnimeList\UserController as MyAnimeListUserController;
use App\Http\Controllers\API\PostController;
use App\Http\Controllers\API\SearchController as ApiSearchController;
use App\Http\Controllers\CharacterController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\SeiyuuController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\VueController;
use App\Jobs\ImportAnime;
use App\Jobs\ImportSeiyuu;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Public API
Route::group([
    'prefix' => 'api',
], function () {
    Route::get('/posts', [PostController::class, 'index']);

    Route::post('/search', [ApiSearchController::class, 'search']);
});

Route::get('/anime', [AnimeController::class, 'roles'])->name('anime_roles');
Route::get('/anime/{id}', [AnimeController::class, 'anime'])->where('id', '[0-9]+');

Route::get('/character', [CharacterController::class, 'roles'])->name('character_roles');
Route
    ::get('/character/top/{user?}', [CharacterController::class, 'top'])
    ->where('user', '[0-9]+')
    ->name('character_top');
Route::get('/character/{id}', [CharacterController::class, 'character'])->where('id', '[0-9]+');

Route::get('/dashboard', [VueController::class, 'index'])->name('dashboard');

Route::get('/search', [SearchController::class, 'search']);

Route::get('/seiyuu', [SeiyuuController::class, 'roles'])->name('seiyuu_roles');
Route::get('/seiyuu/top/{user?}', [SeiyuuController::class, 'top'])->where('user', '[0-9]+')->name('seiyuu_top');
Route::get('/seiyuu/{id}', [SeiyuuController::class, 'seiyuu'])->where('id', '[0-9]+');

Route::get('/user', [UserController::class, 'list'])->name('users');
Route::get('/user/{user}', [UserController::class, 'user'])->where('user', '[0-9]+');

// API Jikan
Route::get('/import/anime/{malId}/characters', [JikanAnimeController::class, 'characters'])->where('mal_id', '[0-9]+');
Route::get('/import/anime/{malId}', [JikanAnimeController::class, 'anime'])->where('mal_id', '[0-9]+');
Route::get('/import/person/{mal_id}', [JikanPersonController::class, 'person'])->where('mal_id', '[0-9]+');

// API MyAnimeList
Route::get('/import/users/{username}/animelist', [MyAnimeListUserController::class, 'animelist']);

Route::group(['middleware' => 'auth'], function () {
    Route::get('/seiyuu/add', [SeiyuuController::class, 'add']);

    Route::get('/character/add', [CharacterController::class, 'add']);

    Route::get('/anime/add', [AnimeController::class, 'add']);
    Route::get('/anime/shared', [AnimeController::class, 'shared'])->name('anime_shared');
    Route::post('/anime/shared', [AnimeController::class, 'shared']);

    Route::get('/game', 'GameController@game');

    Route::get('/user/{user}/profile', [UserController::class, 'profile'])->where('user', '[0-9]+');
    Route::patch('/user/profile', [UserController::class, 'update']);

    // Authenticated API
    Route::group([
        'prefix' => 'api',
    ], function () {
        Route::get('/user/character/random', 'API\UserController@randomCharacter');

        Route::get('/user/last-mal-import', 'API\UserController@lastMalImport')->name('user_last_mal_import');

        Route::post('/anime/add', 'AnimeController@postForm');

        Route::post('/character/add', 'API\CharacterController@postForm')->name('character_post');
        Route::post('/character/gender', 'API\CharacterController@postGender')->name('character_gender');
        Route::post('/character/rank', 'API\CharacterController@rank')->name('character_rank');

        Route::post('/role/weight', 'API\RoleController@weight')->name('role_weight');

        Route::post('/seiyuu/add', 'API\SeiyuuController@postForm')->name('seiyuu_post');
        Route::post('/seiyuu/gender', 'API\SeiyuuController@postGender')->name('seiyuu_gender');
        Route::post('/seiyuu/rank', 'API\SeiyuuController@rank')->name('seiyuu_rank');

        Route::post('/user/import', 'API\UserController@import');
        Route::post('/user/setting', 'API\UserController@setSetting');
    });

    Route
        ::get('/{vue_capture?}', [VueController::class, 'index'])
        ->where('vue_capture', '[\/\w\.-]*');

    // TODO: remove following routes when automation is completed
    // These routes are handy for testing but should not remain
    Route::get('/runImportAnime', function () {
        dispatch(new ImportAnime());

        return '';
    });
    Route::get('/runImportSeiyuu', function () {
        dispatch(new ImportSeiyuu());

        return '';
    });
});
