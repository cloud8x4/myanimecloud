<?php

namespace App\Console;

use App\Jobs\ImportAnime;
use App\Jobs\ImportSeiyuu;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            dispatch(new ImportAnime());
        })
            ->cron(env('SCHEDULE_IMPORT_ANIME_CRON'))
            ->name('importAnime')
            ->withoutOverlapping();

        $schedule->call(function () {
            dispatch(new ImportSeiyuu());
        })
            ->cron(env('SCHEDULE_IMPORT_SEIYUU_CRON'))
            ->name('importSeiyuu')
            ->withoutOverlapping();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
