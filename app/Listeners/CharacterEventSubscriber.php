<?php

namespace App\Listeners;

use App\Events\CharacterRankChanged;
use App\Events\RoleWeightChanged;
use App\Models\Handlers\SeiyuuHandler;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Events\Dispatcher;

class CharacterEventSubscriber implements ShouldQueue
{
    /**
     * Handle character rank changed events.
     * @param mixed $event
     */
    public function handleCharacterRankChanged($event)
    {
        $seiyuuHandler = new SeiyuuHandler();

        $seiyuuHandler->calculateScores($event->user);
    }

    /**
     * Handle role weight changed events.
     * @param mixed $event
     */
    public function handleRoleWeightChanged($event)
    {
        $seiyuuHandler = new SeiyuuHandler();

        $seiyuuHandler->calculateScores($event->user);
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $events
     * @return void
     */
    public function subscribe($events)
    {
        $events->listen(
            CharacterRankChanged::class,
            [self::class, 'handleCharacterRankChanged']
        );

        $events->listen(
            RoleWeightChanged::class,
            [self::class, 'handleRoleWeightChanged']
        );
    }
}
