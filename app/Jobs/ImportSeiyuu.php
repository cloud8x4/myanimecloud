<?php

namespace App\Jobs;

use App\Http\Controllers\API\Jikan\JikanPersonController;
use App\Models\Seiyuu;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ImportSeiyuu implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $seiyuu = $this->getSeiyuuToImport();
        $seiyuu->updated_at = Carbon::Now(); // Save timestamp to prevent eternal loop when nothing can be updated
        $seiyuu->save();

        $jikanPersonController = new JikanPersonController();

        $jikanPersonController->person($seiyuu->MAL_id);
    }

    /**
     * Get all seiyuus and select only the 1 which is updated the farthest in the past.
     *
     * @return Seiyuu
     */
    public function getSeiyuuToImport()
    {
        return Seiyuu::orderBy('seiyuus.updated_at')->first();
    }
}
