<?php

namespace App\Jobs;

use App\Http\Controllers\API\Jikan\AnimeController;
use App\Models\Anime;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ImportAnime implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $anime = $this->getAnimeToImport();

        $anime->updated_at = Carbon::Now(); // Save timestamp to prevent eternal loop when nothing can be updated

        $anime->save();

        $jikanAnimeController = new AnimeController();

        $jikanAnimeController->anime($anime->MAL_id);
    }

    /**
     * Get all animes and select only the 1 which is updated the farthest in the past.
     *
     * @return Anime
     */
    public function getAnimeToImport()
    {
        return Anime::orderBy('animes.updated_at')->first();
    }
}
