<?php

namespace App\Models\Handlers;

use App\Models\Character;
use App\Models\User;
use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class CharacterHandler extends Model
{
    /** @var Character */
    private $characters;

    /** @var Collection */
    private $filter;

    /** @var Collection */
    private $sort;

    /** @var User */
    private $user;

    /**
     * Set the base query builder object characters.
     */
    public function initQuery()
    {
        $this->characters = new Character();
    }

    /**
     * Set user.
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * Add raw selection of character attributes to query for characters.
     *
     * @return mixed
     */
    public function getCharacters()
    {
        return $this->characters->selectRaw('characters.*');
    }

    /**
     * @return mixed
     */
    public function getFilter()
    {
        return $this->filter;
    }

    /**
     * @return mixed
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**********************************************************************
     * Filter functions
     **********************************************************************/

    /**
     * Set filter after request,
     * determine possible values for filters
     * and start the actual filtering.
     *
     * @param Request $request
     */
    public function filter(Request $request)
    {
        $this->setFilter($request);

        $this->filterCharacterGender();
    }

    public function setFilter(Request $request)
    {
        $this->filter = collect([]);

        if ($request->characterGender) {
            $characterGender = $request->characterGender;
        } elseif (Auth::user() && !empty(Auth::user()->setting('characterGender'))) {
            $characterGender = Auth::user()->setting('characterGender');
        } else {
            $characterGender = 'All genders';
        }

        if (Auth::user()) {
            Auth::user()->setSetting('characterGender', $characterGender);
        }

        $this->filter['characterGender'] = $characterGender;
    }

    /**
     * Add filtering of character gender to the query for characters.
     */
    private function filterCharacterGender()
    {
        $characterGender = $this->filter['characterGender'];

        if ($characterGender == 'Unknown') {
            $this->characters = $this->characters->whereNull('gender');
        } elseif ($characterGender != 'All genders') {
            $this->characters = $this->characters->where('gender', $characterGender);
        }
    }

    /**********************************************************************
     * Sort functions
     **********************************************************************/

    /**
     * Add sorting to the query for characters.
     */
    public function sort(Request $request)
    {
        $this->setSort($request);

        $this->characters = $this
            ->characters
            ->orderBy($this->sort['field'], $this->sort['order'])
            ->orderBy('characters.id');
    }

    /**
     * Set sort after request.
     *
     * @param Request $request
     */
    public function setSort(Request $request)
    {
        $this->sort = collect([]);

        $sortBy = 'favorited_count';

        if ($request->characterSortBy) {
            $sortBy = $request->characterSortBy;
        } elseif (Auth::user() && !empty(Auth::user()->setting('characterSortBy'))) {
            $sortBy = Auth::user()->setting('characterSortBy');
        }

        if (Auth::user()) {
            Auth::user()->setSetting('characterSortBy', $sortBy);
        }

        if ($sortBy == 'character_rank' && $this->user === Auth::user()) {
            $sortBy = 'current_user_character_rank';
        }

        $this->sort['field'] = $sortBy;

        $sortOrder = 'desc';

        if ($request->characterSortOrder) {
            $sortOrder = $request->characterSortOrder;
        } elseif (Auth::user() && !empty(Auth::user()->setting('characterSortOrder'))) {
            $sortOrder = Auth::user()->setting('characterSortOrder');
        }

        if (Auth::user()) {
            Auth::user()->setSetting('characterSortOrder', $sortOrder);
        }

        $this->sort['order'] = $sortOrder;
    }

    /**********************************************************************
     * Extend the Character Eloquent object
     **********************************************************************/

    /**
     * Get related user character fields (rank)
     * and eagerload the user character (with pivot).
     */
    public function leftJoinCurrentUserCharacter()
    {
        $this->characters = $this
            ->characters
            ->leftJoin('user_character as cuc', function ($query) {
                $query
                    ->on('characters.id', '=', 'cuc.character_id')
                    ->where('cuc.user_id', Auth::user()->id);
            })
            ->selectRaw('IF(cuc.rank IS NOT NULL, cuc.rank, 1000000) current_user_character_rank')
            ->with('currentUser');
    }

    /**
     * Get related user character fields (rank)
     * and eagerload the user character (with pivot).
     */
    public function leftJoinUserCharacter()
    {
        $this->characters = $this
            ->characters
            ->leftJoin('user_character as uc', function ($query) {
                $query
                    ->on('characters.id', '=', 'uc.character_id')
                    ->where('uc.user_id', $this->user->id);
            })
            ->selectRaw('IF(uc.rank IS NOT NULL, uc.rank, 1000000) character_rank')
            ->with([
                'users' => function ($query) {
                    $query->where('user_id', $this->user->id);
                },
            ]);
    }
}
