<?php

namespace App\Models\Handlers;

use App\Models\MAL\MalUser;
use App\Models\User;
use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

/**
 * Class RoleHandler.
 */
class RoleHandler extends Model
{
    /** @var */
    private $roles;
    /** @var */
    private $filter;
    /** @var */
    private $sort;

    /**********************************************************************
     * Getter and setter functions
     **********************************************************************/

    /**
     * Set the base query builder object roles
     * and join with other tables.
     *
     * @param $roles
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;

        $this->joinAnimes();

        $this->joinSeiyuus();

        if (Auth::user()) {
            $this->leftJoinUserCharacter(Auth::user());

            $this->leftJoinUserSeiyuu(Auth::user());

            if (Auth::user()->malUser) {
                $this->leftJoinMalAnimes(Auth::user()->malUser);
            }
        }
    }

    /**
     * Get roles
     * and eager load relations.
     *
     * @return mixed
     */
    public function getRoles()
    {
        return $this
            ->roles
            ->with([
                'character' => function ($query) {
                    $query->when(Auth::user(), function ($query) {
                        $query->with('currentUser');
                    });
                },
            ])
            ->with([
                'anime',
                'seiyuu' => function ($query) {
                    $query->when(Auth::user(), function ($query) {
                        $query->with('currentUser');
                    });
                },
            ]);
    }

    /**
     * @return mixed
     */
    public function getFilter()
    {
        return $this->filter;
    }

    /**********************************************************************
     * Filter functions
     **********************************************************************/

    /**
     * Set filter options.
     *
     * @param Request $request
     */
    public function setFilter(Request $request)
    {
        $this->filter = collect([]);

        $this->setFilterOption($request, 'characterGender', 'All genders');

        $this->setFilterOption($request, 'seiyuuGender', 'All genders');

        $this->setFilterOption($request, 'animeGenre', 'All genres');

        $this->setFilterOption($request, 'animeSeason', 'All seasons');

        $this->setFilterOption($request, 'animeYear', 'All years');
    }

    /**
     * Set 1 filter option
     * If no value is passed via a request, then value will be retrieved from the user settings
     * If no value exists yet, a default value will be used and written to database.
     *
     * @param Request $request
     * @param string $option
     * @param string $defaultValue
     */
    private function setFilterOption(Request $request, string $option, string $defaultValue)
    {
        if ($request->$option) {
            $value = $request->$option;
        } elseif (Auth::user() && !empty(Auth::user()->setting($option))) {
            $value = Auth::user()->setting($option);
        } else {
            // @TODO: user settings should be created upon user creation with default values
            $value = $defaultValue;
        }
        if (Auth::user()) {
            Auth::user()->setSetting($option, $value);
        }

        $this->filter[$option] = $value;
    }

    /**
     * Add filtering to the query for roles.
     */
    public function filter()
    {
        $this->filterCharacterGender();

        $this->filterSeiyuuGender();

        $this->filterAnimeGenre();

        $this->filterSeasonAndYear();
    }

    /**
     * Add filtering of character gender to the query for roles.
     */
    private function filterCharacterGender()
    {
        $characterGender = $this->filter['characterGender'];
        if ($characterGender == 'Unknown') {
            $this->roles = $this->roles->whereHas('character', function ($query) {
                $query->whereNull('gender');
            });
        } elseif ($characterGender != 'All genders') {
            $this->roles = $this->roles->whereHas('character', function ($query) use ($characterGender) {
                $query->where('gender', $characterGender);
            });
        }
    }

    /**
     * Add filtering of seiyuu gender to the query for roles.
     */
    private function filterSeiyuuGender()
    {
        $seiyuuGender = $this->filter['seiyuuGender'];
        if ($seiyuuGender == 'Unknown') {
            $this->roles = $this->roles->whereHas('seiyuu', function ($query) {
                $query->whereNull('gender');
            });
        } elseif ($seiyuuGender != 'All genders') {
            $this->roles = $this->roles->whereHas('seiyuu', function ($query) use ($seiyuuGender) {
                $query->where('gender', $seiyuuGender);
            });
        }
    }

    /**
     * Add filtering of anime genre to the query for roles.
     */
    private function filterAnimeGenre()
    {
        $genre = $this->filter['animeGenre'];
        if ($genre == 'Unknown') {
            $this->roles->whereDoesntHave('anime.genres');
        } elseif ($genre != 'All genres') {
            $this->roles = $this->roles->whereHas('anime.genres', function ($query) use ($genre) {
                $query->where('genre', $genre);
            });
        }
    }

    /**
     * Add filtering of anime year to the query for roles
     * This has to be combined because a subquery is used.
     */
    private function filterSeasonAndYear()
    {
        $year = $this->filter['animeYear'];

        $this->roles = $this->roles
            ->whereHas('anime', function ($query) use ($year) {
                if ($year == 'Unknown') {
                    $query = $this->filterSeason($query);

                    $query->whereNull('start_date');
                } elseif ($year != 'All years') {
                    $carbonHandler = new CarbonHandler();

                    $carbonHandler->createFromYear($year);

                    $from = $carbonHandler->getCarbon()->startOfYear()->formatLocalized(trans('carbon.mysql'));

                    $until = $carbonHandler->getCarbon()->endOfYear()->formatLocalized(trans('carbon.mysql'));

                    $query = $this->filterSeason($query);

                    $query->whereBetween('start_date', [$from, $until]);
                } else {
                    $this->filterSeason($query);
                }
            });
    }

    /**
     * Add filtering of anime season to the subquery for roles anime season and year.
     *
     * @param $query
     * @return mixed
     */
    private function filterSeason($query)
    {
        $season = $this->filter['animeSeason'];
        if ($season != 'All seasons') {
            $carbonHandler = new CarbonHandler();

            $from = $carbonHandler->animeSeasonStart($season)->month;

            $until = $carbonHandler->animeSeasonEnd($season)->month;

            return $query->whereRaw(("MONTH(start_date) BETWEEN $from AND $until"));
        }

        return $query;
    }

    /**********************************************************************
     * Sorting functions
     **********************************************************************/

    /**
     * Add sorting to the query for roles.
     */
    public function sort()
    {
        $this->roles = $this
            ->roles
            ->when(Auth::user(), function ($query) {
                $query->orderBy('character_rank');
            })
            ->orderBy('role')
            ->when(Auth::user(), function ($query) {
                $query
                    ->orderBy('current_user_seiyuu_rank')
                    ->when(Auth::user()->malUser, function ($query) {
                        $query->orderBy('mal_anime_score', 'DESC');
                    });
            })
            ->orderBy('seiyuu_members_score', 'DESC')
            ->orderBy('anime_members_score', 'DESC')
            ->orderBy('character_id');
    }

    /**********************************************************************
     * Extend the Role Eloquent object
     **********************************************************************/

    /**
     * Get related user character fields (rank)
     * for a given user.
     *
     * @param User $user
     */
    public function leftJoinUserCharacter(User $user)
    {
        $this->roles = $this
            ->roles
            ->leftJoin('user_character', function ($query) use ($user) {
                $query
                    ->on('roles.character_id', '=', 'user_character.character_id')
                    ->where('user_character.user_id', $user->id);
            })
            ->selectRaw('MIN(IF(user_character.rank IS NOT NULL, user_character.rank, 1000000)) character_rank');
    }

    /**
     * Get related user seiyuu fields (rank)
     * for a given user.
     *
     * @param User $user
     */
    public function leftJoinUserSeiyuu(User $user)
    {
        $this->roles = $this
            ->roles
            ->leftJoin('user_seiyuu', function ($query) use ($user) {
                $query->on('roles.seiyuu_id', '=', 'user_seiyuu.seiyuu_id')
                    ->where('user_seiyuu.user_id', $user->id);
            })
            ->selectRaw('MIN(IF(user_seiyuu.rank IS NOT NULL, user_seiyuu.rank, 1000000)) current_user_seiyuu_rank');
    }

    /**
     * Get related MAL anime fields
     * for a given user.
     *
     * @param MalUser $malUser
     */
    public function leftJoinMalAnimes(MalUser $malUser)
    {
        $this->roles = $this
            ->roles
            ->leftJoin('mal_animes', function ($query) use ($malUser) {
                $query->on('roles.anime_id', '=', 'mal_animes.anime_id')
                    ->where('mal_animes.mal_user_id', $malUser->id);
            })
            ->selectRaw('MAX(IF(mal_animes.score IS NOT NULL, mal_animes.score, 0)) mal_anime_score');
    }

    /**
     * Get related anime fields (members score).
     */
    public function joinAnimes()
    {
        $this->roles = $this
            ->roles
            ->join('animes', 'roles.anime_id', '=', 'animes.id')
            ->selectRaw('MAX(members_score) anime_members_score');
    }

    /**
     * Get related seiyuu fields (members score).
     */
    public function joinSeiyuus()
    {
        $this->roles = $this
            ->roles
            ->join('seiyuus', 'roles.seiyuu_id', '=', 'seiyuus.id')
            ->selectRaw('MAX(members_score) seiyuu_members_score');
    }
}
