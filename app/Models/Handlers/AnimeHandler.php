<?php

namespace App\Models\Handlers;

use App\Models\Anime;
use App\Models\Genre;
use App\Models\MAL\MalUser;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class AnimeHandler.
 */
class AnimeHandler extends Model
{
    /** @var Anime */
    private $animes;

    /** @var Collection */
    private $filter;

    /** @var Collection */
    private $sort;

    /**
     * AnimeHandler constructor.
     */
    public function __construct()
    {
        $this->animes = Anime::query();

        if (Auth::user() && Auth::user()->malUser) {
            $this->leftJoinMalAnimes(Auth::user()->malUser);
        }

        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getAnimes()
    {
        return $this
            ->animes
            ->selectRaw('animes.*')
            ->with('roles')
            ->when(Auth::user() && Auth::user()->malUser, function ($animes) {
                return $animes->with('myMalAnime');
            });
    }

    /**
     * @return mixed
     */
    public function getFilter()
    {
        return $this->filter;
    }

    /**
     * @return mixed
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**********************************************************************
     * Filter functions
     **********************************************************************/

    /**
     * @param Request $request
     */
    public function filter(Request $request)
    {
        $this->filter = collect([]);

        $this->setFilterPossibleValues();

        $this->setFilter($request);

        $this->filterClassication();

        $this->filterGenre();

        $this->filterSeason();

        $this->filterYear();

        $this->filterStatus();

        $this->filterType();

        if (Auth::user() && Auth::user()->malUser) {
            $this->filterWatchedStatus();
        }
    }

    /**
     * Determine possible values for filters.
     */
    public function setFilterPossibleValues()
    {
        $this->filter['classifications'] = Anime::getClassificationFilterValues();

        $this->filter['genres'] = Genre::getFilterValues();

        $this->filter['seasons'] = Anime::getSeasonFilterValues();

        $this->filter['years'] = Anime::getYearsFilterValues();

        $this->filter['statuses'] = Anime::getStatusFilterValues();
    }

    /**
     * Set filter options.
     *
     * @param Request $request
     */
    public function setFilter(Request $request)
    {
        $this->setFilterOption($request, 'animeSeason', 'All seasons');

        $this->setFilterOption($request, 'animeYear', 'All years');

        $this->setFilterOption($request, 'animeStatus', 'All statuses');

        $this->setFilterOption($request, 'animeGenre', 'All genres');

        $this->setFilterOption($request, 'animeClassification', 'All classifications');

        $this->setFilterOption($request, 'animeType', 'All types');

        $this->setFilterOption($request, 'animeWatchedStatus', 'Do not apply');
    }

    /**
     * Set 1 filter option
     * If no value is passed via a request, then value will be retrieved from the user settings
     * If no value exists yet, a default value will be used and written to database.
     *
     * @param Request $request
     * @param string $option
     * @param string $defaultValue
     */
    private function setFilterOption(Request $request, string $option, string $defaultValue)
    {
        if ($request->$option) {
            $value = $request->$option;
        } elseif (Auth::user() && !empty(Auth::user()->setting($option))) {
            $value = Auth::user()->setting($option);
        } else {
            // @TODO: user settings should be created upon user creation with default values
            $value = $defaultValue;
        }

        if (Auth::user()) {
            Auth::user()->setSetting($option, $value);
        }

        $this->filter[$option] = $value;
    }

    /**
     * Add filtering of anime genre to the query for animes.
     */
    private function filterGenre()
    {
        $genre = $this->filter['animeGenre'];

        if ($genre == 'Unknown') {
            $this->animes = $this->animes->doesntHave('genres');
        } elseif ($genre != 'All genres') {
            $this->animes = $this->animes->whereHas('genres', function ($query) use ($genre) {
                $query->where('genre', $genre);
            });
        }
    }

    /**
     * Add filtering of anime season to the query for animes.
     */
    private function filterSeason()
    {
        $season = $this->filter['animeSeason'];

        if ($season != 'All seasons') {
            $carbonHandler = new CarbonHandler();

            $from = $carbonHandler->animeSeasonStart($season)->month;

            $until = $carbonHandler->animeSeasonEnd($season)->month;

            $this->animes = $this->animes->whereRaw(("MONTH(start_date) BETWEEN $from AND $until"));
        }
    }

    /**
     * Add filtering of anime year to the query for animes.
     */
    private function filterYear()
    {
        $year = $this->filter['animeYear'];

        if ($year == 'Unknown') {
            $this->animes = $this->animes->whereNull('start_date');
        } elseif ($year != 'All years') {
            $carbonHandler = new CarbonHandler();

            $carbonHandler->createFromYear($year);

            $from = $carbonHandler->getCarbon()->startOfYear()->formatLocalized(trans('carbon.mysql'));

            $until = $carbonHandler->getCarbon()->endOfYear()->formatLocalized(trans('carbon.mysql'));

            $this->animes = $this->animes->whereBetween('start_date', [$from, $until]);
        }
    }

    /**
     * Add filtering of anime status to the query for animes.
     */
    private function filterStatus()
    {
        $status = $this->filter['animeStatus'];

        if ($status == 'Unknown') {
            $this->animes = $this->animes->whereNull('status');
        } elseif ($status != 'All statuses') {
            $this->animes = $this->animes->where('status', $status);
        }
    }

    /**
     * Add filtering of anime classification to the query for animes.
     */
    private function filterClassication()
    {
        $classification = $this->filter['animeClassification'];

        if ($classification == 'Unknown') {
            $this->animes = $this->animes->whereNull('classification');
        } elseif ($classification != 'All classifications') {
            $this->animes = $this->animes->where('classification', $classification);
        }
    }

    /**
     * Add filtering of anime type to the query for animes.
     */
    private function filterType()
    {
        $type = $this->filter['animeType'];

        if ($type == 'Unknown') {
            $this->animes = $this->animes->whereNull('type');
        } elseif ($type != 'All types') {
            $this->animes = $this->animes->where('type', $type);
        }
    }

    /**
     * Add filtering of anime watched status of the user to the query for animes.
     */
    private function filterWatchedStatus()
    {
        $watchedStatus = $this->filter['animeWatchedStatus'];

        if ($watchedStatus != 'Do not apply') {
            if ($watchedStatus == 'In my list') {
                $this->animes = $this->animes->whereNotNull('mal_animes.watched_status');
            } elseif ($watchedStatus == 'Not in my list') {
                $this->animes = $this->animes->whereNull('mal_animes.watched_status');
            } elseif ($watchedStatus == 'Not gonna watch') {
                $this->animes = $this
                    ->animes
                    ->where('mal_animes.watched_status', 'dropped')
                    ->where('mal_animes.watched_episodes', 0);
            } else {
                $this->animes = $this->animes->where('mal_animes.watched_status', $watchedStatus);

                if ($watchedStatus == 'Dropped') {
                    $this->animes->where('mal_animes.watched_episodes', '>', 0);
                }
            }
        }
    }

    /**********************************************************************
     * Sort functions
     **********************************************************************/

    /**
     * @param Request $request
     */
    public function sort(Request $request)
    {
        $this->setSort($request);

        $this->animes = $this
            ->animes
            ->orderBy($this->sort['field'], $this->sort['order'])
            ->orderBy('animes.id');
    }

    /**
     * @param Request $request
     */
    public function setSort(Request $request)
    {
        $this->sort = collect([]);

        if ($request->sortBy) {
            $sortBy = $request->sortBy;
        } elseif (Auth::user()) {
            if (!empty(Auth::user()->setting('animeSortBy'))) {
                $sortBy = Auth::user()->setting('animeSortBy');
            } elseif (Auth::user()->malUser) {
                $sortBy = 'mal_animes.score';
            } else {
                $sortBy = 'members_score';
            }
        } else {
            $sortBy = 'members_score';
        }

        if (Auth::user()) {
            Auth::user()->setSetting('animeSortBy', $sortBy);
        }

        $this->sort['field'] = $sortBy;

        if ($request->sortOrder) {
            $sortOrder = $request->sortOrder;
        } elseif (Auth::user() && !empty(Auth::user()->setting('animeSortOrder'))) {
            $sortOrder = Auth::user()->setting('animeSortOrder');
        } else {
            $sortOrder = 'desc';
        }

        if (Auth::user()) {
            Auth::user()->setSetting('animeSortOrder', $sortOrder);
        }

        $this->sort['order'] = $sortOrder;
    }

    /**********************************************************************
     * Search functions
     **********************************************************************/

    /**
     * Search for an anime title.
     *
     * @param string $search
     */
    public function search(string $search)
    {
        $this->animes = $this->animes->where('title', 'like', '%' . $search . '%');
    }

    /**********************************************************************
     * Extend the Anime Eloquent object
     **********************************************************************/

    /**
     * Get related MAL anime fields
     * for a given MAL user.
     *
     * @param MalUser $malUser
     */
    public function leftJoinMalAnimes(MalUser $malUser)
    {
        $sqlMalAnimes = $malUser->malAnimes()
            ->toSql();

        $this->animes = $this->animes
            ->leftJoin(
                DB::raw("( $sqlMalAnimes ) as mal_animes"), 'animes.id', '=', 'mal_animes.anime_id'
            )
            ->selectRaw('mal_animes.watched_episodes')
            ->selectRaw('mal_animes.score')
            ->selectRaw('mal_animes.updated_at')
            ->setBindings([$malUser->id]);
    }
}
