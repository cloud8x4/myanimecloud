<?php

namespace App\Models\Handlers;

use App\Models\Anime;
use App\Models\Genre;
use App\Models\Seiyuu;
use App\Models\User;
use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

/**
 * Class SeiyuuHandler.
 */
class SeiyuuHandler extends Model
{
    /** @var Seiyuu */
    private $seiyuus;

    /** @var Collection */
    private $filter;

    /** @var Collection */
    private $sort;

    /** @var User */
    private $user;

    /**
     * Set the base query builder object seiyuus.
     */
    public function initQuery()
    {
        $this->seiyuus = (new Seiyuu())->where('name', '<>', 'unknown');
    }

    /**
     * Set user.
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * Add raw selection of seiyuu attributes to query for seiyuu.
     *
     * @return mixed
     */
    public function getSeiyuus()
    {
        return $this->seiyuus->selectRaw('seiyuus.*');
    }

    /**
     * @return mixed
     */
    public function getFilter()
    {
        return $this->filter;
    }

    /**
     * @return mixed
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**********************************************************************
     * Filter functions
     **********************************************************************/

    /**
     * Set filter after request,
     * determine possible values for filters
     * and start the actual filtering.
     *
     * @param Request $request
     */
    public function filter(Request $request)
    {
        $this->filter = collect([]);

        $this->setFilterPossibleValues();

        $this->setFilter($request);

        $this->filterGender();

        $this->filterGenre();

        $this->filterSeasonAndYear();
    }

    /**
     * Determine possible values for filters.
     */
    public function setFilterPossibleValues()
    {
        $this->filter['genres'] = Genre::getFilterValues();

        $this->filter['seasons'] = Anime::getSeasonFilterValues();

        $this->filter['years'] = Anime::getYearsFilterValues();
    }

    /**
     * Set filter options.
     *
     * @param Request $request
     */
    public function setFilter(Request $request)
    {
        $this->setFilterOption($request, 'seiyuuGender', 'All genders');

        $this->setFilterOption($request, 'animeGenre', 'All genres');

        $this->setFilterOption($request, 'animeSeason', 'All seasons');

        $this->setFilterOption($request, 'animeYear', 'All years');
    }

    /**
     * Set 1 filter option
     * If no value is passed via a request, then value will be retrieved from the user settings
     * If no value exists yet, a default value will be used and written to database.
     *
     * @param Request $request
     * @param string $option
     * @param string $defaultValue
     */
    private function setFilterOption(Request $request, string $option, string $defaultValue)
    {
        if ($request->$option) {
            $value = $request->$option;
        } elseif (Auth::user() && !empty(Auth::user()->setting($option))) {
            $value = Auth::user()->setting($option);
        } else {
            // @TODO: user settings should be created upon user creation with default values
            $value = $defaultValue;
        }

        if (Auth::user()) {
            Auth::user()->setSetting($option, $value);
        }

        $this->filter[$option] = $value;
    }

    /**
     * Add filtering of seiyuu gender to the query for seiyuu.
     */
    private function filterGender()
    {
        $seiyuuGender = $this->filter['seiyuuGender'];
        if ($seiyuuGender == 'Unknown') {
            $this->seiyuus = $this->seiyuus->whereNull('gender');
        } elseif ($seiyuuGender != 'All genders') {
            $this->seiyuus = $this->seiyuus->where('gender', $seiyuuGender);
        }
    }

    /**
     * Add filtering of anime gender to the query for seiyuu.
     */
    private function filterGenre()
    {
        $genre = $this->filter['animeGenre'];

        if ($genre == 'Unknown') {
            $this->seiyuus = $this->seiyuus->doesntHave('roles.anime.genres');
        } elseif ($genre != 'All genres') {
            $this->seiyuus = $this->seiyuus->whereHas('roles.anime.genres', function ($query) use ($genre) {
                $query->where('genre', $genre);
            });
        }
    }

    /**
     * Add filtering of anime year to the query for seiyuu
     * This has to be combined because a subquery is used.
     */
    private function filterSeasonAndYear()
    {
        $year = $this->filter['animeYear'];

        $season = $this->filter['animeSeason'];

        if ($year == 'All years' && $season == 'All seasons') {
            return;
        }

        $this->seiyuus = $this->seiyuus->whereHas('roles.anime', function ($query) use ($year) {
            if ($year == 'Unknown') {
                $query = $this->filterSeason($query);

                $query->whereNull('start_date');
            } elseif ($year != 'All years') {
                $carbonHandler = new CarbonHandler();

                $carbonHandler->createFromYear($year);

                $from = $carbonHandler->getCarbon()->startOfYear()->formatLocalized(trans('carbon.mysql'));

                $until = $carbonHandler->getCarbon()->endOfYear()->formatLocalized(trans('carbon.mysql'));

                $query = $this->filterSeason($query);

                $query->whereBetween('start_date', [$from, $until]);
            } else {
                $this->filterSeason($query);
            }
        });
    }

    /**
     * Add filtering of anime season to the query for seiyuus.
     *
     * @param $query
     * @return mixed
     */
    private function filterSeason($query)
    {
        $season = $this->filter['animeSeason'];

        if ($season != 'All seasons') {
            $carbonHandler = new CarbonHandler();

            $from = $carbonHandler->animeSeasonStart($season)->month;

            $until = $carbonHandler->animeSeasonEnd($season)->month;

            return $query->whereRaw(("MONTH(start_date) BETWEEN $from AND $until"));
        }

        return $query;
    }

    /**********************************************************************
     * Sort functions
     **********************************************************************/

    /**
     * Add sorting to the query for seiyuu.
     */
    public function sort(Request $request)
    {
        $this->setSort($request);

        $this->seiyuus = $this
            ->seiyuus
            ->orderBy($this->sort['field'], $this->sort['order'])
            ->orderBy('seiyuus.id');
    }

    /**
     * Set sort after request.
     *
     * @param Request $request
     */
    public function setSort(Request $request)
    {
        $this->sort = collect([]);

        if ($request->seiyuuSortBy) {
            $sortBy = $request->seiyuuSortBy;
        } elseif (Auth::user() && !empty(Auth::user()->setting('seiyuuSortBy'))) {
            $sortBy = Auth::user()->setting('seiyuuSortBy');
        } else {
            $sortBy = 'favorited_count';
        }

        if (Auth::user()) {
            Auth::user()->setSetting('seiyuuSortBy', $sortBy);
        }

        if ($sortBy == 'seiyuu_rank' && $this->user === Auth::user()) {
            $sortBy = 'current_user_seiyuu_rank';
        }

        $this->sort['field'] = $sortBy;

        if ($request->seiyuuSortOrder) {
            $sortOrder = $request->seiyuuSortOrder;
        } elseif (Auth::user() && !empty(Auth::user()->setting('seiyuuSortOrder'))) {
            $sortOrder = Auth::user()->setting('seiyuuSortOrder');
        } else {
            $sortOrder = 'desc';
        }

        if (Auth::user()) {
            Auth::user()->setSetting('seiyuuSortOrder', $sortOrder);
        }

        $this->sort['order'] = $sortOrder;
    }

    /**********************************************************************
     * Extend the Seiyuu Eloquent object
     **********************************************************************/

    /**
     * Get related user seiyuu fields (rank)
     * and eagerload the user seiyuu (with pivot).
     */
    public function leftJoinCurrentUserSeiyuu()
    {
        $this->seiyuus = $this->seiyuus
            ->leftJoin('user_seiyuu', function ($query) {
                $query
                    ->on('seiyuus.id', '=', 'user_seiyuu.seiyuu_id')
                    ->where('user_seiyuu.user_id', $this->user->id);
            })
            ->select('user_seiyuu.score_by_character_ranks as seiyuu_score_by_character_ranks')
            ->selectRaw('IF(user_seiyuu.rank IS NOT NULL, user_seiyuu.rank, 1000000) current_user_seiyuu_rank')
            ->with('currentUser');
    }

    /**
     * Get related user seiyuu fields (rank)
     * and eagerload the user seiyuu (with pivot).
     */
    public function leftJoinUserSeiyuu()
    {
        $this->seiyuus = $this->seiyuus
            ->leftJoin('user_seiyuu as uc', function ($query) {
                $query
                    ->on('seiyuus.id', '=', 'uc.seiyuu_id')
                    ->where('uc.user_id', $this->user->id);
            })
            ->selectRaw('IF(uc.rank IS NOT NULL, uc.rank, 1000000) seiyuu_rank')
            ->with([
                'users' => function ($query) {
                    $query->where('user_id', $this->user->id);
                },
            ]);
    }

    /**********************************************************************
     * Calculation functions
     **********************************************************************/

    /**
     * Get related user seiyuu fields (rank)
     * and eagerload the user seiyuu (with pivot).
     *
     * @param User $user
     */
    public function calculateScores(User $user)
    {
        $seiyuus = $user->seiyuus->map(function (Seiyuu $seiyuu) use ($user) {
            $seiyuu->score = $seiyuu->calculateScore($user);

            return $seiyuu;
        });

        $max = $seiyuus->max('score');

        $seiyuus->each(function (Seiyuu $seiyuu) use ($max, $user) {
            $user->seiyuus()->updateExistingPivot(
                $seiyuu->id,
                ['score_by_character_ranks' => $seiyuu->score / $max * 1000]
            );
        });
    }
}
