<?php

namespace App\Models\Handlers;

use Storage;

class ImageHandler
{
    /** @var string */
    private $basePath = 'public/';

    /** @var string */
    private $path;

    /**
     * ImageHandler constructor.
     *
     * @param $path
     */
    public function __construct($path)
    {
        $this->path = $path;
    }

    /**
     * Save image from external location.
     *
     * @param string $url
     * @return string|null
     */
    public function create(string $url): ?string
    {
        $headers = get_headers($url, 1);

        if ($headers[0] == 'HTTP/1.0 404 Not Found') {
            return null;
        }

        $path_info = pathinfo($url);
        $filename = uniqid() . '.' . strtok($path_info['extension'], '?');
        Storage::put($this->fullPath($filename), file_get_contents($url));

        return $filename;
    }

    /**
     * Check if image exists in storage
     * If yes, return size
     * If no, return 0.
     *
     * @param $fullPath
     * @return int
     */
    public function size($fullPath): int
    {
        return Storage::exists($fullPath) ? Storage::size($fullPath) : 0;
    }

    /**
     * Get full path from storage location to image.
     *
     * @param string $filename
     * @return string
     */
    private function fullPath(string $filename): string
    {
        return $this->basePath . $this->path . $filename;
    }

    /**
     * Check if image exists.
     * If no, create image in storage and database.
     * else, check if image is too small to be a good image (< 1kB).
     *     If yes, do nothing.
     *     If no, replace the image in storage and database.
     *
     * @param string|null $filename
     * @param string $url
     * @return string|null
     */
    public function updateOrCreate(?string $filename, string $url): ?string
    {
        if (empty($filename)) {
            return $this->create($url);
        }
        if ($this->size($this->fullPath($filename)) > 2000) {
            return null;
        }
        Storage::delete($this->fullPath($filename));

        return $this->create($url);
    }
}
