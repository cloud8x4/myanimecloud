<?php

namespace App\Models\Handlers;

use Carbon\Carbon;
use Illuminate\Support\Collection;

/**
 * Class CarbonHandler.
 */
class CarbonHandler
{
    /** @Carbon */
    private $carbon;

    /** @var Collection */
    private $seasons;

    /**
     * CarbonHandler constructor.
     */
    public function __construct()
    {
        $this->seasons = collect([
            'Winter' => collect(['first_month' => 1]),
            'Spring' => collect(['first_month' => 4]),
            'Summer' => collect(['first_month' => 7]),
            'Fall' => collect(['first_month' => '10']),
        ]);
    }

    /**
     * @return mixed
     */
    public function getCarbon()
    {
        return $this->carbon;
    }

    /**********************************************************************
     * Create Carbon Object from various inputs
     **********************************************************************/

    /**
     * @param Carbon $carbon
     */
    public function createFromCarbon(Carbon $carbon)
    {
        $this->carbon = Carbon
            ::createFromDate($carbon->year, $carbon->month, $carbon->day)
            ->startOfDay();
    }

    /**
     * Create Carbon object for given year.
     *
     * @param $year
     */
    public function createFromYear($year)
    {
        $this->carbon = Carbon
            ::createFromDate($year, null, null)
            ->startOfYear();
    }

    /**
     * Create Carbon object for given month.
     *
     * @param $month
     */
    public function createFromMonth($month)
    {
        $this->carbon = Carbon
            ::createFromDate(null, $month, null)
            ->startOfMonth();
    }

    /**
     * Create Carbon object for given season.
     *
     * @param $season
     */
    public function createFromSeason($season)
    {
        $month = $this->seasons->get($season)->get('first_month');

        $this->createFromMonth($month);
    }

    /**
     * Get season according to anime standards (not meteorological standards).
     *
     * @return string
     */
    public function animeSeason()
    {
        switch ($this->carbon->month) {
            case $this->carbon->month > 9:
                $season = trans('Fall');
                break;
            case $this->carbon->month > 6:
                $season = trans('Summer');
                break;
            case $this->carbon->month > 3:
                $season = trans('Spring');
                break;
            default:
                $season = trans('Winter');
        }

        return $season . ' ' . $this->carbon->year;
    }

    /**
     * Get Carbon object of start of given season.
     *
     * @param $season
     * @return mixed
     */
    public function animeSeasonStart($season)
    {
        $this->createFromSeason($season);

        return $this->carbon;
    }

    /**
     * Get Carbon object of end of given season.
     *
     * @param $season
     * @return mixed
     */
    public function animeSeasonEnd($season)
    {
        $this->createFromSeason($season);

        return $this->carbon->addMonths(3)->subDay();
    }
}
