<?php

namespace App\Models;

use App\Models\Handlers\ImageHandler;
use App\Presenters\Seiyuu as SeiyuuPresenter;
use Auth;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Laracasts\Presenter\PresentableTrait;

/**
 * Class Seiyuu.
 */
class Seiyuu extends Model
{
    use HasFactory;
    use PresentableTrait;
    use SoftDeletes;

    /** @var string */
    protected $presenter = SeiyuuPresenter::class;

    /** @var array */
    protected $fillable = [
        'MAL_id',
        'image',
        'name',
        'given_name',
        'family_name',
        'birthday',
        'website_url',
        'more_details',
        'favorited_count',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['birthday', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * Path to seiyuu image on local storage.
     *
     * @var string
     */
    public $imagePath = 'seiyuu/';

    /**********************************************************************
     * Get Eloquent objects
     **********************************************************************/

    /**
     * Get an Eloquent object for all related roles.
     *
     * @return HasMany
     */
    public function roles(): HasMany
    {
        return $this->hasMany(Role::class);
    }

    /**
     * Get an Eloquent object for all related users
     * with pivot table data timestamps and rank.
     *
     * @return BelongsToMany
     */
    public function users(): BelongsToMany
    {
        return $this
            ->belongsToMany(User::class, 'user_seiyuu')
            ->withTimestamps()
            ->withPivot('rank', 'score_by_character_ranks');
    }

    /**
     * Get an Eloquent object for a given user
     * with pivot table data timestamps and rank.
     *
     * @param User $user
     * @return BelongsToMany
     */
    public function user(User $user): BelongsToMany
    {
        return $this->users()->where('users.id', $user->id);
    }

    /**
     * Get an Eloquent object for current user
     * with pivot table data timestamps and rank.
     *
     * @return BelongsToMany
     */
    public function currentUser(): BelongsToMany
    {
        return $this->user(Auth::user());
    }

    /**
     * Get an Eloquent object for all related roles
     * grouped per character
     * so you see each character only once.
     *
     * @return HasMany
     */
    public function rolesGroupedByCharacter(): HasMany
    {
        return $this
            ->roles()
            ->groupBy(['roles.character_id'])
            ->select([
                'roles.character_id',
                DB::raw('MIN(roles.id) as id'),
                DB::raw('MAX(roles.weight) as weight'),
                DB::raw('MIN(roles.anime_id) as anime_id'),
                DB::raw('MIN(roles.role) as role'),
            ]);
    }

    /**
     * Get an Eloquent object for all related roles
     * EXCEPT 1 given Character object
     * grouped per character so you see each character only once
     * with their related Character object and Anime object.
     *
     * @param Character $character
     * @return HasMany
     */
    public function alternativeRolesGroupedByCharacter(Character $character): HasMany
    {
        return $this
            ->rolesGroupedByCharacter()
            ->where('roles.character_id', '<>', $character->id)
            ->whereDoesntHave('seiyuu', function ($query) {
                $query->where('name', 'unknown');
            });
    }

    /**********************************************************************
     * Dyanmic relationships using sub queries
     **********************************************************************/

    /**
     * Add field rank.
     *
     * @param $query
     */
    public function scopeWithCurrentUserRank($query)
    {
        $query
            ->addSelect([
                'rank' => DB
                    ::table('user_seiyuu')
                    ->whereColumn('seiyuu_id', 'seiyuus.id')
                    ->where('user_id', Auth::user()->id)
                    ->select('rank'),
            ]);
    }

    /**********************************************************************
     * Transformation functions
     **********************************************************************/

    /**
     * Set rank for a given user.
     *
     * @param User $user
     * @param string|null $rank
     * @return array
     */
    public function setUserRank(User $user, string $rank = null): array
    {
        $log = collect();
        $log[] = 'Ranking seiyuu ' . $this->name . '.';

        $rankedSeiyuu = $user->seiyuu($this)->first();

        $oldRank = $rankedSeiyuu ? $rankedSeiyuu->pivot->rank : null;
        $log[] = 'Old rank: ' . ($oldRank ?? 'unranked') . '.';

        if (($rank === null || $rank === '') && $oldRank) {
            $log[] = 'Requested rank: unranked.';

            $this->users()->detach($user->id);
            $message = 'Successfully unranked seiyuu ' . $this->name . '.';
            $log[] = $message;

            // Update other seiyuu ranks
            $worstRank = $user->seiyuus()->max('rank');
            $userSeiyuus = DB::table('user_seiyuu')
                ->where('user_id', $user->id)
                ->where('seiyuu_id', '<>', $this->id);

            if ($worstRank && $oldRank !== $worstRank) {
                $log[] = $userSeiyuus->whereBetween('rank', [$oldRank, $worstRank])->count() .
                    ' other seiyuu(s) with rank between ' . $oldRank . ' and ' . $worstRank . ' will rise 1 rank.';
                $userSeiyuus->whereBetween('rank', [$oldRank, $worstRank])->decrement('rank');
            }

            return [
                'status' => 'success',
                'message' => $message,
                'log' => $log,
                'newRank' => '?',
            ];
        }

        if (($rank != '' && !ctype_digit($rank)) || (is_numeric($rank) && $rank < 1)) {
            $message = 'Rank of seiyuu ' . $this->name . ' was not set to ' . $rank . ' because rank must be a number equal to or higher than 1.';
            $log[] = $message;

            return [
                'status' => 'warning',
                'message' => $message,
                'log' => $log,
            ];
        }

        if ($user->seiyuus()->get()->isEmpty()) {
            $log[] = 'User has not ranked any seiyuus yet. New rank 1.';
            $this->users()->attach($user->id, ['rank' => 1]);
            $message = 'Successfully set rank of seiyuu ' . $this->name . ' to 1.';
            $log[] = $message;

            return [
                'status' => 'success',
                'message' => $message,
                'log' => $log,
                'newRank' => 1,
            ];
        }

        $bestRank = $user->seiyuus()->min('rank');
        $worstRank = $user->seiyuus()->max('rank');

        if (($rank === null || $rank === '') && !$oldRank) {
            $log[] = 'Requested rank: empty.';
            $newRank = $worstRank + 1;
            $log[] = 'Since seiyuu is unranked, it will get the worst rank ' . $newRank . '.';

            $this->users()->attach($user->id, ['rank' => $newRank]);

            $message = 'Successfully set rank of seiyuu ' . $this->name . ' to ' . $newRank . '.';
            $log[] = $message;

            return [
                'status' => 'success',
                'message' => $message,
                'log' => $log,
                'newRank' => $newRank,
            ];
        }

        $newRank = (int)$rank;
        $log[] = 'Requested rank: ' . $rank . '.';

        if ($newRank == $oldRank) {
            $message = 'Rank of seiyuu ' . $this->name . ' was not set to ' . $newRank . ' because it already has this rank.';
            $log[] = $message;

            return [
                'status' => 'warning',
                'message' => $message,
                'log' => $log,
            ];
        }

        if ($newRank <= $bestRank) {
            $log[] = 'User has filled in the best rank ' . $bestRank . '.';
            $newRank = 1;
        } elseif ($newRank > $worstRank) {
            $log[] = 'User has filled in a rank ' . $newRank . ' which is worse than the worst rank ' . $worstRank . '. ';
            $newRank = $worstRank;
            if (!$oldRank) {
                $newRank++;
            }
            if ($oldRank == $newRank) {
                $message = 'Rank of seiyuu ' . $this->name . ' was not set to ' . $this->rank . ' because it already has this rank.';
                $log[] = $message;

                return [
                    'status' => 'warning',
                    'message' => $message,
                    'log' => $log,
                ];
            }
        } else {
            $log[] = 'User has filled in a rank between worst rank ' . $worstRank . ' and best rank ' . $bestRank . '. ';
        }

        if (!$oldRank) {
            $this->users()->attach($user->id, ['rank' => $newRank]);
        } elseif ($newRank) {
            $this->users()->updateExistingPivot($user->id, ['rank' => $newRank]);
        }
        $message = 'Successfully set rank of seiyuu ' . $this->name . ' to ' . $newRank . '.';
        $log[] = $message;

        // Update other seiyuu ranks only when necessary
        $userSeiyuus = DB::table('user_seiyuu')
            ->where('user_id', $user->id)
            ->where('seiyuu_id', '<>', $this->id);

        if (!$oldRank && $newRank <= $worstRank) {
            $userSeiyuus = $userSeiyuus->where('rank', '>=', $newRank);
            $log[] = $userSeiyuus->count() .
                ' other seiyuu(s) with rank ' . $newRank . ' or worse will drop 1 rank.';
            $userSeiyuus->increment('rank');
            $userSeiyuus->update(['updated_at' => Carbon::now()]);
        } elseif ($oldRank && $newRank < $oldRank) {
            $userSeiyuus = $userSeiyuus->whereBetween('rank', [$newRank, $oldRank]);
            $log[] = $userSeiyuus->count() .
                ' other seiyuu(s) with rank between ' . $newRank . ' and ' . $oldRank . ' will drop 1 rank.';
            $userSeiyuus->increment('rank');
            $userSeiyuus->update(['updated_at' => Carbon::now()]);
        } elseif ($oldRank && $newRank > $oldRank) {
            $userSeiyuus = $userSeiyuus->whereBetween('rank', [$oldRank, $newRank]);
            $log[] = $userSeiyuus->count() .
                ' other seiyuu(s) with rank between ' . $oldRank . ' and ' . $newRank . ' will rise 1 rank.';
            $userSeiyuus->decrement('rank');
            $userSeiyuus->update(['updated_at' => Carbon::now()]);
        }

        return [
            'status' => 'success',
            'message' => $message,
            'log' => $log,
            'newRank' => $newRank,
        ];
    }

    /**
     * Calculate score based on character rank and role weight.
     *
     * @param User $user
     * @return mixed
     */
    public function calculateScore(User $user)
    {
        return $user
            ->characters()
            ->whereHas('roles', function ($query) {
                $query->where('roles.seiyuu_id', '=', $this->id);
            })
            ->with([
                'roles' => function ($query) {
                    $query->where('seiyuu_id', '=', $this->id);
                },
            ])
            ->get()
            ->map(function (Character $character) {
                return (1000 / pow($character->pivot->rank, 1 / 2)) * ($character->roles->avg('weight') / 100);
            })
            ->sum();
    }

    /**
     * Update or create the seiyuu image.
     *
     * @param $url
     */
    public function updateOrCreateImage($url)
    {
        $this->image = (new ImageHandler($this->imagePath))->updateOrCreate($this->image, $url);
        if ($this->image !== null) {
            $this->save();
        }
    }
}
