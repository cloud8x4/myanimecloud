<?php

namespace App\Models\MAL;

use App\Models\Anime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\belongsTo;
use Illuminate\Support\Facades\DB;

class MalAnime extends Model
{
    use HasFactory;

    protected $fillable = [
        'mal_user_id',
        'anime_id',
        'watched_status',
        'watched_episodes',
        'score',
    ];

    /**********************************************************************
     * Get Eloquent relational objects
     **********************************************************************/

    /**
     * Get an Eloquent object for MAL anime's related MAL user.
     *
     * @return belongsTo
     */
    public function malUser()
    {
        return $this->belongsTo(MalUser::class);
    }

    /**
     * Get an Eloquent object for MAL anime's related anime.
     *
     * @return belongsTo
     */
    public function anime()
    {
        return $this->belongsTo(Anime::class);
    }

    /**********************************************************************
     * Static functions to get extended Eloquent objects
     **********************************************************************/

    public static function qryShared(MalUser $malUser1, MalUser $malUser2)
    {
        $qrySharedMalAnimes = self::from('mal_animes as ma1')
            ->leftJoin('mal_animes as ma2',
                function ($query) use ($malUser2) {
                    $query->on('ma1.anime_id', '=', 'ma2.anime_id')
                        ->where('ma2.mal_user_id', $malUser2->id);
                }
            )
            ->where('ma1.mal_user_id', $malUser1->id);
        $qrySharedMalAnimes = self::qrySharedSelect($qrySharedMalAnimes);

        $qrySharedMalAnimes2 = self::from('mal_animes as ma1')
            ->leftJoin('mal_animes as ma2',
                function ($query) use ($malUser1) {
                    $query->on('ma1.anime_id', '=', 'ma2.anime_id')
                        ->where('ma2.mal_user_id', $malUser1->id);
                }
            )
            ->where('ma1.mal_user_id', $malUser2->id)
            ->whereNull('ma2.id');
        $qrySharedMalAnimes2 = self::qrySharedSelect($qrySharedMalAnimes2);

        return $qrySharedMalAnimes->union($qrySharedMalAnimes2);
    }

    public static function qrySharedSelect($qrySharedMalAnimes)
    {
        return $qrySharedMalAnimes
            ->select(DB::raw('IF (ma1.anime_id IS NOT NULL, ma1.anime_id, ma2.anime_id) as anime_id'),
                'ma1.mal_user_id as user1_user_id',
                'ma1.watched_status as user1_watched_status',
                'ma1.watched_episodes as user1_watched_episodes',
                'ma1.score as user1_score',
                'ma1.updated_at as user1_updated_at',
                'ma2.mal_user_id as user2_user_id',
                'ma2.watched_status as user2_watched_status',
                'ma2.watched_episodes as user2_watched_episodes',
                'ma2.score as user2_score',
                'ma2.updated_at as user2_updated_at');
    }
}
