<?php

namespace App\Models\MAL;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class MalUser.
 */
class MalUser extends Model
{
    use HasFactory;

    /** @var array */
    protected $fillable = [
        'name',
        'anime_days',
    ];

    /**********************************************************************
     * Get Eloquent objects
     **********************************************************************/

    /**
     * @return HasMany
     */
    public function malAnimes()
    {
        return $this->hasMany(MalAnime::class);
    }

    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'name', 'name');
    }
}
