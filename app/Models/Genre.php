<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Genre extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['genre'];

    public static function getFilterValues()
    {
        $genres = self::orderBy('genre')
            ->get()
            ->pluck('genre');
        $genres = collect($genres);
        $genres->prepend('Unknown')
            ->prepend('All genres');

        return $genres;
    }
}
