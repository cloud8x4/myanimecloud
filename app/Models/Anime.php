<?php

namespace App\Models;

use App\Models\Handlers\ImageHandler;
use App\Models\MAL\MalAnime;
use App\Presenters\Anime as AnimePresenter;
use Auth;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\hasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Laracasts\Presenter\PresentableTrait;

/**
 * Class Anime.
 */
class Anime extends Model
{
    use HasFactory;
    use PresentableTrait;
    use SoftDeletes;

    /** @var string */
    protected $presenter = AnimePresenter::class;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['start_date', 'end_date', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'MAL_id',
        'title',
        'type',
        'episodes',
        'duration',
        'status',
        'start_date',
        'end_date',
        'classification',
        'members_score',
        'members_count',
        'rank',
        'popularity_rank',
        'favorited_count',
        'image',
    ];

    /**
     * Path to anime image on local storage.
     *
     * @var string
     */
    public $imagePath = 'anime/';

    /**********************************************************************
     * Get Eloquent relational objects
     **********************************************************************/

    /**
     * Get an Eloquent object for all related roles.
     *
     * @return HasMany
     */
    public function roles(): HasMany
    {
        return $this->hasMany(Role::class);
    }

    /**
     * Get an Eloquent object for related MAL animes.
     *
     * @return hasMany
     */
    public function malAnimes(): HasMany
    {
        return $this->hasMany(MalAnime::class);
    }

    /**
     * Get an Eloquent object for related MAL anime of current user.
     *
     * @return hasOne|null
     */
    public function myMalAnime(): ?hasOne
    {
        return Auth::user() ? $this
            ->hasOne(MalAnime::class)
            ->where('mal_user_id', Auth::user()->malUser->id) : null;
    }

    /**
     * Get an Eloquent object for all related producers.
     *
     * @return BelongsToMany
     */
    public function producers(): BelongsToMany
    {
        return $this->belongsToMany(Producer::class)->withTimestamps();
    }

    /**
     * Get an Eloquent object for all related genres.
     *
     * @return BelongsToMany
     */
    public function genres(): BelongsToMany
    {
        return $this->belongsToMany(Genre::class)->withTimestamps();
    }

    /**
     * Get an Eloquent object for all related opening themes.
     *
     * @return BelongsToMany
     */
    public function openingThemes(): BelongsToMany
    {
        return $this->belongsToMany(OpeningTheme::class);
    }

    /**
     * Get an Eloquent object for all related ending themes.
     *
     * @return BelongsToMany
     */
    public function endingThemes(): BelongsToMany
    {
        return $this->belongsToMany(EndingTheme::class);
    }

    /**********************************************************************
     * Get Eloquent objects
     **********************************************************************/

    /**
     * Get an Eloquent object for all related roles
     * grouped per character
     * so you see each character only once.
     *
     * @return HasMany
     */
    public function rolesGroupedByCharacter(): HasMany
    {
        return $this->roles()
            ->groupBy('roles.character_id')
            ->select([
                'roles.character_id',
                DB::raw('MIN(roles.id) as id'),
                DB::raw('MIN(roles.anime_id) as anime_id'),
                DB::raw('MIN(roles.seiyuu_id) as seiyuu_id'),
                DB::raw('MIN(roles.role) as role'),
                DB::raw('MAX(weight) as weight'),
            ]);
    }

    /**********************************************************************
     * Transformation functions
     **********************************************************************/

    /**
     * For a given collection of producers, loop through them and
     * update or create a Producer object.
     *
     * Link this Anime object to the Producer objects via Eloquent sync
     *
     * @param Collection $producers
     */
    public function addProducers(Collection $producers)
    {
        $this->producers()->sync(
            $producers->map(function (string $producer) {
                return Producer::updateOrCreate(['producer' => $producer])->getKey();
            })->toArray()
        );
    }

    /**
     * For a given collection of genres, loop through them and
     * update or create a Genre object.
     *
     * Link this Anime object to the Genre objects via Eloquent sync
     *
     * @param Collection $genres
     */
    public function addGenres(Collection $genres)
    {
        $genres = $genres->map(function (string $genre) {
            return Genre::updateOrCreate(['genre' => $genre]);
        });

        $this->genres()->sync($genres->pluck('id')->toArray());
    }

    /**
     * For a given collection of opening themes, loop through them and
     * update or create an OpeningTheme object.
     *
     * Link this Anime object to the OpeningTheme objects via Eloquent sync
     *
     * @param Collection $openingThemes
     */
    public function addOpeningThemes(Collection $openingThemes)
    {
        $this->openingThemes()->sync(
            $openingThemes->map(function (string $openingTheme) {
                return OpeningTheme::updateOrCreate(['opening_theme' => $openingTheme])->getKey();
            })->toArray()
        );
    }

    /**
     * For a given collection of ending themes, loop through them and
     * update or create an EndingTheme object.
     *
     * Link this Anime object to the EndingTheme objects via Eloquent sync
     *
     * @param Collection $endingThemes
     */
    public function addEndingThemes(Collection $endingThemes)
    {
        $this->endingThemes()->sync(
            $endingThemes->map(function (string $endingTheme) {
                return EndingTheme::updateOrCreate(['ending_theme' => $endingTheme])->getKey();
            })->toArray()
        );
    }

    /**
     * Update or create the character image.
     *
     * @param $url
     */
    public function updateOrCreateImage($url)
    {
        $this->image = (new ImageHandler($this->imagePath))->updateOrCreate($this->image, $url);
        if ($this->image !== null) {
            $this->save();
        }
    }

    /**********************************************************************
     * Static functions to get extended Eloquent objects
     **********************************************************************/

    /**
     * Get an Anime Eloquent object extended with related MAL anime fields
     * for 2 given MAL user.
     *
     * @param $malUser1
     * @param $malUser2
     * @return Builder
     */
    public static function qryShared($malUser1, $malUser2)
    {
        $sqlSharedMalAnimes = MalAnime::qryShared($malUser1, $malUser2)
            ->toSql();

        return self::join(
            DB::raw("( $sqlSharedMalAnimes ) as sharedMalAnimes"), 'animes.id', '=', 'sharedMalAnimes.anime_id'
        )
            ->setBindings([$malUser2->id, $malUser1->id, $malUser1->id, $malUser2->id]);
    }

    /**********************************************************************
     * Static functions to get filter values
     **********************************************************************/

    /**
     * Get collection of strings which will be used in the anime filter for classification.
     *
     * @return Collection
     */
    public static function getClassificationFilterValues(): Collection
    {
        $classifications = self::whereNotNull('classification')
            ->select('classification')
            ->distinct()
            ->orderBy('classification')
            ->get()
            ->pluck('classification');
        $classifications = collect($classifications);
        $classifications->prepend('All classifications')
            ->push('Unknown');

        return $classifications;
    }

    /**
     * Get collection of strings which will be used in the anime filter for status.
     *
     * @return Collection
     */
    public static function getStatusFilterValues(): Collection
    {
        $statuses = self::whereNotNull('status')
            ->select('status')
            ->distinct()
            ->orderBy('status')
            ->get()
            ->pluck('status');
        $statuses = collect($statuses);
        $statuses->prepend('All statuses')
            ->push('Unknown');

        return $statuses;
    }

    /**
     * Get collection of strings which will be used in the anime filter for season.
     *
     * @return Collection
     */
    public static function getSeasonFilterValues(): Collection
    {
        return collect(['All seasons', 'Winter', 'Spring', 'Summer', 'Fall']);
    }

    /**
     * Get collection of strings which will be used in the anime filter for year.
     *
     * @return Collection
     */
    public static function getYearsFilterValues(): Collection
    {
        return collect(range(1950, Carbon::now()->year + 1))
            ->push('Unknown')
            ->push('All years')
            ->reverse();
    }
}
