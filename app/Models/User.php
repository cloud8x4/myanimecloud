<?php

namespace App\Models;

use App\Models\MAL\MalAnime;
use App\Models\MAL\MalUser;
use Auth;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laracasts\Presenter\PresentableTrait;

/**
 * Class User.
 */
class User extends Authenticatable
{
    use HasFactory;
    use Notifiable;
    use PresentableTrait;

    /** @var string */
    protected $presenter = 'App\Presenters\User';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**********************************************************************
     * Get Eloquent objects
     **********************************************************************/

    /**
     * Get an Eloquent object for the related MAL user with the same name.
     *
     * @return HasOne
     */
    public function malUser()
    {
        return $this->hasOne(MalUser::class, 'name', 'name');
    }

    /**
     * Get an Eloquent object for all posts of the user.
     *
     * @return HasMany
     */
    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    /**
     * Get an Eloquent object for all related seiyuu
     * with pivot table field rank.
     *
     * @return BelongsToMany
     */
    public function seiyuus()
    {
        return $this
            ->belongsToMany(Seiyuu::class, 'user_seiyuu')
            ->withPivot('rank', 'score_by_character_ranks')
            ->withTimestamps();
    }

    /**
     * Get an Eloquent object for 1 related seiyuu
     * with pivot table field rank.
     *
     * @param Seiyuu $seiyuu
     * @return BelongsToMany
     */
    public function seiyuu(Seiyuu $seiyuu)
    {
        return $this->seiyuus()->where('seiyuus.id', $seiyuu->id);
    }

    /**
     * Get an Eloquent object for all related characters
     * with pivot table field rank.
     *
     * @return BelongsToMany
     */
    public function characters()
    {
        return $this
            ->belongsToMany(Character::class, 'user_character')
            ->withPivot('rank')
            ->withTimestamps();
    }

    /**
     * Get an Eloquent object for 1 related character
     * with pivot table field rank.
     *
     * @param Character $character
     * @return $this
     */
    public function character(Character $character)
    {
        return $this->characters()->where('characters.id', $character->id);
    }

    /**
     * Get an Eloquent object for all related user settings.
     *
     * @return HasMany
     */
    public function settings()
    {
        return $this->hasMany(UserSetting::class);
    }

    /**
     * Dynamic relationship.
     *
     * @return BelongsTo
     */
    public function lastUpdatedMalAnime()
    {
        return $this->belongsTo(MalAnime::class);
    }

    /**
     * Get last updated Mal Anime with this user.
     *
     * @param $query
     */
    public function scopeWithLastUpdatedMalAnime($query)
    {
        $query
            ->addSelect([
                'last_updated_mal_anime_id' => MalAnime
                    ::whereHas('malUser', function ($query) {
                        $query->whereHas('user', function ($query) {
                            $query->where('id', '=', Auth::user()->id);
                        });
                    })
                    ->latest()
                    ->take(1)
                    ->select('id'),
            ])
            ->with([
                'lastUpdatedMalAnime' => function ($query) {
                    $query->select(['id', 'updated_at']);
                },
            ]);
    }

    /**********************************************************************
     * Getter functions
     **********************************************************************/

    /**
     * Get user setting for given name
     * If setting does not exist, return empty string.
     *
     * @param string $name
     * @return string
     */
    public function setting(string $name)
    {
        $setting = $this->settings()->where('name', $name)->first();

        if (empty($setting)) {
            return '';
        } elseif ($setting->value == 'true') {
            return true;
        } elseif ($setting->value == 'false') {
            return false;
        } else {
            return $setting->value;
        }
    }

    /**********************************************************************
     * Get pivot properties
     **********************************************************************/

    /**
     * Get user rank; required to call.
     *
     * @return int|null
     */
    public function seiyuuRank(Seiyuu $seiyuu)
    {
        $seiyuu = $this->seiyuu($seiyuu)->first();

        return $seiyuu ? $seiyuu->pivot->rank : null;
    }

    /**
     * Get user rank; required to call.
     *
     * @return int|null
     */
    public function characterRank(Character $character)
    {
        $character = $this->character($character)->first();

        return $character ? $character->pivot->rank : null;
    }

    /**********************************************************************
     * Setter functions
     **********************************************************************/

    /**
     * Set user setting.
     *
     * @param $name
     * @param $value
     */
    public function setSetting(string $name, string $value)
    {
        UserSetting::updateOrCreate(
            [
                'user_id' => $this->id,
                'name' => $name,
            ],
            ['value' => $value]
        );
    }
}
