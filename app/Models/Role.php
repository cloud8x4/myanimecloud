<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Role.
 */
class Role extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['seiyuu_id', 'character_id', 'anime_id', 'role'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**********************************************************************
     * Get Eloquent objects
     **********************************************************************/

    /**
     * Get an Eloquent object for the only related seiyuu.
     *
     * @return BelongsTo
     */
    public function seiyuu()
    {
        return $this->belongsTo(Seiyuu::class);
    }

    /**
     * Get an Eloquent object for the only related character.
     *
     * @return BelongsTo
     */
    public function character()
    {
        return $this->belongsTo(Character::class);
    }

    /**
     * Get an Eloquent object for the only related anime.
     *
     * @return BelongsTo
     */
    public function anime()
    {
        return $this->belongsTo(Anime::class);
    }
}
