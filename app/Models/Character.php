<?php

namespace App\Models;

use App\Models\Handlers\ImageHandler;
use App\Presenters\Character as CharacterPresenter;
use Auth;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Laracasts\Presenter\PresentableTrait;

/**
 * Class Character.
 */
class Character extends Model
{
    use HasFactory;
    use PresentableTrait;
    use SoftDeletes;

    /** @var string */
    protected $presenter = CharacterPresenter::class;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'MAL_id',
        'name',
        'image',
        'favorited_count',
    ];

    /**
     * Path to character image on local storage.
     *
     * @var string
     */
    private $imagePath = 'character/';

    /**********************************************************************
     * Get Eloquent objects
     **********************************************************************/

    /**
     * Get an Eloquent object for all related roles.
     *
     * @return HasMany
     */
    public function roles(): HasMany
    {
        return $this->hasMany(Role::class);
    }

    /**
     * Get an Eloquent object for all related users
     * with pivot table data timestamps and rank.
     *
     * @return BelongsToMany
     */
    public function users(): BelongsToMany
    {
        return $this
            ->belongsToMany(User::class, 'user_character')
            ->withTimestamps()
            ->withPivot('rank');
    }

    /**
     * Get an Eloquent object for a given user
     * with pivot table data timestamps and rank.
     *
     * @param User $user
     * @return mixed
     */
    public function user(User $user)
    {
        return $this->users()
            ->where('users.id', $user->id);
    }

    /**
     * Get an Eloquent object for current user
     * with pivot table data timestamps and rank.
     *
     * @return mixed
     */
    public function currentUser()
    {
        return $this->user(Auth::user());
    }

    /**********************************************************************
     * Dyanmic relationships using sub queries
     **********************************************************************/

    /**
     * Add field rank.
     *
     * @param $query
     */
    public function scopeWithCurrentUserRank($query)
    {
        $query
            ->addSelect([
                'rank' => DB
                    ::table('user_character')
                    ->whereColumn('character_id', 'characters.id')
                    ->where('user_id', Auth::user()->id)
                    ->select('rank'),
            ]);
    }

    /**********************************************************************
     * Transformation functions
     **********************************************************************/

    /**
     * Set rank for a given user.
     *
     * @param User $user
     * @param string|null $rank
     * @return array
     */
    public function setUserRank(User $user, string $rank = null): array
    {
        $log = collect();
        $log[] = 'Ranking character ' . $this->name . '.';

        $rankedCharacter = $user->character($this)->first();

        $oldRank = $rankedCharacter ? $rankedCharacter->pivot->rank : null;
        $log[] = 'Old rank: ' . ($oldRank ?? 'unranked') . '.';

        if (($rank === null || $rank === '') && $oldRank) {
            $log[] = 'Requested rank: unranked.';

            $this->users()->detach($user->id);
            $message = 'Successfully unranked character ' . $this->name . '.';
            $log[] = $message;

            // Update other character ranks
            $worstRank = $user->characters()->max('rank');
            $userCharacters = DB::table('user_character')
                ->where('user_id', $user->id)
                ->where('character_id', '<>', $this->id);

            if ($worstRank && $oldRank !== $worstRank) {
                $log[] = $userCharacters->whereBetween('rank', [$oldRank, $worstRank])->count() .
                    ' other character(s) with rank between ' . $oldRank . ' and ' . $worstRank . ' will rise 1 rank.';
                $userCharacters->whereBetween('rank', [$oldRank, $worstRank])->decrement('rank');
            }

            return [
                'status' => 'success',
                'message' => $message,
                'log' => $log,
                'newRank' => '?',
            ];
        }

        if (($rank != '' && !ctype_digit($rank)) || (is_numeric($rank) && $rank < 1)) {
            $message = 'Rank of character ' . $this->name . ' was not set to ' . $rank . ' because rank must be a number equal to or higher than 1.';
            $log[] = $message;

            return [
                'status' => 'warning',
                'message' => $message,
                'log' => $log,
            ];
        }

        if ($user->characters()->get()->isEmpty()) {
            $log[] = 'User has not ranked any characters yet. New rank 1.';
            $this->users()->attach($user->id, ['rank' => 1]);
            $message = 'Successfully set rank of character ' . $this->name . ' to 1.';
            $log[] = $message;

            return [
                'status' => 'success',
                'message' => $message,
                'log' => $log,
                'newRank' => 1,
            ];
        }

        $bestRank = $user->characters()->min('rank');
        $worstRank = $user->characters()->max('rank');

        if (($rank === null || $rank === '') && !$oldRank) {
            $log[] = 'Requested rank: empty.';
            $newRank = $worstRank + 1;
            $log[] = 'Since character is unranked, it will get the worst rank ' . $newRank . '.';

            $this->users()->attach($user->id, ['rank' => $newRank]);

            $message = 'Successfully set rank of character ' . $this->name . ' to ' . $newRank . '.';
            $log[] = $message;

            return [
                'status' => 'success',
                'message' => $message,
                'log' => $log,
                'newRank' => $newRank,
            ];
        }

        $newRank = (int)$rank;
        $log[] = 'Requested rank: ' . $rank . '.';

        if ($newRank == $oldRank) {
            $message = 'Rank of character ' . $this->name . ' was not set to ' . $newRank . ' because it already has this rank.';
            $log[] = $message;

            return [
                'status' => 'warning',
                'message' => $message,
                'log' => $log,
            ];
        }

        if ($newRank <= $bestRank) {
            $log[] = 'User has filled in the best rank ' . $bestRank . '.';
            $newRank = 1;
        } elseif ($newRank > $worstRank) {
            $log[] = 'User has filled in a rank ' . $newRank . ' which is worse than the worst rank ' . $worstRank . '. ';
            $newRank = $worstRank;
            if (!$oldRank) {
                $newRank++;
            }
            if ($oldRank == $newRank) {
                $message = 'Rank of character ' . $this->name . ' was not set to ' . $this->rank . ' because it already has this rank.';
                $log[] = $message;

                return [
                    'status' => 'warning',
                    'message' => $message,
                    'log' => $log,
                ];
            }
        } else {
            $log[] = 'User has filled in a rank between worst rank ' . $worstRank . ' and best rank ' . $bestRank . '. ';
        }

        if (!$oldRank) {
            $this->users()->attach($user->id, ['rank' => $newRank]);
        } elseif ($newRank) {
            $this->users()->updateExistingPivot($user->id, ['rank' => $newRank]);
        }
        $message = 'Successfully set rank of character ' . $this->name . ' to ' . $newRank . '.';
        $log[] = $message;

        // Update other character ranks only when necessary
        $userCharacters = DB::table('user_character')
            ->where('user_id', $user->id)
            ->where('character_id', '<>', $this->id);

        if (!$oldRank && $newRank <= $worstRank) {
            $userCharacters = $userCharacters->where('rank', '>=', $newRank);
            $log[] = $userCharacters->count() .
                ' other character(s) with rank ' . $newRank . ' or worse will drop 1 rank.';
            $userCharacters->increment('rank');
            $userCharacters->update(['updated_at' => Carbon::now()]);
        } elseif ($oldRank && $newRank < $oldRank) {
            $userCharacters = $userCharacters->whereBetween('rank', [$newRank, $oldRank]);
            $log[] = $userCharacters->count() .
                ' other character(s) with rank between ' . $newRank . ' and ' . $oldRank . ' will drop 1 rank.';
            $userCharacters->increment('rank');
            $userCharacters->update(['updated_at' => Carbon::now()]);
        } elseif ($oldRank && $newRank > $oldRank) {
            $userCharacters = $userCharacters->whereBetween('rank', [$oldRank, $newRank]);
            $log[] = $userCharacters->count() .
                ' other character(s) with rank between ' . $oldRank . ' and ' . $newRank . ' will rise 1 rank.';
            $userCharacters->decrement('rank');
            $userCharacters->update(['updated_at' => Carbon::now()]);
        }

        return [
            'status' => 'success',
            'message' => $message,
            'log' => $log,
            'newRank' => $newRank,
        ];
    }

    /**
     * Update or create the character image.
     *
     * @param $url
     */
    public function updateOrCreateImage($url)
    {
        $this->image = (new ImageHandler($this->imagePath))->updateOrCreate($this->image, $url);
        if ($this->image !== null) {
            $this->save();
        }
    }
}
