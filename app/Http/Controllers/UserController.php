<?php

namespace App\Http\Controllers;

use App\Models\User;
use Auth;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

/**
 * Class UserController.
 */
class UserController extends Controller
{
    /**
     * Show a list of users.
     *
     * @return Factory|View
     */
    public function list()
    {
        $users = User::paginate(24);

        return view('user.list', compact('users'));
    }

    /**
     * Show the logged in user's profile.
     *
     * @param User $user
     * @return Factory|View
     */
    public function profile(User $user)
    {
        $name = $user->name;

        $email = $user->email;

        $importMessage = null;

        return view('user.profile', compact('name', 'email', 'importMessage'));
    }

    /**
     * Show a given user's page.
     *
     * @param User $user
     * @return Factory|View
     */
    public function user(User $user)
    {
        return view('user.user', compact('user'));
    }

    /**
     * Update user fields and show the logged in user's profile page with feedback of the update.
     *
     * @param Request $request
     * @return Factory|View
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|unique:users,email,' . Auth::user()->id,
            'password' => 'confirmed|min:6',
            'password_confirmation' => 'same:password',
        ]);

        Auth::user()->name = $request->name;
        if (Auth::user()->email != $request->email) {
            Auth::user()->email = $request->email;
        }
        if ($request->password) {
            Auth::user()->password = bcrypt($request->password);
        }
        Auth::user()->save();

        $name = Auth::user()->name;
        $email = Auth::user()->email;
        $updateResult = 'success';
        $importMessage = null;

        return view('user.profile', compact('name', 'email', 'updateResult'));
    }
}
