<?php

namespace App\Http\Controllers;

use App\Models\Anime;
use App\Models\Handlers\AnimeHandler;
use App\Models\Handlers\RoleHandler;
use App\Models\MAL\MalUser;
use App\Models\Role;
use Auth;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\View\View;

/**
 * Class AnimeController.
 */
class AnimeController extends Controller
{
    /**
     * For logged in user,
     * show a list of animes and roles with seiyuu and character.
     *
     * Create an anime handler object
     * which takes care of filtering and sorting
     * and contains an extended Anime Eloquent object
     *
     * Get a LengthAwarePaginator object
     * which contains 20 Anime objects
     * ordered by descending members score and ascending id
     *
     * For each Anime object
     * get 12 Role objects grouped per character so you see each character only once
     * with their related Character object and Seiyuu object
     * ordered by ascending role (Main / Supporting), descending seiyuu favorite count and ascending seiyuu id
     *
     * Get anime list user settings
     *
     * @return Factory|View
     */
    public function roles(Request $request)
    {
        $animeHandler = new AnimeHandler();

        $animeHandler->filter($request);

        $animeHandler->sort($request);

        $animes = $animeHandler->getAnimes()->paginate(10);

        $roleHandler = new RoleHandler();

        $roleHandler->setFilter($request);

        $animes->each(function (Anime $anime) use ($roleHandler) {
            $roleHandler->setRoles($anime->rolesGroupedByCharacter());

            $roleHandler->filter();

            $roleHandler->sort();

            $anime->roles = $roleHandler
                ->getRoles()
                ->limit(12)
                ->get();
        });

        $filter = $animeHandler
            ->getFilter()
            ->merge($roleHandler->getFilter());

        $sort = $animeHandler->getSort();

        $settings = $this->rolesSettings();

        return view('anime.list', compact('animes', 'filter', 'sort', 'settings'));
    }

    /**
     * Define list of settings and get values from database.
     *
     * @return Collection
     */
    private function rolesSettings()
    {
        return collect(
            [
                'animeListVisibilityAnimeTitle' => true,
                'animeListVisibilityAnimeType' => true,
                'animeListVisibilityAnimeEpisodes' => true,
                'animeListVisibilityAnimeSeasonStarted' => true,
                'animeListVisibilityAnimeMyMalScore' => true,
                'animeListVisibilityAnimeMembersScore' => true,
                'animeListVisibilityCharacterName' => true,
                'animeListVisibilityCharacterGender' => false,
                'animeListVisibilitySeiyuuName' => true,
                'animeListVisibilitySeiyuuGender' => false,
            ]
        )
            ->when(Auth::user(), function (Collection $settings) {
                return $settings->map(function (bool $defaultValue, string $setting) {
                    $value = Auth::user()->setting($setting);

                    return empty($value) ? $defaultValue : $value;
                });
            });
    }

    /**
     * For 1 given anime show a list characters, seiyuus and related roles with seiyuu and character
     * EXCEPT 1 given character.
     *
     * Get an Anime object for a given id
     * Add a LengthAwarePaginator object wich contains 20 Role objects
     * EXCEPT 1 given Character object
     * ordered by ascending role (Main / Supporting), descending seiyuu favorited count and ascending seiyuu id
     *
     * Each Role object is grouped per character so you see each character only once
     * with their related Character object and Anime object
     * ordered by ascending role (Main / Supporting) and descending anime members score
     *
     * @param $id
     * @return Factory|View
     */
    public function anime($id)
    {
        /* @var Anime $anime */
        $anime = Anime::find($id);

        $roleHandler = new RoleHandler();

        $roleHandler->setRoles($anime->rolesGroupedByCharacter());

        $roleHandler->sort();

        $anime->roles = $roleHandler->getRoles()->paginate(10);

        $anime->roles->each(function (Role $role) use ($roleHandler) {
            $roleHandler->setRoles($role->seiyuu->alternativeRolesGroupedByCharacter($role->character));

            $roleHandler->sort();

            $role->seiyuu->roles = $roleHandler
                ->getRoles()
                ->limit(12)
                ->get();
        });

        return view('anime.index', compact('anime'));
    }

    /**
     * For 1 given MAL username show a list of animes
     * which are on the list of the logged in user or on the list of the given user.
     *
     * @param string $userName
     * @return Factory|RedirectResponse|View
     */
    public function shared(Request $request)
    {
        $malUserName = '';

        if ($request->malUserName) {
            $malUserName = $request->malUserName;
        } elseif ($request->session()->get('malUserName')) {
            $malUserName = $request->session()->get('malUserName');
        }
        if ($malUserName) {
            $malUser1 = Auth::user()->malUser;

            $malUser2 = MalUser::where('name', $malUserName)->first();

            $request->session()->put('malUserName', $malUserName);

            if ($malUser1 && $malUser2) {
                $animes = Anime::qryShared($malUser1, $malUser2)->paginate(10);

                return view('anime.shared', compact('animes', 'malUser1', 'malUser2'));
            }
        }

        return view('anime.shared.mal_user_name_not_selected', compact(['malUserName']));
    }

    /**
     * Show form to add or update anime
     * Unique key is MAL id so when nothing is filled in except MAL id, all fields are overwritten.
     *
     * @TODO: when filling in MAL id get current fields in database for this MAL id
     *
     * @return Factory|View
     */
    public function add()
    {
        return view('anime.add');
    }
}
