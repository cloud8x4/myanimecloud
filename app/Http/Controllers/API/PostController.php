<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Support\Collection;

class PostController extends Controller
{
    /**
     * Get all posts.
     *
     * @return Collection
     */
    public function index()
    {
        return Post::orderBy('created_at', 'desc')->get();
    }
}
