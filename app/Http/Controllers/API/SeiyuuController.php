<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Seiyuu;
use Auth;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

/**
 * Class SeiyuuController.
 */
class SeiyuuController extends Controller
{
    /**
     * Add a new seiyuu or update an existing seiyuu if it does not exist yet.
     *
     * @param Request $request
     * @return Factory|View
     */
    public function postForm(Request $request)
    {
        $MAL_id = $request->MAL_id;
        $name = $request->name;
        $birthday = $request->birthday;
        $image = $request->image;

        // Start validation

        $rules = [
            'name' => 'required',
            'MAL_id' => 'integer',
            'birthday' => 'date',
            'image' => 'url',
        ];

        $this->validate($request, $rules);

        // End validation

        // Get image from url and store locally
        $newName = uniqid();
        $path_info = pathinfo($image);
        $newFullPath = 'storage/seiyuu/' . $newName . '.' . $path_info['extension'];

        copy($image, $newFullPath);
        $image = '/' . $newFullPath;

        // Save object in database
        $Seiyuu = Seiyuu::updateOrCreate(
            ['name' => $name],
            [
                'MAL_id' => empty($MAL_id) ? null : $MAL_id,
                'birthday' => empty($birthday) ? null : $birthday,
                'image' => empty($image) ? null : $image,
            ]
        );

        return $this->seiyuu($Seiyuu->id);
    }

    /**
     * Set gender for seiyuu.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function postGender(Request $request)
    {
        $seiyuu = Seiyuu::find((int)$request->id);

        $seiyuu->gender = $request->gender;

        $seiyuu->save();

        return response()->json([
            'result' => 'Successfully set gender of seiyuu ' . $seiyuu->name . ' to ' . $request->gender,
        ]);
    }

    /**
     * Set rank for current user for valid input.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function rank(Request $request)
    {
        $seiyuu = Seiyuu::find((int)$request->id);

        $result = $seiyuu->setUserRank(Auth::user(), $request->rank);

        return response()->json($result);
    }
}
