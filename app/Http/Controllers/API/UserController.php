<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use Auth;
use Illuminate\Http\Client\RequestException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class UserController extends Controller
{
    /**
     * Import the anime list for a given username.
     *
     * @param Request $request
     * @throws RequestException
     * @return JsonResponse
     */
    public function import(Request $request): JsonResponse
    {
        $validated = $request->validate([
            'name' => 'required|string',
        ]);

        $myAnimeListUserController = new MyAnimeList\UserController();

        $myAnimeListUserController->animelist(Arr::get($validated, 'name'));

        return response()->json();
    }

    /**
     * Get the current user with the time of his last updated related mal anime.
     *
     * @return User
     */
    public function lastMalImport()
    {
        return User
            ::where('id', '=', Auth::user()->id)
            ->select(['id', 'name', 'email'])
            ->withLastUpdatedMalAnime()
            ->first();
    }

    /**
     * Get a random character from the user's ranked characters.
     *
     * @return JsonResponse
     */
    public function randomCharacter()
    {
        $character = Auth::user()
            ->characters()
            ->inRandomOrder()
            ->first();

        if (!$character) {
            return response()->json(['warning' => 'No characters found']);
        }

        return $character;
    }

    /**
     * Save a user's setting and return feedback.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function setSetting(Request $request)
    {
        Auth::user()->setSetting($request->name, $request->value);

        return response()->json(['result' => 'Successfully set ' . $request->name . ' to ' . $request->value]);
    }
}
