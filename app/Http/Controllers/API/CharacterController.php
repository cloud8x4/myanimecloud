<?php

namespace App\Http\Controllers\API;

use App\Events\CharacterRankChanged;
use App\Http\Controllers\Controller;
use App\Models\Character;
use Auth;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

/**
 * Class CharacterController.
 */
class CharacterController extends Controller
{
    /**
     * Add a new character or update an existing character if it does not exist yet.
     *
     * @param Request $request
     * @return Factory|View
     */
    public function postForm(Request $request)
    {
        $MAL_id = $request->MAL_id;
        $name = $request->name;
        $image = $request->image;

        // Start validation

        $rules = [
            'name' => 'required',
            'MAL_id' => 'integer',
            'image' => 'url',
        ];

        $this->validate($request, $rules);

        // End validation

        // Get image from url and store locally
        $newName = uniqid();
        $path_info = pathinfo($image);
        $newFullPath = 'storage/character/' . $newName . '.' . $path_info['extension'];

        copy($image, $newFullPath);
        $image = '/' . $newFullPath;

        // Save object in database
        $character = Character::updateOrCreate(
            ['name' => $name],
            [
                'MAL_id' => empty($MAL_id) ? null : $MAL_id,
                'image' => empty($image) ? null : $image,
            ]
        );

        return $this->character($character->id);
    }

    /**
     * Set gender for character.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function postGender(Request $request)
    {
        $character = Character::find((int)$request->id);

        $character->gender = $request->gender;

        $character->save();

        return response()->json([
            'result' => 'Successfully set gender of character ' . $character->name . ' to ' . $request->gender,
        ]);
    }

    /**
     * Set rank for current user for valid input.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function rank(Request $request)
    {
        /* @var Character $character */
        $character = Character::find((int)$request->id);

        $result = $character->setUserRank(Auth::user(), $request->rank);

        event(new CharacterRankChanged(Auth::user()));

        return response()->json($result);
    }
}
