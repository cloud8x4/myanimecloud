<?php

namespace App\Http\Controllers\API\Jikan;

use App\Http\Controllers\Controller;
use App\Models\Anime;
use App\Models\Character;
use App\Models\Role;
use App\Models\Seiyuu;
use Carbon\Carbon;

class JikanPersonController extends Controller
{
    public function person($mal_id)
    {
        $jikanPerson = json_decode(
            file_get_contents('https://api.jikan.moe/v3/person/' . $mal_id)
        );

        if (!empty($jikanAnime->error)) {
            $seiyuu = Seiyuu::where('MAL_id', '=', $mal_id)->first();

            // TODO: this view can only be used when using a manual import, but automatic imports should be handled differently.
            return view('errors.import.seiyuu', compact('seiyuu', 'error'));
        }

        /* @var Seiyuu $seiyuu */
        $seiyuu = Seiyuu::updateOrCreate(
            ['MAL_id' => $jikanPerson->mal_id],
            [
                'name' => $jikanPerson->name,
                'given_name' => empty($jikanPerson->given_name) ? null : $jikanPerson->given_name,
                'family_name' => empty($jikanPerson->family_name) ? null : $jikanPerson->family_name,
                'birthday' => empty($jikanPerson->birthday) ? null : new Carbon($jikanPerson->birthday),
                'favorited_count' => $jikanPerson->member_favorites,
            ]
        );

        $seiyuu->updateOrCreateImage($jikanPerson->image_url);

        $jikanVoiceActingRoles = collect($jikanPerson->voice_acting_roles);

        $jikanVoiceActingRoles->each(function ($jikanRole) use ($seiyuu) {
            $jikanCharacter = $jikanRole->character;

            /* @var Character $character */
            $character = Character::updateOrCreate(
                ['MAL_id' => $jikanCharacter->mal_id],
                ['name' => $jikanCharacter->name]
            );

            $character->updateOrCreateImage($jikanCharacter->image_url);

            $jikanAnime = $jikanRole->anime;

            /* @var Anime $anime */
            $anime = Anime::updateOrCreate(
                ['MAL_id' => $jikanAnime->mal_id],
                ['title' => $jikanAnime->name]
            );

            $anime->updateOrCreateImage($jikanAnime->image_url);

            Role::updateOrCreate(
                [
                    'seiyuu_id' => $seiyuu->id,
                    'character_id' => $character->id,
                    'anime_id' => $anime->id,
                ],
                ['role' => $jikanRole->role]
            );
        });

        // TODO: this view can only be used when using a manual import, but automatic imports should be handled differently.
        return view('import.MAL.people', compact('seiyuu'));
    }
}
