<?php

namespace App\Http\Controllers\API\Jikan;

use App\Http\Controllers\Controller;
use App\Models\Anime;
use App\Models\Character;
use App\Models\Role;
use App\Models\Seiyuu;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;

class AnimeController extends Controller
{
    private const URL = 'https://api.jikan.moe/v4/';

    /**
     * Import an anime.
     *
     * @param int $malId
     * @return Application|Factory|View
     */
    public function anime(int $malId)
    {
        $endpoint = self::URL . 'anime/' . $malId;

        try {
            $response = Http::acceptJson()->get($endpoint)->throw()->json();
        } catch (Exception $e) {
            return view('errors.import.anime', ['malId' => $malId]);
        }

        $data = Arr::get($response, 'data');

        /* @var Anime $anime */
        $anime = Anime::updateOrCreate(
            ['MAL_id' => $malId],
            [
                'title' => Arr::get($data, 'title'),
                'type' => Arr::get($data, 'type'),
                'episodes' => Arr::get($data, 'episodes'),
                'duration' => Arr::get($data, 'episodes') === 'Unknown' ? null : explode(' ', Arr::get($data, 'episodes'))[0],
                'status' => Arr::get($data, 'status'),
                'start_date' => empty(Arr::get($data, 'aired.from')) ? null : new Carbon(Arr::get($data, 'aired.from')),
                'end_date' => empty(Arr::get($data, 'aired.to')) ? null : new Carbon(Arr::get($data, 'aired.to')),
                'classification' => Arr::get($data, 'rating'),
                'members_score' => Arr::get($data, 'score'),
                'members_count' => Arr::get($data, 'members'),
                'rank' => Arr::get($data, 'rank'),
                'popularity_rank' => Arr::get($data, 'popularity'),
                'favorited_count' => Arr::get($data, 'favorites'),
            ]);

        $anime->updateOrCreateImage(Arr::get($data, 'images.jpg.image_url'));

        $anime->addProducers(collect(Arr::get($data, 'producers'))->pluck('name'));
        $anime->addGenres(collect(Arr::get($data, 'genres'))->pluck('name'));
        $anime->addOpeningThemes(collect(Arr::get($data, 'theme.openings')));
        $anime->addEndingThemes(collect(Arr::get($data, 'theme.endings')));

        $this->characters($malId, $anime);

        return view('import.MAL.anime', compact('anime'));
    }

    /**
     * Import characters for a given MAL id.
     * Optionally the corresponding anime can be provided as second parameter.
     *
     * @param int $malId
     * @param Anime|null $anime
     * @return Application|Factory|View
     */
    public function characters(int $malId, ?Anime $anime = null)
    {
        $endpoint = self::URL . 'anime/' . $malId . '/characters';

        if ($anime === null) {
            $anime = Anime::where('MAL_id', $malId)->first();
        }

        try {
            $response = Http::acceptJson()->get($endpoint)->throw()->json();
        } catch (Exception $e) {
            return view('errors.import.animecast', ['malId' => $malId]);
        }

        $data = Arr::get($response, 'data');

        if (empty($data)) {
            return view('import.MAL.animecastempty', compact('anime'));
        }

        collect($data)
            ->filter(function ($record) {
                return empty(Arr::get($record, 'character.mal_id')) === false;
            })
            ->each(function ($record) use ($anime) {
                $this->handleRecord($record, $anime->getKey());
            });

        return view('import.MAL.animecast', compact('anime'));
    }

    /**
     * Handle a record from the Jikan data and a given anime id.
     *
     * @param array $record
     * @param int $animeId
     * @return void
     */
    private function handleRecord(array $record, int $animeId)
    {
        $jikanRole = Arr::get($record, 'role');

        $character = Character::updateOrCreate(
            ['MAL_id' => Arr::get($record, 'character.mal_id')],
            [
                'name' => Arr::get($record, 'character.name'),
                'favorited_count' => Arr::get($record, 'favorites'),
            ]
        );

        $character->updateOrCreateImage(Arr::get($record, 'character.images.jpg.image_url'));

        if (empty(Arr::get($record, 'voice_actors'))) {
            Role::updateOrCreate(
                [
                    'seiyuu_id' => Seiyuu::where('name', 'unknown')->first()->getKey(),
                    'character_id' => $character->getKey(),
                    'anime_id' => $animeId,
                ],
                ['role' => $jikanRole]
            );
        } else {
            collect(Arr::get($record, 'voice_actors'))
                ->filter(function ($jikanVoiceActor) {
                    return Arr::get($jikanVoiceActor, 'language') === 'Japanese';
                })
                ->each(function ($jikanVoiceActor) use ($animeId, $character, $jikanRole) {
                    $this->handlePerson(Arr::get($jikanVoiceActor, 'person'), $jikanRole, $animeId, $character->getKey());
                });
        }
    }

    /**
     * Handle a person from the Jikan data voice actors and given Jikan role, anime id, character id..
     *
     * @param array $jikanPerson
     * @param int $animeId
     * @param int $characterId
     * @param string $jikanRole
     * @return void
     */
    private function handlePerson(array $jikanPerson, string $jikanRole, int $animeId, int $characterId)
    {
        /* @var Seiyuu $seiyuu */
        $seiyuu = Seiyuu::updateOrCreate(
            ['MAL_id' => Arr::get($jikanPerson, 'mal_id')],
            ['name' => Arr::get($jikanPerson, 'name')]
        );

        $seiyuu->updateOrCreateImage(Arr::get($jikanPerson, 'images.jpg.image_url'));

        Role::updateOrCreate(
            [
                'seiyuu_id' => $seiyuu->getKey(),
                'character_id' => $characterId,
                'anime_id' => $animeId,
            ],
            ['role' => $jikanRole]
        );

        // Delete role for unknown seiyuu
        Role::where('seiyuu_id', Seiyuu::where('name', 'unknown')->first()->getKey())
            ->where('anime_id', $animeId)
            ->where('character_id', $characterId)
            ->delete();
    }
}
