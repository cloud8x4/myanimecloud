<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Character;
use App\Models\Handlers\AnimeHandler;
use App\Models\Seiyuu;
use Auth;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    /**
     * Search for animes, characters and seiyuu.
     *
     * @return JsonResponse
     */
    public function search(Request $request)
    {
        return response()->json([
            'animes' => $this->searchAnimes($request),
            'characters' => $this->searchCharacters($request),
            'seiyuus' => $this->searchSeiyuu($request),
        ]);
    }

    /**
     * Search for animes.
     *
     * @param Request $request
     * @return mixed
     */
    private function searchAnimes(Request $request)
    {
        $handler = new AnimeHandler();

        $handler->search($request->search);

        $handler->sort($request);

        return $handler
            ->getAnimes()
            ->limit(6)
            ->select([
                'animes.id',
                'episodes',
                'image',
                'members_count',
                'title',
                'type',
            ])
            ->get();
    }

    /**
     * Search for animes.
     *
     * @param Request $request
     * @return mixed
     */
    private function searchCharacters(Request $request)
    {
        return Character
            ::where('name', 'like', '%' . $request->search . '%')
            ->when(Auth::user(), function ($query) {
                /* @var Character $query */
                $query
                    ->withCurrentUserRank()
                    ->orderByRaw('rank is null')
                    ->orderBy('rank', 'asc');
            })
            ->orderBy('favorited_count', 'desc')
            ->limit(6)
            ->get()
            ->map(function (Character $character) {
                return $character->only([
                    'favorited_count',
                    'id',
                    'image',
                    'name',
                    'rank',
                ]);
            });
    }

    private function searchSeiyuu(Request $request)
    {
        return Seiyuu
            ::where('name', 'like', '%' . $request->search . '%')
            ->when(Auth::user(), function ($query) {
                /* @var Seiyuu $query */
                $query
                    ->withCurrentUserRank()
                    ->orderByRaw('rank is null')
                    ->orderBy('rank', 'asc');
            })
            ->orderBy('favorited_count', 'desc')
            ->limit(6)
            ->get()
            ->map(function (Seiyuu $seiyuu) {
                return $seiyuu->only([
                    'favorited_count',
                    'id',
                    'image',
                    'name',
                    'rank',
                ]);
            });
    }
}
