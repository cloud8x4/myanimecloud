<?php

namespace App\Http\Controllers\API;

use App\Events\RoleWeightChanged;
use App\Http\Controllers\Controller;
use App\Models\Role;
use Auth;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * For a for a given role id,
     * get all roles with the same character, seiyuu, role and
     * set the new weight.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function weight(Request $request)
    {
        $role = Role::find((int)$request->id);

        Role::where('seiyuu_id', '=', $role->seiyuu_id)
            ->where('character_id', '=', $role->character_id)
            ->where('role', '=', $role->role)
            ->update(['weight' => $request->weight]);

        event(new RoleWeightChanged(Auth::user()));

        return response()->json(['newWeight' => $request->weight]);
    }
}
