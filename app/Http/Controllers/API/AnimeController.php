<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Anime;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

/**
 * Class AnimeController.
 */
class AnimeController extends Controller
{
    /**
     * Add a new anime or update an existing anime if it does not exist yet.
     *
     * @param Request $request
     * @return Factory|View
     */
    public function postForm(Request $request)
    {
        $title = $request->title;
        $MAL_id = $request->MAL_id;
        $episodes = $request->episodes;
        $members_score = $request->members_score;
        $type = $request->type;
        $status = $request->status;
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $image = $request->image;

        // Start validation

        $rules = [
            'title' => 'required',
            'MAL_id' => 'integer',
            'episodes' => 'integer|digits_between:1,5',
            'members_score' => 'numeric|min:0|max:10',
            'type' => 'string',
            'status' => 'string',
            'start_date' => 'date',
            'end_date' => 'date',
            'image' => 'url',
        ];

        $this->validate($request, $rules);

        // End validation

        // Get image from url and store locally
        $newName = uniqid();
        $path_info = pathinfo($image);
        $newFullPath = 'storage/anime/' . $newName . '.' . $path_info['extension'];

        copy($image, $newFullPath);
        $image = '/' . $newFullPath;

        // Save object in database
        $anime = Anime::updateOrCreate(
            ['title' => $title],
            [
                'MAL_id' => empty($MAL_id) ? null : $MAL_id,
                'episodes' => empty($episodes) ? null : $episodes,
                'members_score' => empty($members_score) ? null : $members_score,
                'type' => empty($type) ? null : $type,
                'status' => empty($status) ? null : $status,
                'start_date' => empty($start_date) ? null : $start_date,
                'end_date' => empty($end_date) ? null : $end_date,
                'image' => empty($image) ? null : $image,
            ]
        );

        return $this->anime($anime->id);
    }
}
