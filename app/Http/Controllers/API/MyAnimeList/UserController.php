<?php

namespace App\Http\Controllers\API\MyAnimeList;

use App\Http\Controllers\Controller;
use App\Models\Anime;
use App\Models\MAL\MalAnime;
use App\Models\MAL\MalUser;
use Carbon\Carbon;
use Illuminate\Http\Client\RequestException;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class UserController extends Controller
{
    private const URL = 'https://api.myanimelist.net/v2/';

    /**
     * Import the anime list for a given username.
     *
     * @param string $username
     * @throws RequestException
     * @return string
     */
    public function animelist(string $username): string
    {
        ini_set('max_execution_time', 900); // 900 seconds = 15 minutes

        $fields = [
            'start_date',
            'end_date',
            'mean',
            'rank',
            'popularity',
            'num_list_users',
            'media_type',
            'status',
            'list_status',
            'num_episodes',
            'average_episode_duration',
            'rating',
            'genres',
            'studios',
        ];

        $endpoint = self::URL . 'users/' . $username . '/animelist?limit=1000&fields=' . implode(',', $fields);

        while ($endpoint !== null) {
            $response = Http::acceptJson()->withHeaders(['X-MAL-CLIENT-ID' => env('MY_ANIME_LIST_CLIENT_ID')])->get($endpoint)->throw()->json();

            foreach (Arr::get($response, 'data') as $record) {
                $animeId = $this->handleNode(Arr::get($record, 'node'));
                $this->handleListStatus($username, $animeId, Arr::get($record, 'list_status'));
            }

            $endpoint = Arr::get($response, 'paging.next');
        }

        return 'Import of animelist of user <a href="https://myanimelist.net/profile/' .
            $username . '" target="_blank">' . $username . '</a>  succesful';
    }

    /**
     * Handle an imported MyAnimeList record node.
     *
     * @param array $node
     * @return int
     */
    private function handleNode(array $node): int
    {
        $classifications = [
            'g' => 'G - All Ages',
            'pg' => 'PG - Children',
            'pg_13' => 'PG-13 - Teens 13 or older',
            'r+' => 'R+ - Mild Nudity',
            'r' => 'R - 17+ (violence & profanity)',
            'rx' => 'Rx - Hentai',
            null => 'None',
        ];

        /* @var Anime $anime */
        $anime = Anime::updateOrCreate(
            ['MAL_id' => Arr::get($node, 'id')],
            [
                'title' => Arr::get($node, 'title'),
                'episodes' => Arr::get($node, 'num_episodes'),
                'type' => Arr::get($node, 'media_type'),
                'status' => Str::replace('_', ' ', Arr::get($node, 'status')),
                'duration' => Arr::get($node, 'num_episodes'),
                'start_date' => Arr::get($node, 'start_date') === null ? null : new Carbon(Arr::get($node, 'start_date')),
                'end_date' => Arr::get($node, 'end_date') === null ? null : new Carbon(Arr::get($node, 'end_date')),
                'classification' => Arr::get($classifications, Arr::get($node, 'rating'), Arr::get($node, 'rating')),
                'members_score' => Arr::get($node, 'mean'),
                'members_count' => Arr::get($node, 'num_list_users'),
                'rank' => Arr::get($node, 'rank'),
                'popularity_rank' => Arr::get($node, 'popularity'),
            ]);

        $anime->updateOrCreateImage(Arr::get($node, 'main_picture.medium'));

        $anime->addGenres(collect(Arr::get($node, 'genres'))->pluck('name'));
        $anime->addProducers(collect(Arr::get($node, 'studios'))->pluck('name'));

        return $anime->id;
    }

    /**
     * Handle an imported MyAnimeList record list status.
     *
     * @param string $username
     * @param int $animeId
     * @param array $listStatus
     * @return void
     */
    private function handleListStatus(string $username, int $animeId, array $listStatus)
    {
        $malUser = MalUser::where('name', $username)->first();

        // @TODO: update anime_days (watched)

        if ($malUser === null) {
            $malUser = MalUser::make();
            $malUser->name = $username;
            $malUser->anime_days = -1;
            $malUser->save();
        }

        MalAnime::updateOrCreate(
            [
                'mal_user_id' => $malUser->id,
                'anime_id' => $animeId,
            ],
            [
                'watched_status' => Str::replace('_', ' ', Arr::get($listStatus, 'status')),
                'watched_episodes' => Arr::get($listStatus, 'num_episodes_watched'),
                'score' => Arr::get($listStatus, 'score'),
            ]);
    }
}
