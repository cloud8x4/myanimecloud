<?php

namespace App\Http\Controllers;

use App\Models\Handlers\RoleHandler;
use App\Models\Handlers\SeiyuuHandler;
use App\Models\Seiyuu;
use App\Models\User;
use Auth;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\View\View;

/**
 * Class SeiyuuController.
 */
class SeiyuuController extends Controller
{
    /**
     * For logged in user,
     * create a Seiyuu handler which handles filtering and sorting based on selection
     * and get 20 seiyuu.
     *
     * For each Seiyuu object
     * get 12 Role objects grouped per character so you see each character only once
     * with their related Character object and Anime object
     * ordered by ascending role (Main / Supporting), descending anime members score and ascending anime id
     *
     * Get seiyuu list user settings
     *
     * @return Factory|View
     */
    public function roles(Request $request)
    {
        $seiyuuHandler = new SeiyuuHandler();

        $seiyuuHandler->initQuery();

        if (Auth::user()) {
            $seiyuuHandler->setUser(Auth::user());

            $seiyuuHandler->leftJoinCurrentUserSeiyuu();
        }

        $seiyuuHandler->filter($request);

        $seiyuuHandler->sort($request);

        $seiyuus = $seiyuuHandler
            ->getSeiyuus()
            ->with('roles')
            ->paginate(10);

        $roleHandler = new RoleHandler();

        $roleHandler->setFilter($request);

        $seiyuus->map(function (Seiyuu $seiyuu) use ($roleHandler) {
            $roleHandler->setRoles($seiyuu->rolesGroupedByCharacter());

            $roleHandler->filter();

            $roleHandler->sort();

            $seiyuu->roles = $roleHandler
                ->getRoles()
                ->limit(12)
                ->get();
        });

        $filter = $seiyuuHandler
            ->getFilter()
            ->merge($roleHandler->getFilter());

        $sort = $seiyuuHandler->getSort();

        $settings = $this->rolesSettings();

        return view('seiyuu.list', compact('seiyuus', 'filter', 'sort', 'settings'));
    }

    /**
     * Define list of settings and get values from database.
     *
     * @return Collection
     */
    private function rolesSettings()
    {
        return collect([
            'seiyuuListVisibilitySeiyuuName' => true,
            'seiyuuListVisibilitySeiyuuRank' => true,
            'seiyuuListVisibilitySeiyuuScoreByCharacterRanks' => true,
            'seiyuuListVisibilitySeiyuuFavoritedCount' => true,
            'seiyuuListVisibilitySeiyuuAge' => true,
            'seiyuuListVisibilitySeiyuuBirthday' => true,
            'seiyuuListVisibilityCharacterName' => true,
            'seiyuuListVisibilityAnimeTitle' => true,
        ])
            ->when(Auth::user(), function (Collection $settings) {
                return $settings->map(function (bool $defaultValue, string $setting) {
                    $value = Auth::user()->setting($setting);

                    return empty($value) ? $defaultValue : $value;
                });
            });
    }

    /**
     * Get a Seiyuu object for a given id
     * Add a LengthAwarePaginator object wich contains 50 Role objects.
     *
     * Each Role object is grouped per character so you see each character only once
     * with their related Character object and Anime object
     * ordered by ascending role (Main / Supporting), descending anime members score and ascending anime id
     *
     * @param $id
     * @return Factory|View
     */
    public function seiyuu($id)
    {
        /* @var Seiyuu $seiyuu */
        $seiyuu = Seiyuu::find($id);

        $roleHandler = new RoleHandler();

        $roleHandler->setRoles($seiyuu->rolesGroupedByCharacter());

        $roleHandler->sort();

        $seiyuu->roles = $roleHandler->getRoles()->paginate(10);

        return view('seiyuu.index', compact('seiyuu'));
    }

    /**
     * Get a list of a given user's seiyuus
     * If no user is given, then seiyuus of logged in user are retrieved.
     *
     * @param User $user
     * @param Request $request
     * @return Factory|View
     */
    public function top(User $user, Request $request)
    {
        if (!$user->id) {
            if (!Auth::user()) {
                return view('errors.no_permission');
            }

            $user = Auth::user();
        }

        $seiyuuHandler = new SeiyuuHandler();

        $seiyuuHandler->initQuery();

        $seiyuuHandler->setUser($user);

        if (Auth::user()) {
            $seiyuuHandler->leftJoinCurrentUserSeiyuu();
        }

        if ($user !== Auth::user()) {
            $seiyuuHandler->leftJoinUserSeiyuu();
        }

        $seiyuuHandler->filter($request);

        $seiyuuHandler->sort($request);

        $seiyuus = $seiyuuHandler->getSeiyuus()->paginate(48);

        $filter = $seiyuuHandler->getFilter();

        $sort = $seiyuuHandler->getSort();

        return view('seiyuu.top', compact('user', 'seiyuus', 'filter', 'sort'));
    }

    /**
     * Show form to add or update seiyuu
     * Unique key is MAL id so when nothing is filled in except MAL id, all fields are overwritten.
     *
     * @TODO: when filling in MAL id get current fields in database for this MAL id
     *
     * @return Factory|View
     */
    public function add()
    {
        return view('seiyuu.add');
    }
}
