<?php

namespace App\Http\Controllers;

use App\Models\Anime;
use App\Models\Character;
use App\Models\Handlers\CharacterHandler;
use App\Models\Role;
use App\Models\User;
use Auth;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

/**
 * Class CharacterController.
 */
class CharacterController extends Controller
{
    /**
     * For logged in user,
     * create a Character handler which handles filtering and sorting based on selection
     * and get 20 characters.
     *
     * For each Character object
     * get 6 Role objects
     * with their related Character object and Anime object
     *
     * Finally get 2 separate collections of
     * 1. unique Seiyuu objects ordered by descending favorite count and ascending id
     * 2. unique Anime objects ordered by descending members score and ascending id
     *
     * Get character list user settings
     *
     * @return Factory|View
     */
    public function roles(Request $request)
    {
        $characterHandler = new CharacterHandler();

        $characterHandler->initQuery();

        if (Auth::user()) {
            $characterHandler->setUser(Auth::user());

            $characterHandler->leftJoinCurrentUserCharacter();
        }

        $characterHandler->filter($request);

        $characterHandler->sort($request);

        $characters = $characterHandler
            ->getCharacters()
            ->with('roles')
            ->paginate(10);

        $characters->each(function (Character $character) {
            $roles = $character
                ->roles()
                ->limit(12)
                ->with('seiyuu');

            if (Auth::user() && Auth::user()->malUser) {
                $roles = $roles->with('anime.myMalAnime');
            }

            $roles = $roles->get();

            $character->seiyuus = $roles
                ->pluck('seiyuu')
                ->unique()
                ->sortByDesc('favorited_count')
                ->sortBy('id');

            // Remove unknown seiyuu if multiple seiyuus belong to a character
            if ($character->seiyuus->count() > 1) {
                $character->seiyuus = $character->seiyuus->filter(function ($seiyuu) {
                    return $seiyuu->name !== 'unknown';
                });
            }

            $character->animes = $roles
                ->pluck('anime')
                ->unique()
                ->sortByDesc('members_score')
                ->sortBy('id');
        });

        $filter = $characterHandler->getFilter();

        $sort = $characterHandler->getSort();

        $settings = $this->rolesSettings();

        return view('character.list', compact('characters', 'filter', 'sort', 'settings'));
    }

    /**
     * Define list of settings and get values from database.
     *
     * @return Collection
     */
    private function rolesSettings()
    {
        return collect(
            [
                'characterRolesVisibilityCharacterName' => true,
                'characterRolesVisibilityCharacterRole' => true,
                'characterRolesVisibilityCharacterRank' => true,
                'characterRolesVisibilitySeiyuuName' => true,
                'characterRolesVisibilitySeiyuuRank' => true,
                'characterRolesVisibilitySeiyuuScoreByCharacterRanks' => true,
                'characterRolesVisibilityAnimeTitle' => true,
                'characterRolesVisibilityAnimeMyMalScore' => true,
            ]
        )
            ->when(Auth::user(), function (Collection $settings) {
                return $settings->map(function (bool $defaultValue, string $setting) {
                    $value = Auth::user()->setting($setting);

                    return empty($value) ? $defaultValue : $value;
                });
            });
    }

    /**
     * Get a Character object for a given id
     * Add a LengthAwarePaginator object wich contains 20 Role objects
     * ordered by ascending role (Main / Supporting) and ascending seiyuu id
     * with their related Character object and Anime object.
     *
     * @param $id
     * @return Factory|View
     */
    public function character($id)
    {
        /* @var Character $character */
        $character = Character::find($id);

        $roles = $character
            ->roles()
            ->groupBy(['seiyuu_id', 'role'])
            ->select([
                DB::raw('MIN(id) as id'),
                DB::raw('MIN(role) as role'),
                DB::raw('MAX(weight) as weight  '),
                DB::raw('MIN(seiyuu_id) as seiyuu_id'),
                DB::raw('GROUP_CONCAT(anime_id) as anime_ids'),
            ])
            ->orderBy('role')
            ->with('seiyuu')
            ->paginate(10);

        $roles->transform(function (Role $role) {
            $role->animes = Anime::whereIn('id', explode(',', $role->anime_ids))->get();

            unset($role->anime_ids);

            return $role;
        });

        return view('character.index', compact('character', 'roles'));
    }

    /**
     * Get a list of a given user's characters
     * If no user is given, then characters of logged in user are retrieved.
     *
     * @param User $user
     * @param Request $request
     * @return Factory|View
     */
    public function top(User $user, Request $request)
    {
        if (!$user->id) {
            if (!Auth::user()) {
                return view('errors.no_permission');
            }

            $user = Auth::user();
        }

        $characterHandler = new CharacterHandler();

        $characterHandler->initQuery();

        $characterHandler->setUser($user);

        if (Auth::user()) {
            $characterHandler->leftJoinCurrentUserCharacter();
        }

        if ($user !== Auth::user()) {
            $characterHandler->leftJoinUserCharacter();
        }

        $characterHandler->filter($request);

        $characterHandler->sort($request);

        $characters = $characterHandler
            ->getCharacters()
            ->paginate(48);

        $filter = $characterHandler->getFilter();

        $sort = $characterHandler->getSort();

        return view('character.top', compact('user', 'characters', 'filter', 'sort'));
    }

    /**
     * Show page with form to add or update character
     * Unique key is MAL id so when nothing is filled in except MAL id, all fields are overwritten.
     *
     * @TODO: when filling in MAL id get current fields in database for this MAL id
     *
     * @return Factory|View
     */
    public function add()
    {
        return view('character.add');
    }
}
