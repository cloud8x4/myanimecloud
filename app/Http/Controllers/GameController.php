<?php

namespace App\Http\Controllers;

/**
 * Class GameController.
 */
class GameController extends Controller
{
    public function game()
    {
        return view('game.index');
    }
}
