<?php

namespace App\Presenters;

use Laracasts\Presenter\Presenter;

/**
 * Class User.
 */
class User extends Presenter
{
    /**
     * Present a link to the MyAnimeList website.
     *
     * @return string
     */
    public function linkMAL()
    {
        if ($this->malUser) {
            return 'https://myanimelist.net/profile/' . $this->name;
        }

        return trans('general.unknown');
    }

    /**
     * Present joined date.
     *
     * @return string
     */
    public function joined()
    {
        return $this->created_at->formatLocalized(trans('carbon.monthdatecommayear'));
    }

    /**
     * Present a link to this user's character top.
     *
     * @return string
     */
    public function linkCharacterTop()
    {
        return action('CharacterController@top', [$this->id]);
    }

    /**
     * Present a link to this user's seiyuu top.
     *
     * @return string
     */
    public function linkSeiyuuTop()
    {
        return action('SeiyuuController@top', [$this->id]);
    }
}
