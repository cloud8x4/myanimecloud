<?php

namespace App\Presenters;

use App\Models\Handlers\CarbonHandler;
use Auth;
use Laracasts\Presenter\Presenter;

class Anime extends Presenter
{
    /**
     * Present the number of episodes
     * if unknown return empty string
     * else return a nice format.
     *
     * @return string
     */
    public function presentEpisodes()
    {
        if (!is_null($this->episodes)) {
            return '(' . $this->episodes . ' eps)';
        }

        return '';
    }

    /**
     * Present start date.
     *
     * @return string
     */
    public function presentStartDate()
    {
        if (is_null($this->start_date)) {
            return '?';
        }

        return date('M', $this->start_date->month) . ' ' .
            $this->start_date->formatLocalized(trans('carbon.daycommayear'));
    }

    /**
     * Present end date.
     *
     * @return string
     */
    public function presentEndDate()
    {
        if (is_null($this->end_date)) {
            if ($this->status == 'finished airing') {
                return $this->presentStartData;
            }

            return '?';
        }

        return date('M', $this->end_date->month) . ' ' .
            $this->end_date->formatLocalized(trans('carbon.daycommayear'));
    }

    /**
     * Present period in which the anime aired.
     *
     * @return string
     */
    public function aired()
    {
        $aired = $start = $this->presentStartDate();
        $end = $this->presentStartDate();
        if ($start != $end) {
            $aired .= ' to ' . $end;
        }

        return trans('Aired') . ': ' . $aired;
    }

    /**
     * Present the season in which the anime started
     * if unknown return empty string
     * else return a nice format.
     *
     * @return string
     */
    public function seasonStarted()
    {
        $season = '';
        if (!is_null($this->start_date)) {
            $carbonHandler = new CarbonHandler();
            $carbonHandler->createFromCarbon($this->start_date);
            $season = $carbonHandler->animeSeason();
        }

        return $season;
    }

    /**
     * Present the members score
     * if unknown return empty string
     * else return a nice format.
     *
     * @return string
     */
    public function presentMembersScore()
    {
        if (!is_null($this->members_score)) {
            return 'Members score: ' . $this->members_score;
        }

        return '';
    }

    /**
     * Present a link to the anime page of the MyAnimeList website.
     *
     * @return string
     */
    public function linkMAL()
    {
        return 'https://myanimelist.net/anime/' . $this->MAL_id;
    }

    /**
     * Present a link to the anime characters page of the MyAnimeList website.
     *
     * @return string
     */
    public function linkMALCharacters()
    {
        return $this->linkMAL() . 'PLACEHOLDER/characters';
    }

    /**
     * Present an link to this website.
     *
     * @return string
     */
    public function link()
    {
        return '/anime/' . $this->id;
    }

    /**
     * Present a link to the public image tor this anime.
     *
     * @return string
     */
    public function publicImage()
    {
        return '/storage/anime/' . $this->image;
    }

    /**
     * Present the user's MAL score
     * if unknown return empty string
     * else return a nice format.
     *
     * @return string
     */
    public function myMALScore()
    {
        if (Auth::user() && Auth::user()->malUser && !is_null($this->myMalAnime) && $this->myMalAnime->score > 0) {
            return trans('My MAL score') . ': ' . $this->myMalAnime->score;
        }

        return '';
    }
}
