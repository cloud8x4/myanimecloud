<?php

namespace App\Presenters;

use Carbon\Carbon;
use Laracasts\Presenter\Presenter;

/**
 * Class Seiyuu.
 */
class Seiyuu extends Presenter
{
    /**
     * Present age
     * If unknown return an empty string
     * else return a nice format.
     *
     * @return string
     */
    public function age()
    {
        if (is_null($this->birthday)) {
            return '';
        } else {
            return 'Age: ' . $this->birthday->diff(Carbon::now())
                    ->format('%y');
        }
    }

    /**
     * Present birthday
     * If unknown return an empty string
     * else return a nice format.
     *
     * @return string
     */
    public function birthdayDateLocalized()
    {
        if (is_null($this->birthday)) {
            return '';
        } else {
            return 'Born: ' . $this->birthday->formatLocalized(trans('carbon.monthdatecommayear'));
        }
    }

    /**
     * Present favorite count
     * If unknown return an empty string
     * else return a nice format.
     *
     * @return string
     */
    public function presentFavoritedCount()
    {
        if (is_null($this->favorited_count)) {
            return '';
        } else {
            return 'Favorites: ' . $this->favorited_count;
        }
    }

    /**
     * Present current user rank
     * If unknown return ?
     *
     * @return string
     */
    public function myRank()
    {
        $currentUser = $this->currentUser->first();

        return $currentUser ? $currentUser->pivot->rank : '?';
    }

    /**
     * Present a user rank
     * If unknown return ?
     *
     * @return string
     */
    public function userRank()
    {
        $user = $this->users->first();

        return $user ? $user->pivot->rank : '?';
    }

    /**
     * Present score based on character ranks
     * If unknown return ?
     *
     * @return string
     */
    public function scoreByCharacterRanks()
    {
        $currentUser = $this->currentUser->first();

        return $currentUser ? $currentUser->pivot->score_by_character_ranks : '?';
    }

    /**
     * Present a link to the MyAnimeList website.
     *
     * @return string
     */
    public function linkMAL()
    {
        return 'https://myanimelist.net/people/' . $this->MAL_id;
    }

    /**
     * Present an link to this website.
     *
     * @return string
     */
    public function link()
    {
        return '/seiyuu/' . $this->id;
    }

    /**
     * Present a link to the public image.
     * @return string
     */
    public function publicImage()
    {
        return '/storage/seiyuu/' . $this->image;
    }
}
