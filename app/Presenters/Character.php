<?php

namespace App\Presenters;

use Laracasts\Presenter\Presenter;

/**
 * Class Character.
 */
class Character extends Presenter
{
    /**
     * Present current user rank
     * If unknown return ?
     *
     * @return string
     */
    public function myRank()
    {
        $currentUser = $this->currentUser->first();

        return $currentUser ? $currentUser->pivot->rank : '?';
    }

    /**
     * Present an user rank
     * If unknown return ?
     *
     * @return string
     */
    public function userRank()
    {
        $user = $this->users->first();

        return $user ? $user->pivot->rank : '?';
    }

    /**
     * Present a link to the MyAnimeList website.
     *
     * @return string
     */
    public function linkMAL()
    {
        return 'https://myanimelist.net/character/' . $this->MAL_id;
    }

    /**
     * Present an link to this website.
     *
     * @return string
     */
    public function link()
    {
        return '/character/' . $this->id;
    }

    /**
     * Present a link to the public image.
     * @return string
     */
    public function publicImage()
    {
        return '/storage/character/' . $this->image;
    }
}
