describe('Anime index', () => {
    before(() => {
        cy.refreshDatabase()
            .create('App\\Models\\Role')
            .create('App\\Models\\Role', {
                'anime_id': 1,
            })
    })

    context('Authorized user', () => {
        before(() => {
            cy.create('App\\Models\\User', {
                name: 'Cypress',
            })
        })

        beforeEach(() => {
            cy.login({ name: 'Cypress' })
                .visit('/anime/1')
        })

        it('can rank a character', () => {
            cy.get('#character-1')
                .children()
                .contains('My rank: ?')
                .click()
                .type('{enter}')
                .should('have.text', 'My rank: 1')
        })

        it('can rank an second character', () => {
            cy.get('#character-2')
                .children()
                .contains('My rank: ?')
                .click()
                .type('{enter}')
                .should('have.text', 'My rank: 2')
        })

        it('can change the rank of a character', () => {
            cy.get('#character-1')
                .children()
                .contains('My rank: 1')
                .click()
                .type('2{enter}')
                .should('have.text', 'My rank: 2')
        })

        it('can unrank a character', () => {
            cy.get('#character-1')
                .children()
                .contains('My rank: 2')
                .click()
                .type('{enter}')
                .should('have.text', 'My rank: ?')
        })

        it('can rank a seiyuu', () => {
            cy.get('#seiyuu-2')
                .children()
                .contains('My rank: ?')
                .click()
                .type('{enter}')
                .should('have.text', 'My rank: 1')
        })

        it('can rank an second seiyuu', () => {
            cy.get('#seiyuu-3')
                .children()
                .contains('My rank: ?')
                .click()
                .type('{enter}')
                .should('have.text', 'My rank: 2')
        })

        it('can change the rank of a seiyuu', () => {
            cy.get('#seiyuu-2')
                .children()
                .contains('My rank: 1')
                .click()
                .type('2{enter}')
                .should('have.text', 'My rank: 2')
        })

        it('can unrank a seiyuu', () => {
            cy.get('#seiyuu-2')
                .children()
                .contains('My rank: 2')
                .click()
                .type('{enter}')
                .should('have.text', 'My rank: ?')
        })

        it('can update the weight of a role', () => {
            cy.get('#character-1')
                .children()
                .contains('Weight: 100 %')
                .click()
                .type('10{enter}')
                .should('have.text', 'Weight: 10 %')
        })
    })

    context('Guest', () => {
        beforeEach(() => {
            cy.visit('/anime/1')
        })

        it('cannot rank a character', () => {
            cy.get('#character-1')
                .children()
                .should('not.contain', 'Rank: ?')
        })

        it('cannot rank a character', () => {
            cy.get('#character-1')
                .children()
                .should('not.contain', 'Rank: ?')
        })

        it('cannot update the weight of a role', () => {
            cy.get('#character-1')
                .children()
                .should('not.contain', 'Weight: 100 %')
        })
    })
})
