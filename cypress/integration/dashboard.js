describe('Dashboard', () => {
    before(() => {
        cy.refreshDatabase()
    })

    context('Authorized user who has not imported animes', () => {
        beforeEach(() => {
            cy.login({ name: 'Cypress' })
                .server()
                .route('/api/user/last-mal-import', {
                    id: 1,
                    last_updated_mal_anime: null,
                })
                .visit('/dashboard')
        })

        it('can see suggestions', () => {
            cy.get('h1')
                .contains('Suggestions')
        })

        it('can see posts', () => {
            cy.get('h1')
                .contains('Posts')
        })
    })

    context('Authorized user who has imported animes', () => {
        beforeEach(() => {
            cy.login({ name: 'Cypress2' })
                .server()
                .route('/api/user/last-mal-import', {
                    id: 1,
                    last_updated_mal_anime: {
                        updated_at: '2021-01-01'
                    },
                })
                .visit('/dashboard')
        })

        it('can see suggestions', () => {
            cy.get('h1')
                .contains('Suggestions')
        })

        it('can see posts', () => {
            cy.get('h1')
                .contains('Posts')
        })
    })

    context('Guest', () => {
        beforeEach(() => {
            cy.server()
                .route('/api/user/last-mal-import')
                .visit('/dashboard')
        })

        it('can see posts', () => {
            cy.get('h1')
                .contains('Posts')
        })
    })
})
