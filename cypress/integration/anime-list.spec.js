describe('Anime list', () => {
    before(() => {
        cy.refreshDatabase()
            .create('App\\Models\\Role', 2)
    })

    context('Authorized user', () => {
        before(() => {
            cy.create('App\\Models\\User', {
                name: 'Cypress',
            })
        })

        beforeEach(() => {
            cy.login({ name: 'Cypress' })
                .visit('/anime')
        })

        it('can set character gender', () => {
            cy.get('#filterAnimesButton')
                .click()
                .get('#animeListVisibilityCharacterGender')
                .first()
                .click()
                .get('.characterFemaleOrMaleSelect')
                .first()
                .click()

            cy.visit('/anime')
                .get('.characterFemaleOrMaleSelect')
                .first()
                .should('have.class', 'active')
        })
    })
})
