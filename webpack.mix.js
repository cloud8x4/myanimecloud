const mix = require('laravel-mix')

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application, as well as bundling up your JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')

mix
    .scripts([
        'resources/js/anime.js',
        'resources/js/seiyuu.js',
        'resources/js/character.js',
        'resources/js/role.js',
        'resources/js/user.js'
    ], 'public/js/myanimecloud.js')

    .styles([
        'resources/css/myanimecloud.css'
    ], 'public/css/myanimecloud.css')

    /* Copy images */
    .copy('resources/img/MAL.png', 'public/storage')
    .copy('resources/img/unknown.gif', 'public/storage/seiyuu')
