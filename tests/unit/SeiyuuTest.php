<?php

namespace Tests\unit;

use App\Models\Seiyuu;
use App\Models\User;
use Faker\Factory as Faker;
use Tests\BrowserKitTestCase;

class SeiyuuTest extends BrowserKitTestCase
{
    protected $user;

    protected $faker;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create();
        $this->actingAs($this->user);

        $this->faker = Faker::create();
    }

    /**********************************************************************
     * Ranking when only 1 user exists
     **********************************************************************/

    /**
     * @test
     */
    public function a_rank_is_not_set_if_a_negative_rank_or_rank_0_is_requested()
    {
        $seiyuu = Seiyuu::factory()->create();

        $seiyuu->setUserRank($this->user, 0);
        $this->assertTrue($this->user->seiyuus->isEmpty());

        $seiyuu->setUserRank($this->user, -1);
        $this->assertTrue($this->user->seiyuus->isEmpty());
    }

    /**
     * @test
     */
    public function a_rank_is_not_set_if_a_string_is_requested()
    {
        $seiyuu = Seiyuu::factory()->create();
        $seiyuu->setUserRank($this->user, $this->faker->word);

        $this->assertTrue($this->user->seiyuus->isEmpty());
    }

    /**
     * @test
     */
    public function the_worst_rank_is_given_is_for_an_unranked_seiyuu_if_a_rank_worse_than_the_worst_rank_or_an_empty_rank_is_requested()
    {
        $seiyuus = Seiyuu::factory()->count(5)->create();

        $seiyuus[0]->setUserRank($this->user, 1);
        $this->assertEquals(1, $this->user->seiyuuRank($seiyuus[0]));

        $seiyuus[1]->setUserRank($this->user, 2);
        $this->assertEquals(2, $this->user->seiyuuRank($seiyuus[1]));

        $seiyuus[2]->setUserRank($this->user, $this->faker->numberBetween(4, 100));
        $this->assertEquals(3, $this->user->seiyuuRank($seiyuus[2]));

        $seiyuus[3]->setUserRank($this->user, '');
        $this->assertEquals(4, $this->user->seiyuuRank($seiyuus[3]));

        $seiyuus[4]->setUserRank($this->user);
        $this->assertEquals(5, $this->user->seiyuuRank($seiyuus[4]));
    }

    /**
     * @test
     */
    public function a_rank_can_be_removed()
    {
        $seiyuu = Seiyuu::factory()->create();

        $seiyuu->setUserRank($this->user, 1);
        $this->assertEquals(1, $this->user->seiyuuRank($seiyuu));

        $seiyuu->setUserRank($this->user);
        $this->assertTrue($this->user->seiyuus()->get()->isEmpty());
    }

    /**
     * @test
     */
    public function a_rank_is_not_changed_if_the_same_rank_is_requested()
    {
        $seiyuus = Seiyuu::factory()->count(2)->create();

        $seiyuus->each(function ($seiyuu, $index) {
            $seiyuu->setUserRank($this->user, $index + 1);
            $rank = $this->user->seiyuuRank($seiyuu);

            $seiyuu->setUserRank($this->user, $index + 1);
            $this->assertEquals($rank, $this->user->seiyuuRank($seiyuu));
        });
    }

    /**
     * @test
     */
    public function the_worst_rank_is_set_if_a_rank_worse_than_the_worst_rank_is_requested()
    {
        $seiyuus = Seiyuu::factory()->count(2)->create();

        $seiyuus[0]->setUserRank($this->user, 1);
        $this->assertEquals(1, $this->user->seiyuuRank($seiyuus[0]));

        $seiyuus[1]->setUserRank($this->user, 2);
        $this->assertEquals(2, $this->user->seiyuuRank($seiyuus[1]));

        $seiyuus[1]->setUserRank($this->user, 3);
        $this->assertEquals(2, $this->user->seiyuuRank($seiyuus[1]));

        $seiyuus[1]->setUserRank($this->user, $this->faker->numberBetween(4, 100));
        $this->assertEquals(2, $this->user->seiyuuRank($seiyuus[1]));

        $seiyuus[0]->setUserRank($this->user, 3);
        $this->assertEquals(2, $this->user->seiyuuRank($seiyuus[0]));

        $seiyuus[1]->setUserRank($this->user, $this->faker->numberBetween(4, 100));
        $this->assertEquals(2, $this->user->seiyuuRank($seiyuus[1]));
    }

    /**
     * @test
     */
    public function an_existing_rank_is_set()
    {
        $seiyuus = Seiyuu::factory()->count(2)->create();

        $seiyuus[0]->setUserRank($this->user, 1);

        $seiyuus[1]->setUserRank($this->user, 1);
        $this->assertEquals(1, $this->user->seiyuuRank($seiyuus[1]));

        $seiyuus[1]->setUserRank($this->user, 2);
        $this->assertEquals(2, $this->user->seiyuuRank($seiyuus[1]));
    }

    /**
     * @test
     */
    public function seiyuu_ranks_drop_1_rank_if_an_unranked_seiyuu_is_ranked_in_their_range()
    {
        $seiyuus = Seiyuu::factory()->count(5)->create();

        $seiyuus[0]->setUserRank($this->user, 1);

        $seiyuus[1]->setUserRank($this->user, 1);
        $this->assertEquals(2, $this->user->seiyuuRank($seiyuus[0]));

        $seiyuus[2]->setUserRank($this->user, 1);
        $this->assertEquals(3, $this->user->seiyuuRank($seiyuus[0]));
        $this->assertEquals(2, $this->user->seiyuuRank($seiyuus[1]));

        $seiyuus[3]->setUserRank($this->user, 2);
        $this->assertEquals(4, $this->user->seiyuuRank($seiyuus[0]));
        $this->assertEquals(3, $this->user->seiyuuRank($seiyuus[1]));
        $this->assertEquals(1, $this->user->seiyuuRank($seiyuus[2]));
    }

    /**
     * @test
     */
    public function seiyuu_ranks_rise_1_rank_if_a_ranked_seiyuu_is_ranked_worse_than_worst_rank()
    {
        $seiyuus = Seiyuu::factory()->count(5)->create();
        $seiyuus[0]->setUserRank($this->user, 1);
        $seiyuus[1]->setUserRank($this->user, 2);
        $seiyuus[2]->setUserRank($this->user, 3);
        $seiyuus[3]->setUserRank($this->user, 4);
        $seiyuus[4]->setUserRank($this->user, 5);

        $seiyuus[3]->setUserRank($this->user, 6);                                 // 4 -> 3
        $this->assertEquals(4, $this->user->seiyuuRank($seiyuus[4])); // 5 -> 4
        $this->assertEquals(3, $this->user->seiyuuRank($seiyuus[2])); // 3 -> 3

        $seiyuus[2]->setUserRank($this->user, 6);                                 // 3 -> 5
        $this->assertEquals(4, $this->user->seiyuuRank($seiyuus[3])); // 5 -> 4
        $this->assertEquals(3, $this->user->seiyuuRank($seiyuus[4])); // 4 -> 3
        $this->assertEquals(2, $this->user->seiyuuRank($seiyuus[1])); // 2 -> 2

        $seiyuus[0]->setUserRank($this->user, 6);                                 // 1 -> 5
        $this->assertEquals(1, $this->user->seiyuuRank($seiyuus[1])); // 2 -> 1
        $this->assertEquals(4, $this->user->seiyuuRank($seiyuus[2])); // 5 -> 4
        $this->assertEquals(3, $this->user->seiyuuRank($seiyuus[3])); // 4 -> 3
        $this->assertEquals(2, $this->user->seiyuuRank($seiyuus[4])); // 3 -> 2
    }

    /**
     * @test
     */
    public function seiyuu_ranks_rise_1_rank_if_a_ranked_seiyuu_is_unranked()
    {
        $seiyuus = Seiyuu::factory()->count(5)->create();
        $seiyuus[0]->setUserRank($this->user, 1);
        $seiyuus[1]->setUserRank($this->user, 2);
        $seiyuus[2]->setUserRank($this->user, 3);
        $seiyuus[3]->setUserRank($this->user, 4);
        $seiyuus[4]->setUserRank($this->user, 5);

        $seiyuus[3]->setUserRank($this->user);                                    // 4 -> unranked
        $this->assertEquals(4, $this->user->seiyuuRank($seiyuus[4])); // 5 -> 4
        $this->assertEquals(3, $this->user->seiyuuRank($seiyuus[2])); // 3 -> 3

        $seiyuus[1]->setUserRank($this->user);                                    // 2 -> unranked
        $this->assertEquals(3, $this->user->seiyuuRank($seiyuus[4])); // 4 -> 3
        $this->assertEquals(2, $this->user->seiyuuRank($seiyuus[2])); // 3 -> 2
        $this->assertEquals(1, $this->user->seiyuuRank($seiyuus[0])); // 1 -> 1

        $seiyuus[0]->setUserRank($this->user);                                    // 1 -> unranked
        $this->assertEquals(2, $this->user->seiyuuRank($seiyuus[4])); // 3 -> 2
        $this->assertEquals(1, $this->user->seiyuuRank($seiyuus[2])); // 2 -> 1
    }

    /**
     * @test
     */
    public function seiyuu_ranks_drop_1_rank_if_a_ranked_seiyuu_is_ranked_in_their_range()
    {
        $seiyuus = Seiyuu::factory()->count(5)->create();
        $seiyuus[0]->setUserRank($this->user, 1);
        $seiyuus[1]->setUserRank($this->user, 2);
        $seiyuus[2]->setUserRank($this->user, 3);
        $seiyuus[3]->setUserRank($this->user, 4);
        $seiyuus[4]->setUserRank($this->user, 5);

        $seiyuus[4]->setUserRank($this->user, 4);                                 // 5 -> 4
        $this->assertEquals(5, $this->user->seiyuuRank($seiyuus[3])); // 4 -> 5
        $this->assertEquals(3, $this->user->seiyuuRank($seiyuus[2])); // 3 -> 3

        $seiyuus[3]->setUserRank($this->user, 3);                                 // 5 -> 3
        $this->assertEquals(4, $this->user->seiyuuRank($seiyuus[2])); // 3 -> 4
        $this->assertEquals(5, $this->user->seiyuuRank($seiyuus[4])); // 4 -> 5
        $this->assertEquals(2, $this->user->seiyuuRank($seiyuus[1])); // 2 -> 2

        $seiyuus[4]->setUserRank($this->user, 1);                                 // 5 -> 1
        $this->assertEquals(2, $this->user->seiyuuRank($seiyuus[0])); // 1 -> 2
        $this->assertEquals(3, $this->user->seiyuuRank($seiyuus[1])); // 2 -> 3
        $this->assertEquals(4, $this->user->seiyuuRank($seiyuus[3])); // 3 -> 4
        $this->assertEquals(5, $this->user->seiyuuRank($seiyuus[2])); // 4 -> 5

        $seiyuus[3]->setUserRank($this->user, 3);                                 // 4 -> 3
        $this->assertEquals(4, $this->user->seiyuuRank($seiyuus[1])); // 3 -> 4
        $this->assertEquals(5, $this->user->seiyuuRank($seiyuus[2])); // 5 -> 5

        $seiyuus[1]->setUserRank($this->user, 2);                                 // 4 -> 2
        $this->assertEquals(3, $this->user->seiyuuRank($seiyuus[0])); // 2 -> 3
        $this->assertEquals(4, $this->user->seiyuuRank($seiyuus[3])); // 3 -> 4
        $this->assertEquals(5, $this->user->seiyuuRank($seiyuus[2])); // 5 -> 5

        $seiyuus[3]->setUserRank($this->user, 1);                                 // 4 -> 1
        $this->assertEquals(2, $this->user->seiyuuRank($seiyuus[4])); // 1 -> 2
        $this->assertEquals(3, $this->user->seiyuuRank($seiyuus[1])); // 2 -> 3
        $this->assertEquals(4, $this->user->seiyuuRank($seiyuus[0])); // 3 -> 4
        $this->assertEquals(5, $this->user->seiyuuRank($seiyuus[2])); // 5 -> 5

        $seiyuus[1]->setUserRank($this->user, 2);                                 // 3 -> 2
        $this->assertEquals(3, $this->user->seiyuuRank($seiyuus[4])); // 2 -> 3
        $this->assertEquals(4, $this->user->seiyuuRank($seiyuus[0])); // 4 -> 4

        $seiyuus[4]->setUserRank($this->user, 1);                                 // 3 -> 5
        $this->assertEquals(2, $this->user->seiyuuRank($seiyuus[3])); // 1 -> 2
        $this->assertEquals(3, $this->user->seiyuuRank($seiyuus[1])); // 2 -> 3
        $this->assertEquals(4, $this->user->seiyuuRank($seiyuus[0])); // 4 -> 4

        $seiyuus[3]->setUserRank($this->user, 1);                                 // 2 -> 1
        $this->assertEquals(2, $this->user->seiyuuRank($seiyuus[4])); // 1 -> 2
        $this->assertEquals(3, $this->user->seiyuuRank($seiyuus[1])); // 3 -> 3
    }

    /**
     * @test
     */
    public function seiyuu_ranks_rise_1_rank_if_a_ranked_seiyuu_is_ranked_in_their_range()
    {
        $seiyuus = Seiyuu::factory(0)->count(5)->create();
        $seiyuus[0]->setUserRank($this->user, 1);
        $seiyuus[1]->setUserRank($this->user, 2);
        $seiyuus[2]->setUserRank($this->user, 3);
        $seiyuus[3]->setUserRank($this->user, 4);
        $seiyuus[4]->setUserRank($this->user, 5);

        $seiyuus[0]->setUserRank($this->user, 2);                                 // 1 -> 2
        $this->assertEquals(1, $this->user->seiyuuRank($seiyuus[1])); // 2 -> 1
        $this->assertEquals(3, $this->user->seiyuuRank($seiyuus[2])); // 3 -> 3

        $seiyuus[1]->setUserRank($this->user, 3);                                 // 1 -> 3
        $this->assertEquals(2, $this->user->seiyuuRank($seiyuus[2])); // 3 -> 2
        $this->assertEquals(1, $this->user->seiyuuRank($seiyuus[0])); // 2 -> 1
        $this->assertEquals(4, $this->user->seiyuuRank($seiyuus[3])); // 4 -> 4

        $seiyuus[0]->setUserRank($this->user, 5);                                 // 1 -> 5
        $this->assertEquals(4, $this->user->seiyuuRank($seiyuus[4])); // 5 -> 4
        $this->assertEquals(3, $this->user->seiyuuRank($seiyuus[3])); // 4 -> 3
        $this->assertEquals(2, $this->user->seiyuuRank($seiyuus[1])); // 3 -> 2
        $this->assertEquals(1, $this->user->seiyuuRank($seiyuus[2])); // 2 -> 1

        $seiyuus[1]->setUserRank($this->user, 3);                                 // 2 -> 3
        $this->assertEquals(2, $this->user->seiyuuRank($seiyuus[3])); // 3 -> 2
        $this->assertEquals(1, $this->user->seiyuuRank($seiyuus[2])); // 1 -> 1

        $seiyuus[3]->setUserRank($this->user, 4);                                 // 2 -> 4
        $this->assertEquals(3, $this->user->seiyuuRank($seiyuus[4])); // 4 -> 3
        $this->assertEquals(2, $this->user->seiyuuRank($seiyuus[1])); // 3 -> 2
        $this->assertEquals(1, $this->user->seiyuuRank($seiyuus[2])); // 1 -> 1

        $seiyuus[1]->setUserRank($this->user, 5);                                 // 2 -> 5
        $this->assertEquals(4, $this->user->seiyuuRank($seiyuus[0])); // 5 -> 4
        $this->assertEquals(3, $this->user->seiyuuRank($seiyuus[3])); // 4 -> 3
        $this->assertEquals(2, $this->user->seiyuuRank($seiyuus[4])); // 3 -> 2
        $this->assertEquals(1, $this->user->seiyuuRank($seiyuus[2])); // 1 -> 1

        $seiyuus[3]->setUserRank($this->user, 4);                                 // 3 -> 4
        $this->assertEquals(3, $this->user->seiyuuRank($seiyuus[0])); // 4 -> 3
        $this->assertEquals(2, $this->user->seiyuuRank($seiyuus[4])); // 2 -> 2

        $seiyuus[0]->setUserRank($this->user, 5);                                 // 3 -> 5
        $this->assertEquals(4, $this->user->seiyuuRank($seiyuus[1])); // 5 -> 4
        $this->assertEquals(3, $this->user->seiyuuRank($seiyuus[3])); // 4 -> 3
        $this->assertEquals(2, $this->user->seiyuuRank($seiyuus[4])); // 2 -> 2

        $seiyuus[1]->setUserRank($this->user, 5);                                 // 4 -> 5
        $this->assertEquals(4, $this->user->seiyuuRank($seiyuus[0])); // 5 -> 4
        $this->assertEquals(3, $this->user->seiyuuRank($seiyuus[3])); // 3 -> 3
    }

    /**
     * @test
     */
    public function ranks_are_not_changed_if_a_negative_rank_or_rank_0_is_requested()
    {
        $seiyuus = Seiyuu::factory()->count(3)->create();
        $seiyuus[0]->setUserRank($this->user, 1);
        $seiyuus[1]->setUserRank($this->user, 2);
        $seiyuus[2]->setUserRank($this->user, 3);

        $seiyuus[0]->setUserRank($this->user, 0);
        $this->assertEquals(1, $this->user->seiyuuRank($seiyuus[0]));
        $this->assertEquals(2, $this->user->seiyuuRank($seiyuus[1]));
        $this->assertEquals(3, $this->user->seiyuuRank($seiyuus[2]));

        $seiyuus[0]->setUserRank($this->user, -1);
        $this->assertEquals(1, $this->user->seiyuuRank($seiyuus[0]));
        $this->assertEquals(2, $this->user->seiyuuRank($seiyuus[1]));
        $this->assertEquals(3, $this->user->seiyuuRank($seiyuus[2]));

        $seiyuus[1]->setUserRank($this->user, 0);
        $this->assertEquals(1, $this->user->seiyuuRank($seiyuus[0]));
        $this->assertEquals(2, $this->user->seiyuuRank($seiyuus[1]));
        $this->assertEquals(3, $this->user->seiyuuRank($seiyuus[2]));

        $seiyuus[1]->setUserRank($this->user, -1);
        $this->assertEquals(1, $this->user->seiyuuRank($seiyuus[0]));
        $this->assertEquals(2, $this->user->seiyuuRank($seiyuus[1]));
        $this->assertEquals(3, $this->user->seiyuuRank($seiyuus[2]));

        $seiyuus[2]->setUserRank($this->user, 0);
        $this->assertEquals(1, $this->user->seiyuuRank($seiyuus[0]));
        $this->assertEquals(2, $this->user->seiyuuRank($seiyuus[1]));
        $this->assertEquals(3, $this->user->seiyuuRank($seiyuus[2]));

        $seiyuus[2]->setUserRank($this->user, -1);
        $this->assertEquals(1, $this->user->seiyuuRank($seiyuus[0]));
        $this->assertEquals(2, $this->user->seiyuuRank($seiyuus[1]));
        $this->assertEquals(3, $this->user->seiyuuRank($seiyuus[2]));
    }

    /**********************************************************************
     * Ranking when 2 users exist
     **********************************************************************/

    /**
     * @test
     */
    public function ranks_of_another_user_are_not_changed_when_1_user_changes_a_rank()
    {
        $user2 = User::factory()->create();
        $seiyuus = Seiyuu::factory()->count(3)->create();

        $seiyuus[0]->setUserRank($user2, 1);
        $seiyuus[1]->setUserRank($user2, 2);
        $seiyuus[2]->setUserRank($user2, 3);

        $seiyuus[0]->setUserRank($this->user, 1);
        $this->assertEquals(1, $user2->seiyuuRank($seiyuus[0]));
        $this->assertEquals(2, $user2->seiyuuRank($seiyuus[1]));
        $this->assertEquals(3, $user2->seiyuuRank($seiyuus[2]));

        $seiyuus[1]->setUserRank($this->user, 2);
        $this->assertEquals(1, $user2->seiyuuRank($seiyuus[0]));
        $this->assertEquals(2, $user2->seiyuuRank($seiyuus[1]));
        $this->assertEquals(3, $user2->seiyuuRank($seiyuus[2]));

        $seiyuus[1]->setUserRank($this->user, 1);
        $this->assertEquals(1, $user2->seiyuuRank($seiyuus[0]));
        $this->assertEquals(2, $user2->seiyuuRank($seiyuus[1]));
        $this->assertEquals(3, $user2->seiyuuRank($seiyuus[2]));

        $seiyuus[2]->setUserRank($this->user, 1);
        $this->assertEquals(1, $user2->seiyuuRank($seiyuus[0]));
        $this->assertEquals(2, $user2->seiyuuRank($seiyuus[1]));
        $this->assertEquals(3, $user2->seiyuuRank($seiyuus[2]));

        $seiyuus[2]->setUserRank($this->user, 2);
        $this->assertEquals(1, $user2->seiyuuRank($seiyuus[0]));
        $this->assertEquals(2, $user2->seiyuuRank($seiyuus[1]));
        $this->assertEquals(3, $user2->seiyuuRank($seiyuus[2]));

        $seiyuus[2]->setUserRank($this->user, 3);
        $this->assertEquals(1, $user2->seiyuuRank($seiyuus[0]));
        $this->assertEquals(2, $user2->seiyuuRank($seiyuus[1]));
        $this->assertEquals(3, $user2->seiyuuRank($seiyuus[2]));

        $seiyuus[1]->setUserRank($this->user);                              // Unrank
        $this->assertEquals(1, $user2->seiyuuRank($seiyuus[0]));
        $this->assertEquals(2, $user2->seiyuuRank($seiyuus[1]));
        $this->assertEquals(3, $user2->seiyuuRank($seiyuus[2]));
    }
}
