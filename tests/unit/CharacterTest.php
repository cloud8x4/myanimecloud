<?php

namespace Tests\unit;

use App\Models\Character;
use App\Models\User;
use Faker\Factory as Faker;
use Tests\BrowserKitTestCase;

class CharacterTest extends BrowserKitTestCase
{
    protected $user;

    protected $faker;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create();

        $this->actingAs($this->user);

        $this->faker = Faker::create();
    }

    /**********************************************************************
     * Ranking
     **********************************************************************/

    /**
     * @test
     */
    public function a_rank_is_not_set_if_a_negative_rank_or_rank_0_is_requested()
    {
        $character = Character::factory()->create();

        $character->setUserRank($this->user, 0);
        $this->assertTrue($this->user->characters->isEmpty());

        $character->setUserRank($this->user, -1);
        $this->assertTrue($this->user->characters->isEmpty());
    }

    /**
     * @test
     */
    public function a_rank_is_not_set_if_a_string_is_requested()
    {
        $character = Character::factory()->create();
        $character->setUserRank($this->user, $this->faker->word);

        $this->assertTrue($this->user->characters->isEmpty());
    }

    /**
     * @test
     */
    public function the_worst_rank_is_given_is_for_an_unranked_character_if_a_rank_worse_than_the_worst_rank_or_an_empty_rank_is_requested()
    {
        $characters = Character::factory()->count(5)->create();

        $characters[0]->setUserRank($this->user, 1);
        $this->assertEquals(1, $this->user->characterRank($characters[0]));

        $characters[1]->setUserRank($this->user, 2);
        $this->assertEquals(2, $this->user->characterRank($characters[1]));

        $characters[2]->setUserRank($this->user, $this->faker->numberBetween(4, 100));
        $this->assertEquals(3, $this->user->characterRank($characters[2]));

        $characters[3]->setUserRank($this->user, '');
        $this->assertEquals(4, $this->user->characterRank($characters[3]));

        $characters[4]->setUserRank($this->user);
        $this->assertEquals(5, $this->user->characterRank($characters[4]));
    }

    /**
     * @test
     */
    public function a_rank_can_be_removed()
    {
        $character = Character::factory()->create();

        $character->setUserRank($this->user, 1);
        $this->assertEquals(1, $this->user->characterRank($character));

        $character->setUserRank($this->user);
        $this->assertTrue($this->user->characters()->get()->isEmpty());
    }

    /**
     * @test
     */
    public function a_rank_is_not_changed_if_the_same_rank_is_requested()
    {
        $characters = Character::factory()->count(2)->create();

        $characters->each(function ($character, $index) {
            $character->setUserRank($this->user, $index + 1);
            $rank = $this->user->characterRank($character);

            $character->setUserRank($this->user, $index + 1);
            $this->assertEquals($rank, $this->user->characterRank($character));
        });
    }

    /**
     * @test
     */
    public function the_worst_rank_is_set_if_a_rank_worse_than_the_worst_rank_is_requested()
    {
        $characters = Character::factory()->count(2)->create();

        $characters[0]->setUserRank($this->user, 1);
        $this->assertEquals(1, $this->user->characterRank($characters[0]));

        $characters[1]->setUserRank($this->user, 2);
        $this->assertEquals(2, $this->user->characterRank($characters[1]));

        $characters[1]->setUserRank($this->user, 3);
        $this->assertEquals(2, $this->user->characterRank($characters[1]));

        $characters[1]->setUserRank($this->user, $this->faker->numberBetween(4, 100));
        $this->assertEquals(2, $this->user->characterRank($characters[1]));

        $characters[0]->setUserRank($this->user, 3);
        $this->assertEquals(2, $this->user->characterRank($characters[0]));

        $characters[1]->setUserRank($this->user, $this->faker->numberBetween(4, 100));
        $this->assertEquals(2, $this->user->characterRank($characters[1]));
    }

    /**
     * @test
     */
    public function an_existing_rank_is_set()
    {
        $characters = Character::factory()->count(2)->create();

        $characters[0]->setUserRank($this->user, 1);

        $characters[1]->setUserRank($this->user, 1);
        $this->assertEquals(1, $this->user->characterRank($characters[1]));

        $characters[1]->setUserRank($this->user, 2);
        $this->assertEquals(2, $this->user->characterRank($characters[1]));
    }

    /**
     * @test
     */
    public function character_ranks_drop_1_rank_if_an_unranked_character_is_ranked_in_their_range()
    {
        $characters = Character::factory()->count(5)->create();

        $characters[0]->setUserRank($this->user, 1);

        $characters[1]->setUserRank($this->user, 1);
        $this->assertEquals(2, $this->user->characterRank($characters[0]));

        $characters[2]->setUserRank($this->user, 1);
        $this->assertEquals(3, $this->user->characterRank($characters[0]));
        $this->assertEquals(2, $this->user->characterRank($characters[1]));

        $characters[3]->setUserRank($this->user, 2);
        $this->assertEquals(4, $this->user->characterRank($characters[0]));
        $this->assertEquals(3, $this->user->characterRank($characters[1]));
        $this->assertEquals(1, $this->user->characterRank($characters[2]));
    }

    /**
     * @test
     */
    public function character_ranks_rise_1_rank_if_a_ranked_character_is_ranked_worse_than_worst_rank()
    {
        $characters = Character::factory()->count(5)->create();
        $characters[0]->setUserRank($this->user, 1);
        $characters[1]->setUserRank($this->user, 2);
        $characters[2]->setUserRank($this->user, 3);
        $characters[3]->setUserRank($this->user, 4);
        $characters[4]->setUserRank($this->user, 5);

        $characters[3]->setUserRank($this->user, 6);                                 // 4 -> 3
        $this->assertEquals(4, $this->user->characterRank($characters[4])); // 5 -> 4
        $this->assertEquals(3, $this->user->characterRank($characters[2])); // 3 -> 3

        $characters[2]->setUserRank($this->user, 6);                                 // 3 -> 5
        $this->assertEquals(4, $this->user->characterRank($characters[3])); // 5 -> 4
        $this->assertEquals(3, $this->user->characterRank($characters[4])); // 4 -> 3
        $this->assertEquals(2, $this->user->characterRank($characters[1])); // 2 -> 2

        $characters[0]->setUserRank($this->user, 6);                                 // 1 -> 5
        $this->assertEquals(1, $this->user->characterRank($characters[1])); // 2 -> 1
        $this->assertEquals(4, $this->user->characterRank($characters[2])); // 5 -> 4
        $this->assertEquals(3, $this->user->characterRank($characters[3])); // 4 -> 3
        $this->assertEquals(2, $this->user->characterRank($characters[4])); // 3 -> 2
    }

    /**
     * @test
     */
    public function character_ranks_rise_1_rank_if_a_ranked_character_is_unranked()
    {
        $characters = Character::factory()->count(5)->create();
        $characters[0]->setUserRank($this->user, 1);
        $characters[1]->setUserRank($this->user, 2);
        $characters[2]->setUserRank($this->user, 3);
        $characters[3]->setUserRank($this->user, 4);
        $characters[4]->setUserRank($this->user, 5);

        $characters[3]->setUserRank($this->user);                                    // 4 -> unranked
        $this->assertEquals(4, $this->user->characterRank($characters[4])); // 5 -> 4
        $this->assertEquals(3, $this->user->characterRank($characters[2])); // 3 -> 3

        $characters[1]->setUserRank($this->user);                                    // 2 -> unranked
        $this->assertEquals(3, $this->user->characterRank($characters[4])); // 4 -> 3
        $this->assertEquals(2, $this->user->characterRank($characters[2])); // 3 -> 2
        $this->assertEquals(1, $this->user->characterRank($characters[0])); // 1 -> 1

        $characters[0]->setUserRank($this->user);                                    // 1 -> unranked
        $this->assertEquals(2, $this->user->characterRank($characters[4])); // 3 -> 2
        $this->assertEquals(1, $this->user->characterRank($characters[2])); // 2 -> 1
    }

    /**
     * @test
     */
    public function character_ranks_drop_1_rank_if_a_ranked_character_is_ranked_in_their_range()
    {
        $characters = Character::factory()->count(5)->create();
        $characters[0]->setUserRank($this->user, 1);
        $characters[1]->setUserRank($this->user, 2);
        $characters[2]->setUserRank($this->user, 3);
        $characters[3]->setUserRank($this->user, 4);
        $characters[4]->setUserRank($this->user, 5);

        $characters[4]->setUserRank($this->user, 4);                                 // 5 -> 4
        $this->assertEquals(5, $this->user->characterRank($characters[3])); // 4 -> 5
        $this->assertEquals(3, $this->user->characterRank($characters[2])); // 3 -> 3

        $characters[3]->setUserRank($this->user, 3);                                 // 5 -> 3
        $this->assertEquals(4, $this->user->characterRank($characters[2])); // 3 -> 4
        $this->assertEquals(5, $this->user->characterRank($characters[4])); // 4 -> 5
        $this->assertEquals(2, $this->user->characterRank($characters[1])); // 2 -> 2

        $characters[4]->setUserRank($this->user, 1);                                 // 5 -> 1
        $this->assertEquals(2, $this->user->characterRank($characters[0])); // 1 -> 2
        $this->assertEquals(3, $this->user->characterRank($characters[1])); // 2 -> 3
        $this->assertEquals(4, $this->user->characterRank($characters[3])); // 3 -> 4
        $this->assertEquals(5, $this->user->characterRank($characters[2])); // 4 -> 5

        $characters[3]->setUserRank($this->user, 3);                                 // 4 -> 3
        $this->assertEquals(4, $this->user->characterRank($characters[1])); // 3 -> 4
        $this->assertEquals(5, $this->user->characterRank($characters[2])); // 5 -> 5

        $characters[1]->setUserRank($this->user, 2);                                 // 4 -> 2
        $this->assertEquals(3, $this->user->characterRank($characters[0])); // 2 -> 3
        $this->assertEquals(4, $this->user->characterRank($characters[3])); // 3 -> 4
        $this->assertEquals(5, $this->user->characterRank($characters[2])); // 5 -> 5

        $characters[3]->setUserRank($this->user, 1);                                 // 4 -> 1
        $this->assertEquals(2, $this->user->characterRank($characters[4])); // 1 -> 2
        $this->assertEquals(3, $this->user->characterRank($characters[1])); // 2 -> 3
        $this->assertEquals(4, $this->user->characterRank($characters[0])); // 3 -> 4
        $this->assertEquals(5, $this->user->characterRank($characters[2])); // 5 -> 5

        $characters[1]->setUserRank($this->user, 2);                                 // 3 -> 2
        $this->assertEquals(3, $this->user->characterRank($characters[4])); // 2 -> 3
        $this->assertEquals(4, $this->user->characterRank($characters[0])); // 4 -> 4

        $characters[4]->setUserRank($this->user, 1);                                 // 3 -> 5
        $this->assertEquals(2, $this->user->characterRank($characters[3])); // 1 -> 2
        $this->assertEquals(3, $this->user->characterRank($characters[1])); // 2 -> 3
        $this->assertEquals(4, $this->user->characterRank($characters[0])); // 4 -> 4

        $characters[3]->setUserRank($this->user, 1);                                 // 2 -> 1
        $this->assertEquals(2, $this->user->characterRank($characters[4])); // 1 -> 2
        $this->assertEquals(3, $this->user->characterRank($characters[1])); // 3 -> 3
    }

    /**
     * @test
     */
    public function character_ranks_rise_1_rank_if_a_ranked_character_is_ranked_in_their_range()
    {
        $characters = Character::factory()->count(5)->create();
        $characters[0]->setUserRank($this->user, 1);
        $characters[1]->setUserRank($this->user, 2);
        $characters[2]->setUserRank($this->user, 3);
        $characters[3]->setUserRank($this->user, 4);
        $characters[4]->setUserRank($this->user, 5);

        $characters[0]->setUserRank($this->user, 2);                                 // 1 -> 2
        $this->assertEquals(1, $this->user->characterRank($characters[1])); // 2 -> 1
        $this->assertEquals(3, $this->user->characterRank($characters[2])); // 3 -> 3

        $characters[1]->setUserRank($this->user, 3);                                 // 1 -> 3
        $this->assertEquals(2, $this->user->characterRank($characters[2])); // 3 -> 2
        $this->assertEquals(1, $this->user->characterRank($characters[0])); // 2 -> 1
        $this->assertEquals(4, $this->user->characterRank($characters[3])); // 4 -> 4

        $characters[0]->setUserRank($this->user, 5);                                 // 1 -> 5
        $this->assertEquals(4, $this->user->characterRank($characters[4])); // 5 -> 4
        $this->assertEquals(3, $this->user->characterRank($characters[3])); // 4 -> 3
        $this->assertEquals(2, $this->user->characterRank($characters[1])); // 3 -> 2
        $this->assertEquals(1, $this->user->characterRank($characters[2])); // 2 -> 1

        $characters[1]->setUserRank($this->user, 3);                                 // 2 -> 3
        $this->assertEquals(2, $this->user->characterRank($characters[3])); // 3 -> 2
        $this->assertEquals(1, $this->user->characterRank($characters[2])); // 1 -> 1

        $characters[3]->setUserRank($this->user, 4);                                 // 2 -> 4
        $this->assertEquals(3, $this->user->characterRank($characters[4])); // 4 -> 3
        $this->assertEquals(2, $this->user->characterRank($characters[1])); // 3 -> 2
        $this->assertEquals(1, $this->user->characterRank($characters[2])); // 1 -> 1

        $characters[1]->setUserRank($this->user, 5);                                 // 2 -> 5
        $this->assertEquals(4, $this->user->characterRank($characters[0])); // 5 -> 4
        $this->assertEquals(3, $this->user->characterRank($characters[3])); // 4 -> 3
        $this->assertEquals(2, $this->user->characterRank($characters[4])); // 3 -> 2
        $this->assertEquals(1, $this->user->characterRank($characters[2])); // 1 -> 1

        $characters[3]->setUserRank($this->user, 4);                                 // 3 -> 4
        $this->assertEquals(3, $this->user->characterRank($characters[0])); // 4 -> 3
        $this->assertEquals(2, $this->user->characterRank($characters[4])); // 2 -> 2

        $characters[0]->setUserRank($this->user, 5);                                 // 3 -> 5
        $this->assertEquals(4, $this->user->characterRank($characters[1])); // 5 -> 4
        $this->assertEquals(3, $this->user->characterRank($characters[3])); // 4 -> 3
        $this->assertEquals(2, $this->user->characterRank($characters[4])); // 2 -> 2

        $characters[1]->setUserRank($this->user, 5);                                 // 4 -> 5
        $this->assertEquals(4, $this->user->characterRank($characters[0])); // 5 -> 4
        $this->assertEquals(3, $this->user->characterRank($characters[3])); // 3 -> 3
    }

    /**
     * @test
     */
    public function ranks_are_not_changed_if_a_negative_rank_or_rank_0_is_requested()
    {
        $characters = Character::factory()->count(3)->create();
        $characters[0]->setUserRank($this->user, 1);
        $characters[1]->setUserRank($this->user, 2);
        $characters[2]->setUserRank($this->user, 3);

        $characters[0]->setUserRank($this->user, 0);
        $this->assertEquals(1, $this->user->characterRank($characters[0]));
        $this->assertEquals(2, $this->user->characterRank($characters[1]));
        $this->assertEquals(3, $this->user->characterRank($characters[2]));

        $characters[0]->setUserRank($this->user, -1);
        $this->assertEquals(1, $this->user->characterRank($characters[0]));
        $this->assertEquals(2, $this->user->characterRank($characters[1]));
        $this->assertEquals(3, $this->user->characterRank($characters[2]));

        $characters[1]->setUserRank($this->user, 0);
        $this->assertEquals(1, $this->user->characterRank($characters[0]));
        $this->assertEquals(2, $this->user->characterRank($characters[1]));
        $this->assertEquals(3, $this->user->characterRank($characters[2]));

        $characters[1]->setUserRank($this->user, -1);
        $this->assertEquals(1, $this->user->characterRank($characters[0]));
        $this->assertEquals(2, $this->user->characterRank($characters[1]));
        $this->assertEquals(3, $this->user->characterRank($characters[2]));

        $characters[2]->setUserRank($this->user, 0);
        $this->assertEquals(1, $this->user->characterRank($characters[0]));
        $this->assertEquals(2, $this->user->characterRank($characters[1]));
        $this->assertEquals(3, $this->user->characterRank($characters[2]));

        $characters[2]->setUserRank($this->user, -1);
        $this->assertEquals(1, $this->user->characterRank($characters[0]));
        $this->assertEquals(2, $this->user->characterRank($characters[1]));
        $this->assertEquals(3, $this->user->characterRank($characters[2]));
    }

    /**********************************************************************
     * Ranking when 2 users exist
     **********************************************************************/

    /**
     * @test
     */
    public function ranks_of_another_user_are_not_changed_when_1_user_changes_a_rank()
    {
        $user2 = User::factory()->create();
        $characters = Character::factory()->count(3)->create();

        $characters[0]->setUserRank($user2, 1);
        $characters[1]->setUserRank($user2, 2);
        $characters[2]->setUserRank($user2, 3);

        $characters[0]->setUserRank($this->user, 1);
        $this->assertEquals(1, $user2->characterRank($characters[0]));
        $this->assertEquals(2, $user2->characterRank($characters[1]));
        $this->assertEquals(3, $user2->characterRank($characters[2]));

        $characters[1]->setUserRank($this->user, 2);
        $this->assertEquals(1, $user2->characterRank($characters[0]));
        $this->assertEquals(2, $user2->characterRank($characters[1]));
        $this->assertEquals(3, $user2->characterRank($characters[2]));

        $characters[1]->setUserRank($this->user, 1);
        $this->assertEquals(1, $user2->characterRank($characters[0]));
        $this->assertEquals(2, $user2->characterRank($characters[1]));
        $this->assertEquals(3, $user2->characterRank($characters[2]));

        $characters[2]->setUserRank($this->user, 1);
        $this->assertEquals(1, $user2->characterRank($characters[0]));
        $this->assertEquals(2, $user2->characterRank($characters[1]));
        $this->assertEquals(3, $user2->characterRank($characters[2]));

        $characters[2]->setUserRank($this->user, 2);
        $this->assertEquals(1, $user2->characterRank($characters[0]));
        $this->assertEquals(2, $user2->characterRank($characters[1]));
        $this->assertEquals(3, $user2->characterRank($characters[2]));

        $characters[2]->setUserRank($this->user, 3);
        $this->assertEquals(1, $user2->characterRank($characters[0]));
        $this->assertEquals(2, $user2->characterRank($characters[1]));
        $this->assertEquals(3, $user2->characterRank($characters[2]));

        $characters[1]->setUserRank($this->user);                                   // Unrank
        $this->assertEquals(1, $user2->characterRank($characters[0]));
        $this->assertEquals(2, $user2->characterRank($characters[1]));
        $this->assertEquals(3, $user2->characterRank($characters[2]));
    }
}
