<?php

namespace Tests\unit;

use App\Models\Anime;
use App\Models\MAL\MalAnime;
use App\Models\User;
use Tests\BrowserKitTestCase;

class AnimeTest extends BrowserKitTestCase
{
    protected $anime;

    public function setUp(): void
    {
        parent::setUp();

        $this->anime = Anime::factory()->create();
    }

    /**
     * @test
     */
    public function mal_animes_are_retrieved_when_an_anime_has_mal_animes()
    {
        $number = 2;

        MalAnime::factory()->count(2)->create(['anime_id' => $this->anime->id]);

        $malAnimes = $this->anime->malAnimes;

        $this->assertEquals($malAnimes->count(), $number);
    }

    /**
     * @test
     */
    public function my_mal_anime_score_is_retrieved_when_an_anime_has_mal_animes()
    {
        $malAnime = MalAnime::factory()->create(['anime_id' => $this->anime->id, 'score' => 10]);

        $user = User::factory()->create(['name' => $malAnime->malUser->name]);

        MalAnime::factory()->create(['anime_id' => $this->anime->id, 'score' => 5]);

        $this
            ->actingAs($user)
            ->assertEquals($malAnime->score, $this->anime->myMalAnime->score);
    }

    /**
     * @test
     */
    public function my_mal_anime_score_is_presented_correctly_when_an_anime_has_mal_animes()
    {
        $malAnime = MalAnime::factory()->create(['anime_id' => $this->anime->id, 'score' => 10]);

        $user = User::factory()->create(['name' => $malAnime->malUser->name]);

        MalAnime::factory()->create(['anime_id' => $this->anime->id, 'score' => 5]);

        $this
            ->actingAs($user)
            ->assertEquals($malAnime->anime->present()->myMALScore, $this->anime->present()->myMALScore);
    }
}
