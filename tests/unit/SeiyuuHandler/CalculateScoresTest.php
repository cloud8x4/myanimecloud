<?php

namespace Tests\unit\SeiyuuHandler;

use App\Models\Handlers\SeiyuuHandler;
use App\Models\Role;
use App\Models\User;
use Tests\BrowserKitTestCase;

class CalculateScoresTest extends BrowserKitTestCase
{
    /** @var User */
    protected $user;

    /** @var SeiyuuHandler */
    private $seiyuuHandler;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create();

        $this->seiyuuHandler = new SeiyuuHandler();

        /* @var Role $role */
        $role = Role::factory()->create();

        $role->seiyuu->setUserRank($this->user);

        $role->character->setUserRank($this->user);
    }

    /**
     * @test
     */
    public function it_calculates_scores()
    {
        $this->seiyuuHandler->calculateScores($this->user);

        $this->seeInDatabase('user_seiyuu', ['score_by_character_ranks' => 1000]);
    }
}
