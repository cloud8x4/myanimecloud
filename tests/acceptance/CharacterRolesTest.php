<?php

namespace Tests\acceptance;

use App\Models\Role;
use App\Models\User;
use Tests\BrowserKitTestCase;

class CharacterRolesTest extends BrowserKitTestCase
{
    protected $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create();
    }

    /**
     * @test
     */
    public function a_guest_should_see_the_page()
    {
        $this
            ->visit(route('character_roles'))
            ->seePageIs(route('character_roles'));
    }

    /**
     * @test
     */
    public function a_user_should_see_a_character_with_seiyuu_and_anime()
    {
        $role = Role::factory()->create();

        $this
            ->actingAs($this->user)
            ->visit(route('character_roles'))
            ->see($role->character->image)
            ->see($role->role)
            ->see($role->character->present()->myRank)
            ->see($role->seiyuu->image)
            ->see($role->seiyuu->name)
            ->see($role->seiyuu->present()->myRank)
            ->see($role->anime->image)
            ->see($role->anime->title)
            ->see($role->anime->present()->myMALScore);
    }
}
