<?php

namespace Tests\acceptance;

use App\Models\User;
use Tests\BrowserKitTestCase;

class LogoutTest extends BrowserKitTestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @test
     */
    public function a_user_should_be_able_to_logout()
    {
        $password = 'mysecretpass';

        $user = User::factory()->create([
            'email' => 'email',
            'password' => bcrypt($password),
        ]);

        $this
            ->visit('/login')
            ->type($user->email, 'email')
            ->type($password, 'password')
            ->press('Login')
            ->see(trans('navbar.Logout'))
            ->call('POST', '/logout');

        $this->assertRedirectedTo('/');
    }
}
