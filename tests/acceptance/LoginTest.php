<?php

namespace Tests\acceptance;

use App\Models\User;
use Tests\BrowserKitTestCase;

class LoginTest extends BrowserKitTestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @test
     */
    public function a_user_should_not_be_able_to_login_with_wrong_password()
    {
        $user = User::factory()->create();

        $this
            ->visit('/login')
            ->type($user->email, 'email')
            ->type('wrongpassword', 'password')
            ->press('Login')
            ->see(trans('auth.failed'));
    }

    /**
     * @test
     */
    public function a_user_should_not_be_able_to_login_with_wrong_username()
    {
        $password = 'mysecretpass';

        User::factory()->create([
            'email' => 'email',
            'password' => bcrypt($password),
        ]);

        $this
            ->visit('/login')
            ->type('Nonexistingemail', 'email')
            ->type($password, 'password')
            ->press('Login')
            ->see(trans('auth.failed'));
    }

    /**
     * @test
     */
    public function a_user_should_be_able_to_login()
    {
        $password = 'mysecretpass';

        $user = User::factory()->create([
            'email' => 'email',
            'password' => bcrypt($password),
        ]);

        $this
            ->visit('/login')
            ->type($user->email, 'email')
            ->type($password, 'password')
            ->press('Login')
            ->seePageIs(route('dashboard'));
    }
}
