<?php

namespace Tests\acceptance;

use App\Models\MAL\MalAnime;
use App\Models\MAL\MalUser;
use App\Models\Role;
use App\Models\User;
use Carbon\Carbon;
use Tests\BrowserKitTestCase;

class AnimeRolesTest extends BrowserKitTestCase
{
    /** @var User */
    protected $user;

    /** @var string */
    protected $uri;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create();

        $this->uri = route('anime_roles');
    }

    /**
     * @test
     */
    public function a_guest_should_see_the_page()
    {
        $this
            ->visit($this->uri)
            ->seePageIs($this->uri);
    }

    /**
     * @test
     */
    public function a_user_should_see_an_anime_with_character_and_seiyuu()
    {
        $role = Role::factory()->create();

        $this
            ->actingAs($this->user)
            ->visit($this->uri)
            ->see($role->anime->image)
            ->see($role->anime->title)
            ->see($role->anime->type)
            ->see($role->anime->present()->presentEpisodes)
            ->see($role->anime->present()->seasonStarted)
            ->see($role->anime->present()->myMALScore)
            ->see($role->anime->present()->presentMembersScore)
            ->see($role->character->name)
            ->see($role->character->image)
            ->see($role->seiyuu->image)
            ->see($role->seiyuu->name);
        // @TODO: see gender of seiyuu and character?;
    }

    /**********************************************************************
     * Filter
     **********************************************************************/

    /**
     * @test
     */
    public function a_user_should_see_a_filter_button()
    {
        $this
            ->actingAs($this->user)
            ->visit($this->uri)
            ->see('#filterAnimes');
    }

    /**
     * @test
     */
    public function a_user_should_see_a_collapsed_filter()
    {
        $this
            ->actingAs($this->user)
            ->visit($this->uri)
            ->see('filterAnimes')
            ->dontsee('<div id="filterAnimes" class="panel-body collapse in" aria-expanded="true">');
    }

    /**
     * @test
     */
    public function a_user_should_see_a_season_filter()
    {
        $this
            ->actingAs($this->user)
            ->visit($this->uri)
            ->see(trans('general.Season'))
            ->see(trans('general.Winter'))
            ->see(trans('general.Spring'))
            ->see(trans('general.Summer'))
            ->see(trans('general.Fall'))
            ->see(trans('general.All seasons'));
    }

    /**
     * @test
     */
    public function a_user_should_see_a_years_filter()
    {
        $this
            ->actingAs($this->user)
            ->visit($this->uri)
            ->see(trans('general.Season'))
            ->see(trans('general.All years'))
            ->see(trans('general.Unknown'))
            ->see(1950)
            ->see(Carbon::now()->year + 1)
            ->see(Carbon::now()->year)
            ->see(2000);
    }

    /**********************************************************************
     * Changed account name
     **********************************************************************/

    /**
     * @test
     */
    public function a_user_with_changed_existing_mal_user_name_sees_the_right_MAL_score()
    {
        $malAnime2 = MalAnime::factory()->create(['score' => 10]);

        MalUser::factory()->create(['name' => $this->user->name]);

        $malAnime = MalAnime::factory()->create([
            'mal_user_id' => $this->user->malUser->id,
            'score' => 5,
            'anime_id' => $malAnime2->anime->id,
        ]);

        $this->actingAs($this->user)
            ->visit($this->uri)
            ->see(trans('My MAL score' . ': ' . $malAnime->score))
            ->dontsee(trans('My MAL score' . ': ' . $malAnime2->score));
    }
}
