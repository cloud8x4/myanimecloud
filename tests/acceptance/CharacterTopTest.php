<?php

namespace Tests\acceptance;

use App\Models\Character;
use App\Models\User;
use Tests\BrowserKitTestCase;

class CharacterTopTest extends BrowserKitTestCase
{
    protected $user;

    protected $uri;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create();

        $this->uri = route('character_top');
    }

    /**
     * @test
     */
    public function a_guest_should_see_that_he_is_not_supposed_to_be_here()
    {
        $this
            ->visit($this->uri)
            ->seePageIs($this->uri)
            ->see("You're not supposed to be here!");
    }

    /**
     * @test
     */
    public function my_rank_shows_rank_of_the_logged_in_user()
    {
        $character = Character::factory()->create();

        $this
            ->actingAs($this->user)
            ->visit($this->uri)
            ->see(trans('My rank') . ': ?')
            ->dontsee(trans('My rank') . ': 1');

        $user2 = User::factory()->create();

        $character->setUserRank($user2);

        $this
            ->actingAs($this->user)
            ->visit($this->uri)
            ->see(trans('My rank') . ': ?')
            ->dontSee(trans('My rank') . ': 1');

        $this
            ->actingAs($this->user)
            ->visit(route('character_top', ['user' => $user2->id]))
            ->see(trans('My rank') . ': ?')
            ->dontSee(trans('My rank') . ': 1');

        $character->setUserRank($this->user);

        $character->setUserRank($user2); // unrank

        $this
            ->actingAs($this->user)
            ->visit($this->uri)
            ->see(trans('My rank') . ': 1')
            ->dontsee(trans('My rank') . ': ?');

        $this
            ->actingAs($this->user)
            ->visit(route('character_top', ['user' => $user2->id]))
            ->see(trans('My rank') . ': 1')
            ->dontsee(trans('My rank') . ': ?');
    }

    /**
     * @test
     */
    public function user_rank_is_only_visible_in_chosen_user_top()
    {
        User::factory()->create();

        Character::factory()->create();

        $this
            ->actingAs($this->user)
            ->visit($this->uri)
            ->dontsee(trans('User rank') . ': ');

        $this
            ->actingAs($this->user)
            ->visit(route('character_top', ['user' => $this->user->id]))
            ->see(trans('User rank') . ': ');

        $this
            ->actingAs($this->user)
            ->visit(route('character_top', ['user' => $this->user->id]))
            ->see(trans('User rank') . ': ');
    }

    /**
     * @test
     */
    public function user_rank_shows_rank_of_the_other_user()
    {
        $character = Character::factory()->create();

        $user2 = User::factory()->create();

        $character->setUserRank($user2);

        $this
            ->actingAs($this->user)
            ->visit(route('character_top', ['user' => $user2->id]))
            ->see(trans('User rank') . ': 1')
            ->dontsee(trans('User rank') . ': ?');

        $character->setUserRank($this->user);

        $character->setUserRank($user2); // unrank

        $this
            ->actingAs($this->user)
            ->visit(route('character_top', ['user' => $user2->id]))
            ->see(trans('User rank') . ': ?')
            ->dontSee(trans('User rank') . ': 1');
    }
}
