<?php

namespace Tests\acceptance;

use App\Models\User;
use Tests\BrowserKitTestCase;

class AnimeSharedTest extends BrowserKitTestCase
{
    /** @var User */
    protected $user;

    /** @var string */
    protected $uri;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create();

        $this->uri = route('anime_shared');
    }

    /**
     * @test
     */
    public function a_guest_should_be_redirected_to_the_login_page()
    {
        $this
            ->visit($this->uri)
            ->seePageIs('/login');
    }

    /**
     * @test
     */
    public function a_user_should_see_the_page()
    {
        $this
            ->actingAs($this->user)
            ->visit($this->uri)
            ->seePageIs($this->uri)
            ->see('Shared anime');
    }
}
