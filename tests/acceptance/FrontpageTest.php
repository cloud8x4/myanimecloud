<?php

namespace Tests\acceptance;

use Tests\BrowserKitTestCase;

class FrontpageTest extends BrowserKitTestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @test
     */
    public function a_guest_should_be_directed_to_the_dashboard()
    {
        $this
            ->visit('/')
            ->click('link-dashboard')
            ->seePageIs(route('dashboard'));
    }

    /**
     * @test
     */
    public function a_user_should_see_the_front_page()
    {
        $this
            ->visit('/')
            ->see(env('APP_NAME'))
            ->see(trans('navbar.Login'))
            ->see(trans('navbar.Register'))
            ->see('Bitbucket Repository')
            ->see('MyAnimeList');
    }
}
