<?php

namespace Tests\acceptance;

use App\Models\Anime;
use App\Models\Role;
use App\Models\Seiyuu;
use App\Models\User;
use Tests\BrowserKitTestCase;

class AnimeIndexTest extends BrowserKitTestCase
{
    protected $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create();
    }

    /**
     * @test
     */
    public function a_guest_should_see_the_page()
    {
        $anime = Anime::factory()->create();

        $this
            ->visit(action('AnimeController@anime', $anime->id))
            ->seePageIs(action('AnimeController@anime', $anime->id));
    }

    /**
     * @test
     */
    public function a_user_should_see_an_anime()
    {
        $anime = Anime::factory()->create();

        $this
            ->actingAs($this->user)
            ->visit(action('AnimeController@anime', $anime->id))
            ->see($anime->title)
            ->see($anime->updated_at->formatLocalized(trans('carbon.dateformatform')));
    }

    /**
     * @test
     */
    public function a_user_should_see_a_role_in_an_anime()
    {
        $role = Role::factory()->create();

        $this
            ->actingAs($this->user)
            ->visit(action('AnimeController@anime', $role->anime->id))
            ->see($role->character->image)
            ->see($role->character->name)
            ->see($role->character->present()->myRank)
            ->see($role->seiyuu->image)
            ->see($role->seiyuu->name)
            ->see($role->seiyuu->present()->myRank)
            ->see($role->seiyuu->present()->presentFavoritedCount);
    }

    /**
     * @test
     */
    public function a_user_should_see_roles_in_other_animes()
    {
        $seiyuu = Seiyuu::factory()->create();
        $roles = Role::factory()->count(2)->create(['seiyuu_id' => $seiyuu->id]);

        $this
            ->actingAs($this->user)
            ->visit('/anime/' . $roles[0]->anime->id)
            ->see($roles[1]->character->name)
            ->see($roles[1]->character->image)
            ->see($roles[1]->anime->image)
            ->see($roles[1]->anime->name);
    }
}
