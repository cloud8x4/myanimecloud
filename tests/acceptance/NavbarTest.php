<?php

namespace Tests\acceptance;

use App\Models\User;
use Tests\BrowserKitTestCase;

class NavbarTest extends BrowserKitTestCase
{
    /** @var User */
    protected $user;

    /** @var string */
    protected $uri;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create();

        $this->uri = route('dashboard'); // Any page except the front page can be used.
    }

    /***********************************************************
     * Guest
     ***********************************************************/

    /**
     * @test
     */
    public function a_guest_can_see_right_items()
    {
        $this
            ->visit($this->uri)
            ->see(trans('navbar.Login'))
            ->see(trans('navbar.Register'));
    }

    /**
     * @test
     */
    public function a_guest_cannot_see_user_profile_and_import()
    {
        $this
            ->visit($this->uri)
            ->dontSee(trans('navbar.Profile'))
            ->dontSee('Import');
    }

    /**
     * @test
     */
    public function a_guest_can_see_search()
    {
        $this
            ->visit($this->uri)
            ->see(trans('navbar.Search'));
    }

    /**
     * @test
     */
    public function a_guest_can_see_left_items()
    {
        $this
            ->visit($this->uri)
            ->see(env('APP_NAME'))
            ->see(trans('navbar.Seiyuu'))
            ->see(trans('navbar.Character'))
            ->see(trans('navbar.Anime'))
            ->see(trans('navbar.Roles'))
            ->see(trans('navbar.Top'))
            ->see(trans('navbar.Shared'))
            ->see(trans('navbar.Users'));
    }

    /**
     * @test
     */
    public function a_guest_can__navigate_to_seiyuu_roles()
    {
        $this
            ->actingAs($this->user)
            ->visit($this->uri)
            ->click('seiyuu_roles')
            ->seePageIs(route('seiyuu_roles'));
    }

    /**
     * @test
     */
    public function a_guest_can_not_navigate_to_seiyuu_top()
    {
        $this
            ->visit($this->uri)
            ->donTsee('seiyuu_top');
    }

    /**
     * @test
     */
    public function a_guest_can__navigate_to_character_roles()
    {
        $this
            ->visit($this->uri)
            ->click('character_roles')
            ->seePageIs(route('character_roles'));
    }

    /**
     * @test
     */
    public function a_guest_can_not_navigate_to_character_top()
    {
        $this
            ->visit($this->uri)
            ->donTsee('character_top');
    }

    /**
     * @test
     */
    public function a_guest_can__navigate_to_anime_roles()
    {
        $this
            ->visit($this->uri)
            ->click('anime_roles')
            ->seePageIs('/anime');
    }

    /**
     * @test
     */
    public function a_guest_can_not_navigate_to_anime_shared()
    {
        $this
            ->visit($this->uri)
            ->donTsee('anime_shared');
    }

    /**
     * @test
     */
    public function a_guest_can__navigate_to_users()
    {
        $this
            ->visit($this->uri)
            ->click('Users')
            ->seePageIs(route('users'));
    }

    /***********************************************************
     * User
     ***********************************************************/

    /**
     * @test
     */
    public function a_user_can_see_right_items()
    {
        $this
            ->actingAs($this->user)
            ->visit($this->uri)
            ->see($this->user->name)
            ->see(trans('navbar.Profile'))
            ->see('Import')
            ->see(trans('navbar.Logout'));
    }

    /**
     * @test
     */
    public function a_user_cannot_see_register_and_login()
    {
        $this
            ->actingAs($this->user)
            ->visit($this->uri)
            ->dontSee(trans('navbar.Login'))
            ->dontSee(trans('navbar.Register'));
    }

    /**
     * @test
     */
    public function a_user_can_navigate_to_user_profile()
    {
        $this
            ->actingAs($this->user)
            ->visit($this->uri)
            ->click('user_profile')
            ->seePageIs(action('UserController@profile', $this->user));
    }

    /**
     * @test
     */
    public function a_user_can_navigate_to_user_import()
    {
        $this
            ->actingAs($this->user)
            ->visit($this->uri)
            ->click('user_import')
            ->seePageIs('/user/' . $this->user->id . '/import');
        // This is a vue page, so no specific route exists in web.php.
    }

    /**
     * @test
     */
    public function a_user_can_see_search()
    {
        $this
            ->actingAs($this->user)
            ->visit($this->uri)
            ->see(trans('navbar.Search'));
    }

    /**
     * @test
     */
    public function a_user_can_see_left_items()
    {
        $this
            ->actingAs($this->user)
            ->visit($this->uri)
            ->see(env('APP_NAME'))
            ->see(trans('navbar.Seiyuu'))
            ->see(trans('navbar.Character'))
            ->see(trans('navbar.Anime'))
            ->see(trans('navbar.Roles'))
            ->see(trans('navbar.Top'))
            ->see(trans('navbar.Shared'))
            ->see(trans('navbar.Users'));
    }

    /**
     * @test
     */
    public function a_user_can_navigate_to_seiyuu_roles()
    {
        $this
            ->actingAs($this->user)
            ->visit($this->uri)
            ->click('seiyuu_roles')
            ->seePageIs(route('seiyuu_roles'));
    }

    /**
     * @test
     */
    public function a_user_can_navigate_to_seiyuu_top()
    {
        $this
            ->actingAs($this->user)
            ->visit($this->uri)
            ->click('seiyuu_top')
            ->seePageIs(route('seiyuu_top'));
    }

    /**
     * @test
     */
    public function a_user_can_navigate_to_character_roles()
    {
        $this
            ->actingAs($this->user)
            ->visit($this->uri)
            ->click('character_roles')
            ->seePageIs(route('character_roles'));
    }

    /**
     * @test
     */
    public function a_user_can_navigate_to_character_top()
    {
        $this
            ->actingAs($this->user)
            ->visit($this->uri)
            ->click('character_top')
            ->seePageIs(route('character_top'));
    }

    /**
     * @test
     */
    public function a_user_can_navigate_to_anime_roles()
    {
        $this
            ->actingAs($this->user)
            ->visit($this->uri)
            ->click('anime_roles')
            ->seePageIs('/anime');
    }

    /**
     * @test
     */
    public function a_user_can_navigate_to_anime_shared()
    {
        $this
            ->actingAs($this->user)
            ->visit($this->uri)
            ->click('anime_shared')
            ->seePageIs(route('anime_shared'));
    }

    /**
     * @test
     */
    public function a_user_can_navigate_to_users()
    {
        $this
            ->actingAs($this->user)
            ->visit($this->uri)
            ->click('Users')
            ->seePageIs(route('users'));
    }
}
