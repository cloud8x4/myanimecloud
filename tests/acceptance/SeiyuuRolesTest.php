<?php

namespace Tests\acceptance;

use App\Models\Role;
use App\Models\User;
use Tests\BrowserKitTestCase;

class SeiyuuRolesTest extends BrowserKitTestCase
{
    protected $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create();
    }

    /**
     * @test
     */
    public function a_guest_should_see_the_page()
    {
        $this->visit(action('SeiyuuController@roles'))
            ->seePageIs(action('SeiyuuController@roles'));
    }

    /**
     * @test
     */
    public function a_user_should_see_a_seiyuu_with_character_and_anime()
    {
        $role = Role::factory()->create();

        $this->actingAs($this->user)
            ->visit(action('SeiyuuController@roles'))
            ->see($role->seiyuu->image)
            ->see($role->character->present()->myRank)
            ->see($role->character->present()->presentFavoritedCount)
            ->see($role->character->present()->age)
            ->see($role->character->present()->birthdayDateLocalized)
            ->see($role->seiyuu->image)
            ->see($role->seiyuu->name)
            ->see($role->seiyuu->present()->myRank)
            ->see($role->anime->image)
            ->see($role->anime->title)
            ->see($role->anime->present()->myMALScore);
    }
}
