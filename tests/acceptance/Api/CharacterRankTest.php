<?php

namespace Tests\acceptance\Api;

use App\Events\CharacterRankChanged;
use App\Models\Character;
use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\Event;
use Tests\BrowserKitTestCase;

class CharacterRankTest extends BrowserKitTestCase
{
    /** @var User */
    protected $user;

    /** @var string */
    protected $uri;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create();

        $this->actingAs($this->user);

        $this->uri = route('character_rank');
    }

    /**
     * @test
     */
    public function a_user_can_set_ranks()
    {
        $characters = Character::factory()->count(2)->create();

        $this
            ->post($this->uri, ['id' => $characters[0]->id, 'rank' => ''])
            ->seeJson(['message' => 'Successfully set rank of character ' . $characters[0]->name . ' to ' . '1.']);

        $this
            ->post($this->uri, ['id' => $characters[1]->id, 'rank' => '2'])
            ->seeJson(['message' => 'Successfully set rank of character ' . $characters[1]->name . ' to ' . '2.'])
            ->post($this->uri, ['id' => $characters[1]->id, 'rank' => '1'])
            ->seeJson(['message' => 'Successfully set rank of character ' . $characters[1]->name . ' to ' . '1.']);

        $characters[1]->setUserRank($this->user, '');

        $this
            ->post($this->uri, ['id' => $characters[1]->id, 'rank' => ''])
            ->seeJson(['message' => 'Successfully set rank of character ' . $characters[1]->name . ' to ' . '2.']);
    }

    /**
     * @test
     */
    public function a_user_can_unrank()
    {
        $character = Character::factory()->create();

        $character->setUserRank($this->user, 1);

        $this
            ->post($this->uri, ['id' => $character->id, 'rank' => ''])
            ->seeJson(['message' => 'Successfully unranked character ' . $character->name . '.']);
    }

    /**
     * @test
     */
    public function a_user_cannot_set_wrong_ranks()
    {
        $character = Character::factory()->create();

        $this
            ->post($this->uri, ['id' => $character->id, 'rank' => '0'])
            ->seeJson(['message' => 'Rank of character ' . $character->name . ' was not set to 0 because rank must be a number equal to or higher than 1.']);

        $this
            ->post($this->uri, ['id' => $character->id, 'rank' => '-1'])
            ->seeJson(['message' => 'Rank of character ' . $character->name . ' was not set to -1 because rank must be a number equal to or higher than 1.']);

        $this
            ->post($this->uri, ['id' => $character->id, 'rank' => 'e'])
            ->seeJson(['message' => 'Rank of character ' . $character->name . ' was not set to e because rank must be a number equal to or higher than 1.']);

        $this
            ->post($this->uri, ['id' => $character->id, 'rank' => 'test string'])
            ->seeJson(['message' => 'Rank of character ' . $character->name . ' was not set to test string because rank must be a number equal to or higher than 1.']);
    }

    /**
     * @test
     */
    public function a_character_rank_changed_event_was_dispatched()
    {
        Event::fake();

        $character = Character::factory()->create();

        $this->post($this->uri, ['id' => $character->id, 'rank' => '']);

        Event::assertDispatched(CharacterRankChanged::class);

        Event::assertDispatched(function (CharacterRankChanged $event) {
            return $event->user->id === $this->user->id;
        });
    }

    /**
     * @test
     */
    public function the_character_rank_changed_event_is_handled_as_expected()
    {
        /* @var Role $role */
        $role = Role::factory()->create();

        $role->seiyuu->setUserRank($this->user);

        $this->post($this->uri, ['id' => $role->character->id, 'rank' => '']);

        $this->seeInDatabase('user_seiyuu', ['score_by_character_ranks' => 1000]);
    }
}
