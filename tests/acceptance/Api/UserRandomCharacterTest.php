<?php

namespace Tests\acceptance\Api;

use App\Models\Character;
use App\Models\User;
use Illuminate\Http\Response;
use Tests\BrowserKitTestCase;

class UserRandomCharacterTest extends BrowserKitTestCase
{
    /** @var User */
    protected $user;

    /** @var string */
    protected $uri;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create();

        $this->uri = '/api/user/character/random';
    }

    /**
     * @test
     */
    public function a_guest_gets_an_unauthorized_response()
    {
        $this
            ->getJson($this->uri)
            ->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }

    /**
     * @test
     */
    public function a_user_gets_a_warning_if_no_characters_found()
    {
        $this
            ->actingAs($this->user)
            ->getJson($this->uri)
            ->assertResponseOk()
            ->seeJsonEquals(['warning' => 'No characters found']);
    }

    /**
     * @test
     */
    public function a_user_gets_a_character()
    {
        $character = Character::factory()->create();

        $character->setUserRank($this->user, 1);

        $this
            ->actingAs($this->user)
            ->getJson($this->uri)
            ->assertResponseOk()
            ->seeJson($character->toArray());
    }
}
