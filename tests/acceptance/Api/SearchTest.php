<?php

namespace Tests\acceptance\Api;

use App\Models\Anime;
use App\Models\Character;
use App\Models\Seiyuu;
use App\Models\User;
use Tests\BrowserKitTestCase;

class SearchTest extends BrowserKitTestCase
{
    /** @var User */
    protected $user;

    /** @var string */
    protected $uri;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create();

        $this->uri = '/api/search';
    }

    /**
     * @test
     */
    public function a_guest_can_search_an_anime()
    {
        $anime = Anime::factory()->create();

        $this
            ->postJson($this->uri, ['search' => substr($anime->title, 0, 4)])
            ->assertResponseOk()
            ->seeJsonStructure([
                'animes' => [
                    [
                        'id',
                        'episodes',
                        'image',
                        'members_count',
                        'title',
                        'type',
                    ],
                ],
            ]);
    }

    /**
     * @test
     */
    public function a_guest_can_search_a_character()
    {
        $character = Character::factory()->create();

        $this
            ->postJson($this->uri, ['search' => substr($character->name, 0, 4)])
            ->assertResponseOk()
            ->seeJsonStructure([
                'characters' => [
                    [
                        'id',
                        'image',
                        'name',
                    ],
                ],
            ]);
    }

    /**
     * @test
     */
    public function a_guest_can_search_a_seiyuu()
    {
        $seiyuu = Seiyuu::factory()->create();

        $this
            ->postJson($this->uri, ['search' => substr($seiyuu->name, 0, 4)])
            ->assertResponseOk()
            ->seeJsonStructure([
                'seiyuus' => [
                    [
                        'favorited_count',
                        'id',
                        'image',
                        'name',
                    ],
                ],
            ]);
    }

    /**
     * @test
     */
    public function a_user_can_search_an_anime()
    {
        $anime = Anime::factory()->create();

        $this
            ->actingAs($this->user)
            ->postJson($this->uri, ['search' => substr($anime->title, 0, 4)])
            ->assertResponseOk()
            ->seeJsonStructure([
                'animes' => [
                    [
                        'id',
                        'episodes',
                        'image',
                        'members_count',
                        'title',
                        'type',
                    ],
                ],
            ]);
    }

    /**
     * @test
     */
    public function a_user_can_search_a_character()
    {
        $character = Character::factory()->create();

        $this
            ->actingAs($this->user)
            ->postJson($this->uri, ['search' => substr($character->name, 0, 4)])
            ->assertResponseOk()
            ->seeJsonStructure([
                'characters' => [
                    [
                        'favorited_count',
                        'id',
                        'image',
                        'name',
                        'rank',
                    ],
                ],
            ]);
    }

    /**
     * @test
     */
    public function a_user_can_search_a_seiyuu()
    {
        $seiyuu = Seiyuu::factory()->create();

        $this
            ->actingAs($this->user)
            ->postJson($this->uri, ['search' => substr($seiyuu->name, 0, 4)])
            ->assertResponseOk()
            ->seeJsonStructure([
                'seiyuus' => [
                    [
                        'favorited_count',
                        'id',
                        'image',
                        'name',
                        'rank',
                    ],
                ],
            ]);
    }
}
