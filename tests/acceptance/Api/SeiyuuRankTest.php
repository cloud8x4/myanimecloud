<?php

namespace Tests\acceptance\Api;

use App\Models\Seiyuu;
use App\Models\User;
use Tests\BrowserKitTestCase;

class SeiyuuRankTest extends BrowserKitTestCase
{
    /** @var User */
    protected $user;

    /** @var string */
    protected $uri;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create();

        $this->actingAs($this->user);

        $this->uri = action('API\SeiyuuController@rank');
    }

    /**
     * @test
     */
    public function a_user_can_set_ranks()
    {
        $seiyuus = Seiyuu::factory()->count(2)->create();

        $this
            ->post($this->uri, ['id' => $seiyuus[0]->id, 'rank' => ''])
            ->seeJson(['message' => 'Successfully set rank of seiyuu ' . $seiyuus[0]->name . ' to ' . '1.']);

        $this
            ->post($this->uri, ['id' => $seiyuus[1]->id, 'rank' => '2'])
            ->seeJson(['message' => 'Successfully set rank of seiyuu ' . $seiyuus[1]->name . ' to ' . '2.'])
            ->post($this->uri, ['id' => $seiyuus[1]->id, 'rank' => '1'])
            ->seeJson(['message' => 'Successfully set rank of seiyuu ' . $seiyuus[1]->name . ' to ' . '1.']);

        $seiyuus[1]->setUserRank($this->user, '');

        $this
            ->post($this->uri, ['id' => $seiyuus[1]->id, 'rank' => ''])
            ->seeJson(['message' => 'Successfully set rank of seiyuu ' . $seiyuus[1]->name . ' to ' . '2.']);
    }

    /**
     * @test
     */
    public function a_user_can_unrank()
    {
        $seiyuu = Seiyuu::factory()->create();

        $seiyuu->setUserRank($this->user, 1);

        $this
            ->post($this->uri, ['id' => $seiyuu->id, 'rank' => ''])
            ->seeJson(['message' => 'Successfully unranked seiyuu ' . $seiyuu->name . '.']);
    }

    /**
     * @test
     */
    public function a_user_cannot_set_wrong_ranks()
    {
        $seiyuu = Seiyuu::factory()->create();

        $this
            ->post($this->uri, ['id' => $seiyuu->id, 'rank' => '0'])
            ->seeJson([
                'message' => 'Rank of seiyuu ' . $seiyuu->name . ' was not set to 0 because rank must be a number equal to or higher than 1.',
            ]);

        $this
            ->post($this->uri, ['id' => $seiyuu->id, 'rank' => '-1'])
            ->seeJson([
                'message' => 'Rank of seiyuu ' . $seiyuu->name . ' was not set to -1 because rank must be a number equal to or higher than 1.',
            ]);

        $this
            ->post($this->uri, ['id' => $seiyuu->id, 'rank' => 'e'])
            ->seeJson([
                'message' => 'Rank of seiyuu ' . $seiyuu->name . ' was not set to e because rank must be a number equal to or higher than 1.',
            ]);

        $this
            ->post($this->uri, ['id' => $seiyuu->id, 'rank' => 'test string'])
            ->seeJson([
                'message' => 'Rank of seiyuu ' . $seiyuu->name . ' was not set to test string because rank must be a number equal to or higher than 1.',
            ]);
    }
}
