<?php

namespace Tests\acceptance\Api;

use App\Events\RoleWeightChanged;
use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\Event;
use Tests\BrowserKitTestCase;

class RoleWeightTest extends BrowserKitTestCase
{
    /** @var User */
    protected $user;

    /** @var string */
    protected $uri;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create();

        $this->actingAs($this->user);

        $this->uri = route('role_weight');
    }

    /**
     * @test
     */
    public function a_role_weight_changed_event_was_dispatched()
    {
        Event::fake();

        $role = Role::factory()->create();

        $this->post($this->uri, ['id' => $role->id, 'weight' => '10']);

        Event::assertDispatched(RoleWeightChanged::class);
    }
}
