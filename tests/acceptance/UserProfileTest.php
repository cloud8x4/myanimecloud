<?php

namespace Tests\acceptance;

use App\Models\User;
use Auth;
use Faker\Factory as Faker;
use Faker\Generator;
use Hash;
use Tests\BrowserKitTestCase;

class UserProfileTest extends BrowserKitTestCase
{
    /** @var User */
    protected $user;

    /** @var string */
    protected $uri;

    /** @var Generator */
    protected $faker;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create();

        $this->uri = action('UserController@profile', $this->user);

        $this->faker = Faker::create();
    }

    /**
     * @test
     */
    public function a_guest_should_be_redirected_to_the_login_page()
    {
        $this
            ->visit($this->uri)
            ->seePageIs('/login');
    }

    /**
     * @test
     */
    public function a_user_should_see_update()
    {
        $this
            ->actingAs($this->user)
            ->visit($this->uri)
            ->see(trans('user.update.title'))
            ->see(trans('user.update.name'))
            ->see(trans('user.update.email'))
            ->see(trans('user.update.password'))
            ->see(trans('user.update.confirm password'));
    }

    /**
     * @test
     */
    public function a_user_can_update_his_username()
    {
        $username = $this->faker->userName;

        $this
            ->actingAs($this->user)
            ->visit($this->uri)
            ->type($username, 'name')
            ->press('update')
            ->see(trans('user.update.success'))
            ->seePageIs('/user/profile')
            ->assertEquals($username, Auth::user()->name);
    }

    /**
     * @test
     */
    public function a_user_can_update_his_email()
    {
        $email = $this->faker->email;

        $this
            ->actingAs($this->user)
            ->visit($this->uri)
            ->type($email, 'email')
            ->press('update')
            ->see(trans('user.update.success'))
            ->seePageIs('/user/profile')
            ->assertEquals($email, Auth::user()->email);
    }

    /**
     * @test
     */
    public function a_user_can_update_his_password()
    {
        $password = $this->faker->password;

        $this
            ->actingAs($this->user)
            ->visit($this->uri)
            ->type($password, 'password')
            ->type($password, 'password_confirmation')
            ->press('update')
            ->see(trans('user.update.success'))
            ->seePageIs('/user/profile')
            ->assertTrue(Hash::check($password, Auth::user()->password));
    }
}
