<?php

namespace Tests\acceptance;

use App\Models\Seiyuu;
use App\Models\User;
use Tests\BrowserKitTestCase;

class SeiyuuTopTest extends BrowserKitTestCase
{
    protected $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create();
    }

    /**
     * @test
     */
    public function a_guest_should_see_that_he_is_not_supposed_to_be_here()
    {
        $this
            ->visit('/seiyuu/top')
            ->seePageIs('/seiyuu/top')
            ->see("You're not supposed to be here!");
    }

    /**
     * @test
     */
    public function my_rank_shows_seiyuu_rank_of_the_logged_in_user()
    {
        $seiyuu = Seiyuu::factory()->create();

        $this
            ->actingAs($this->user)
            ->visit('/seiyuu/top')
            ->see(trans('My rank') . ': ?')
            ->dontsee(trans('My rank') . ': 1');

        $user2 = User::factory()->create();

        $seiyuu->setUserRank($user2);

        $this
            ->actingAs($this->user)
            ->visit('/seiyuu/top')
            ->see(trans('My rank') . ': ?')
            ->dontSee(trans('My rank') . ': 1');

        $this
            ->actingAs($this->user)
            ->visit('/seiyuu/top/' . $user2->id)
            ->see(trans('My rank') . ': ?')
            ->dontSee(trans('My rank') . ': 1');

        $seiyuu->setUserRank($this->user);

        $seiyuu->setUserRank($user2); // unrank

        $this
            ->actingAs($this->user)
            ->visit('/seiyuu/top')
            ->see(trans('My rank') . ': 1')
            ->dontsee(trans('My rank') . ': ?');
        $this
            ->actingAs($this->user)
            ->visit('/seiyuu/top/' . $user2->id)
            ->see(trans('My rank') . ': 1')
            ->dontsee(trans('My rank') . ': ?');
    }

    /**
     * @test
     */
    public function user_seiyuu_rank_is_only_visible_in_chosen_user_top()
    {
        $user = User::factory()->create();

        Seiyuu::factory()->create();

        $this
            ->actingAs($this->user)
            ->visit('/seiyuu/top')
            ->dontsee(trans('User rank') . ': ');

        $this
            ->actingAs($this->user)
            ->visit('/seiyuu/top/' . $this->user->id)
            ->see(trans('User rank') . ': ');

        $this
            ->actingAs($this->user)
            ->visit('/seiyuu/top/' . $user->id)
            ->see(trans('User rank') . ': ');
    }

    /**
     * @test
     */
    public function user_seiyuu_rank_shows_rank_of_the_other_user()
    {
        $seiyuu = Seiyuu::factory()->create();

        $user2 = User::factory()->create();

        $seiyuu->setUserRank($user2);

        $this
            ->actingAs($this->user)
            ->visit('/seiyuu/top/' . $user2->id)
            ->see(trans('User rank') . ': 1')
            ->dontsee(trans('User rank') . ': ?');

        $seiyuu->setUserRank($this->user);

        $seiyuu->setUserRank($user2); // unrank

        $this
            ->actingAs($this->user)
            ->visit('/seiyuu/top/' . $user2->id)
            ->see(trans('User rank') . ': ?')
            ->dontSee(trans('User rank') . ': 1');
    }
}
