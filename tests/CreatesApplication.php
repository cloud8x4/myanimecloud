<?php

namespace Tests;

use Illuminate\Contracts\Console\Kernel;
use Illuminate\Foundation\Application;

trait CreatesApplication
{
    /**
     * Creates the application.
     *
     * @return Application
     */
    public function createApplication()
    {
        $app = require __DIR__ . '/../bootstrap/app.php';

        if (!file_exists(dirname(__DIR__) . '/.env.testing')) {
            dd('Please create a .env.testing configuration file');
        }

        $app->make(Kernel::class)->bootstrap();

        return $app;
    }
}
