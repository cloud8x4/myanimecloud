<?php

namespace Tests;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Artisan;
use Laravel\BrowserKitTesting\TestCase as BaseTestCase;

abstract class BrowserKitTestCase extends BaseTestCase
{
    use CreatesApplication;
    use DatabaseTransactions;

    /**
     * Is this the first test.
     *
     * @var bool
     */
    protected static $isInitialTest = true;

    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost';

    public function setUp(): void
    {
        parent::setUp();

        if (self::$isInitialTest) {
            self::$isInitialTest = false;
            Artisan::call('migrate:refresh');
        }
    }
}
